package com.grahhnt.Crossbow.Kits;

import java.util.HashMap;

import com.grahhnt.Crossbow.Crossbow;
import com.grahhnt.Crossbow.CrossbowComponent;

/**
 * Kits are inventory layouts. Each kit will be registered with Crossbow to
 * allow for customization via lobby NPCs.
 * 
 * @author grant
 *
 */
public class KitManager extends CrossbowComponent {
	private HashMap<String, InventoryLayout> kits = new HashMap<>();

	public KitManager(Crossbow game) {
		super(game);
	}

	@Override
	public void initialize() {

	}

	public void registerKit(InventoryLayout kit) {
		kits.put(kit.getID(), kit);
	}

	public InventoryLayout getKit(String id) {
		return kits.get(id);
	}
}
