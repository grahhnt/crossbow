package com.grahhnt.Crossbow.Kits;

import java.util.ArrayList;
import java.util.HashMap;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

import com.grahhnt.Crossbow.Items.CrossbowItem;
import com.grahhnt.Crossbow.Players.CrossbowTeam;

public class InventoryLayout {
	private String id;
	private String name;
	private ArrayList<Item> items = new ArrayList<>();

	public InventoryLayout(String id, String name) {
		this.id = id;
		this.name = name;
	}

	public String getID() {
		return id;
	}

	public String getName() {
		return name;
	}

	public void addItem(Item item) {
		items.add(item);
	}

	public ArrayList<Item> getItems() {
		return items;
	}

	public static class Item {
		protected int defaultSlot;
		private CrossbowItem item;

		public Item(CrossbowItem item, int defaultSlot) {
			this.item = item;
			this.defaultSlot = defaultSlot;
		}
		
		public Item setSlot(int slot) {
			this.defaultSlot = slot;
			return this;
		}

		// TODO: also load from db if any preferences were set
		public int getSlot() {
			return defaultSlot;
		}

		public CrossbowItem getItem() {
			return item;
		}
		
		public Item clone() {
			return new Item(item, defaultSlot);
		}
	}

	/**
	 * Items that change based on what team you're on
	 * 
	 * @author grant
	 *
	 */
	public static class TeamItem extends InventoryLayout.Item {
		// keyed by team ID
		private HashMap<String, CrossbowItem> variants = new HashMap<>();

		public TeamItem(int defaultSlot) {
			super(new CrossbowItem(Material.WHITE_WOOL, 1), defaultSlot);
		}

		public TeamItem addVariant(CrossbowTeam team, CrossbowItem item) {
			variants.put(team.getID(), item);
			return this;
		}

		public CrossbowItem getItem(CrossbowTeam team) {
			return variants.get(team.getID());
		}

		@Override
		public CrossbowItem getItem() {
			throw new RuntimeException("getItem() on TeamKit.Item");
		}
		
		@Override
		public TeamItem clone() {
			TeamItem i = new TeamItem(defaultSlot);
			i.variants = variants;
			return i;
		}
	}
}
