package com.grahhnt.Crossbow;

import org.bukkit.permissions.Permission;

/**
 * All Crossbow components should extend this
 * 
 * @author grant
 *
 */
public abstract class CrossbowComponent {
	protected Crossbow game;

	protected CrossbowComponent(Crossbow game) {
		this.game = game;
	}

	protected void registerPermission(Permission permission) {
		game.permissions.registerPermission(this, permission);
	}

	/**
	 * Called when Crossbow has been initialized
	 */
	public abstract void initialize();

	/**
	 * Called when game has ended and components need to cleanup
	 */
	public void cleanup() {

	}

	/**
	 * Called upon server shutdown
	 */
	public void destroy() {

	}
}
