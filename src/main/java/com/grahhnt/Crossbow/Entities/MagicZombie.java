package com.grahhnt.Crossbow.Entities;

import org.bukkit.Location;
import org.bukkit.entity.EntityType;

import com.grahhnt.Crossbow.Crossbow;

public class MagicZombie extends CrossbowEntity {
	public MagicZombie() {
		npc = Crossbow.getInstance().entities.getRegistry().createNPC(EntityType.ZOMBIE, "magicZombie");
	}

	@Override
	public void spawn(Location location) {
		npc.spawn(location);
		npc.getNavigator().setTarget(location.clone().add(-10, 0, 0));
		npc.getEntity().setGravity(false);
	}
}
