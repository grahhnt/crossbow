package com.grahhnt.Crossbow.Entities;

import com.grahhnt.Crossbow.Crossbow;
import com.grahhnt.Crossbow.CrossbowComponent;

import net.citizensnpcs.api.CitizensAPI;
import net.citizensnpcs.api.npc.MemoryNPCDataStore;
import net.citizensnpcs.api.npc.NPCRegistry;

public class EntityManager extends CrossbowComponent {
	private NPCRegistry npcs;
	
	public EntityManager(Crossbow game) {
		super(game);
		npcs = CitizensAPI.createNamedNPCRegistry("Crossbow", new MemoryNPCDataStore());
	}
	
	@Override
	public void initialize() {
		
	}
	
	public void cleanup() {
		npcs.deregisterAll();
	}
	
	public NPCRegistry getRegistry() {
		return npcs;
	}
}
