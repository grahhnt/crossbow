package com.grahhnt.Crossbow;

import java.lang.reflect.InvocationTargetException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

import org.bukkit.GameMode;
import org.bukkit.attribute.Attribute;
import org.bukkit.event.HandlerList;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitTask;

import com.grahhnt.Crossbow.GameMetadata.DisplayType;
import com.grahhnt.Crossbow.Commands.CommandManager;
import com.grahhnt.Crossbow.Components.Functions;
import com.grahhnt.Crossbow.Components.General.InstantRespawn;
import com.grahhnt.Crossbow.Components.PostGame.AnnounceWinners;
import com.grahhnt.Crossbow.Components.PostGame.ClearPlayersInventories;
import com.grahhnt.Crossbow.Components.PostGame.Fireworks;
import com.grahhnt.Crossbow.Components.PostGame.ResetMap;
import com.grahhnt.Crossbow.Components.Team.NoFriendlyFire;
import com.grahhnt.Crossbow.Entities.EntityManager;
import com.grahhnt.Crossbow.Events.GameEventHandler;
import com.grahhnt.Crossbow.Events.GameEventManager;
import com.grahhnt.Crossbow.Events.GameListener;
import com.grahhnt.Crossbow.Events.Game.GameEndEvent;
import com.grahhnt.Crossbow.Events.Game.GameStartEvent;
import com.grahhnt.Crossbow.Events.Game.GameStateEvent;
import com.grahhnt.Crossbow.Events.GameEventHandler.EventPriority;
import com.grahhnt.Crossbow.Events.Map.MapChangeEvent;
import com.grahhnt.Crossbow.Events.Player.GamePlayerDamageEvent;
import com.grahhnt.Crossbow.Events.Player.GamePlayerDeathEvent;
import com.grahhnt.Crossbow.Events.Player.GamePlayerDestroyEvent;
import com.grahhnt.Crossbow.Events.Player.GamePlayerDisconnectEvent;
import com.grahhnt.Crossbow.Events.Player.GamePlayerJoinEvent;
import com.grahhnt.Crossbow.Exceptions.EventCanceledException;
import com.grahhnt.Crossbow.Exceptions.NoMapSelectedException;
import com.grahhnt.Crossbow.GUI.CrossbowGUI;
import com.grahhnt.Crossbow.Items.ItemManager;
import com.grahhnt.Crossbow.Kits.KitManager;
import com.grahhnt.Crossbow.Map.MapManager;
import com.grahhnt.Crossbow.Map.Config.MapConfiguration;
import com.grahhnt.Crossbow.Players.CrossbowTeam;
import com.grahhnt.Crossbow.Players.GamePlayer;
import com.grahhnt.Crossbow.Players.PlayerManager;
import com.grahhnt.Crossbow.Players.Winnable;
import com.grahhnt.ServerMetaAPI.ServerMetaAPI;

public class Crossbow implements GameListener {
	JavaPlugin plugin;

	public enum GameState {
		LOBBY, PREGAME, GAME, POSTGAME,

		/**
		 * A game timeout is currently active.
		 * 
		 * eg. player disconnect
		 */
		TIMEOUT
	}

	private GameState gameState = GameState.LOBBY;
//	private GameTimeout activeTimeout = null;
	private static Crossbow instance;

	public PlayerManager players = null;
	public KitManager kits = null;
	public GameEventManager events = null;
	public ScoreboardManager scoreboard = null;
	public MapManager maps = null;
	public CommandManager commands = null;
	public ItemManager items = new ItemManager(this);
	public EntityManager entities = null;
	public PermissionsManager permissions = null;
	public GameMetadata metadata = null;

	public String serverDate = null;
	private EventListeners eventListeners = null;

	private BukkitTask startTimer = null;
	public int secondsTilStart = 10;

	private Random random = null;

	// [Functions] TODO: Move all these into a class?
	private CrossbowUtils.Function<List<Winnable>> getWinners = Functions.playerWinner;

	@SuppressWarnings("deprecation")
	Crossbow(JavaPlugin plugin) {
		this.plugin = plugin;
		this.instance = this;

		new CrossbowUtils(this);
		new CrossbowGUI(this);
	}

	public Random getRandom() {
		if (random == null)
			random = new Random();
		return random;
	}

	public static Crossbow getInstance() {
		return instance;
	}

	public JavaPlugin getPlugin() {
		return plugin;
	}

	public void setGameMetadata(GameMetadata metadata) {
		this.metadata = metadata;
	}

//	public void setTimeout(Class<? extends GameTimeout> timeout) {
//		activeTimeout.end();
//		activeTimeout = null;
//		
//		if(timeout == null) {
//			return;
//		}
//		
//		GameTimeout timeoutInstance = null;
//		try {
//			timeoutInstance = timeout.getConstructor().newInstance();
//		} catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException
//				| NoSuchMethodException | SecurityException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		
//		activeTimeout = timeoutInstance;
//		activeTimeout.start();
//	}
//	
//	public GameTimeout getTimeout() {
//		return activeTimeout;
//	}

	// [Functions] TODO: Move all these into a class?
	public void setWinningFunction(CrossbowUtils.Function<List<Winnable>> getWinners) {
		this.getWinners = getWinners;
	}

	public List<Winnable> getWinners() {
		return getWinners.invoke();
	}

	public GameState getGameState() {
		return gameState;
	}

	public void setGameState(GameState gameState) {
		GameStateEvent event = events.emit(new GameStateEvent(this.gameState, gameState));
		this.gameState = gameState;
		scoreboard.updateAll();

		if (!event.isCancelled()) {
			ArrayList<GamePlayer> aplayers = players.all();
			for (GamePlayer player : aplayers) {
				if (player.isConnected()) {
					player.getPlayer()
							.setHealth(player.getPlayer().getAttribute(Attribute.GENERIC_MAX_HEALTH).getValue());
					player.getPlayer().setFoodLevel(20);
				}
			}

			switch (gameState) {
			case POSTGAME:
				new BukkitRunnable() {
					@Override
					public void run() {
						getPlugin().getLogger().info("Game has ended; resetting");
						events.emit(new GameEndEvent());
						setGameState(GameState.LOBBY);
						cleanup();
					}
				}.runTaskLater(plugin, 20L * 10);
				break;
			default:
			}
		}
	}

	void initialize() {
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy");
		LocalDateTime now = LocalDateTime.now();
		serverDate = dtf.format(now);

		permissions.initialize();
		events.initialize();
		players.initialize();
		scoreboard.initialize();
		maps.initialize();
		commands.initialize();
		items.initialize();

		events.registerEvents(this);

		eventListeners = new EventListeners(this);
		plugin.getServer().getPluginManager().registerEvents(eventListeners, plugin);

		if (plugin.getServer().getPluginManager().isPluginEnabled("ServerMetaAPI")) {
			ServerMetaAPI.getInstance().setDisplayName(metadata.getDisplayName(DisplayType.SMA));
		}

		try {
			// register default events
			events.registerComponentHook(GameState.POSTGAME, Fireworks.class);
			events.registerComponentHook(GameState.POSTGAME, AnnounceWinners.class);
			events.registerComponentHook(GameEndEvent.class, ResetMap.class);
			events.registerComponentHook(GameEndEvent.class, ClearPlayersInventories.class);
			events.registerComponentHook(GamePlayerDamageEvent.class, NoFriendlyFire.class);
			events.registerComponentHook(GamePlayerDeathEvent.class, InstantRespawn.class);
		} catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException
				| NoSuchMethodException | SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		setGameState(GameState.LOBBY);
	}

	/**
	 * Start the game
	 * 
	 * @param timeTilStart If above -1, specify seconds til start
	 * @throws NoMapSelectedException
	 */
	public void startGame(int timeTilStart) throws NoMapSelectedException, EventCanceledException {
		if (MapConfiguration.hasInstance()) {
			throw new EventCanceledException("Map configuration in progress");
		}

		if (maps.getMap() == null) {
			throw new NoMapSelectedException();
		}

		GameStartEvent ev = events.emit(new GameStartEvent());
		if (ev.isCancelled()) {
			throw new EventCanceledException(ev.getCancelledReason());
		}

		setGameState(GameState.PREGAME);

		if (timeTilStart > -1) {
			secondsTilStart = timeTilStart;
		} else {
			secondsTilStart = 10;
		}

		startTimer = new BukkitRunnable() {
			@Override
			public void run() {
				secondsTilStart--;
				scoreboard.updateAll();

				if (secondsTilStart <= 0) {
					startTimer.cancel();
					startTimer = null;
					setGameState(GameState.GAME);
				}
			}
		}.runTaskTimer(plugin, 0L, 20L);
	}

	@GameEventHandler
	public void on(GamePlayerDestroyEvent event) {
		if (getGameState() == GameState.GAME) {
			// if game is currently running, check that there is a winner
			// opponent teams may be empty at this time after player gets removed from all
			// references

			List<CrossbowTeam> validTeams = players.getTeams().stream().filter(t -> t.getPlayers().size() > 0)
					.collect(Collectors.toList());

			if (validTeams.size() < 2) {
				if (validTeams.size() == 1) {
					validTeams.get(0).setWinner(true);
				}
				setGameState(GameState.POSTGAME);
			}
		}
	}

	@GameEventHandler
	public void on(MapChangeEvent.Pre event) {
		players.broadcast("&8&oMap changing...");
		if (startTimer != null) {
			startTimer.cancel();
			startTimer = null;
		}
		setGameState(GameState.LOBBY);
	}

	@GameEventHandler
	public void on(MapChangeEvent.Post event) {
		players.broadcast("&8&oMap changed.");

		if (MapConfiguration.hasInstance()) {
			players.broadcast("&eWarning; Map configuration enabled.");

			for (GamePlayer player : players.<GamePlayer>all()) {
				if (MapConfiguration.hasInstance(player)) {
					player.getPlayer().setGameMode(GameMode.CREATIVE);
				} else {
					player.getPlayer().setGameMode(GameMode.SPECTATOR);
				}
			}
			return;
		}

		if (event.getMap() == null) {
			if (startTimer != null) {
				startTimer.cancel();
				startTimer = null;
			}
			setGameState(GameState.LOBBY);
		} else {
			if (players.connected().size() >= metadata.getPlayers()[0]) {
				if (gameState == GameState.LOBBY) {
					try {
						startGame(-1);
					} catch (NoMapSelectedException e) {
						players.broadcast("&cCan't start game; no map selected");
					} catch (EventCanceledException e) {
						if (e.getMessage() == null) {
							players.broadcast("&cFailed to start game &8&oEvent Canceled with no reason");
						} else {
							players.broadcast("&cFailed to start game; " + e.getMessage());
						}
					}
				}
			}
		}
	}

	// TODO: move these to a different (maybe toggleable) class
	@GameEventHandler(priority = EventPriority.LOW)
	public void on(GamePlayerJoinEvent event) {
		event.getPlayer().getPlayer().setGameMode(GameMode.ADVENTURE);

		if (MapConfiguration.hasInstance()) {
			event.getPlayer().sendMessage("&eMap configuration in progress.");
			return;
		}

		if ((gameState == GameState.GAME || gameState == GameState.POSTGAME) && !event.isRejoin()) {
			event.setCancelled(true, "Game already started");
			return;
		}

		// meets minimum
		if (players.connected().size() >= metadata.getPlayers()[0]) {
			if (gameState == GameState.LOBBY) {
				try {
					startGame(-1);
				} catch (NoMapSelectedException e) {
					players.broadcast("&cCan't start game; no map selected");
				} catch (EventCanceledException e) {
					if (e.getMessage() == null) {
						players.broadcast("&cFailed to start game &8&oEvent Canceled with no reason");
					} else {
						players.broadcast("&cFailed to start game; " + e.getMessage());
					}
				}
			}
		} else if (players.size() > metadata.getPlayers()[1]) { // exceeds max players
			event.setCancelled(true);
		}

		scoreboard.updateAll();
	}

	@GameEventHandler
	public void on(GamePlayerDisconnectEvent event) {
		if (MapConfiguration.hasInstance(event.getPlayer())) {
			MapConfiguration.getInstance(event.getPlayer()).destroy();
			return;
		}

		if (gameState == GameState.PREGAME) {
			if (players.connected().size() < metadata.getPlayers()[0]) {
				setGameState(GameState.LOBBY);

				if (startTimer != null) {
					startTimer.cancel();
					startTimer = null;
				}
			}
		}
		if (gameState == GameState.GAME) {
			if (players.connected().size() <= 0) {
				// no players connected to the game server
				events.emit(new GameEndEvent());
				setGameState(GameState.LOBBY);
			}
		}
		scoreboard.updateAll();
	}

	/**
	 * Called in {@link JavaPlugin#onDisable()} to cleanup all listeners
	 */
	public void shutdown() {
		HandlerList.unregisterAll(eventListeners);
		maps.destroy();
		entities.cleanup();

		if (startTimer != null) {
			startTimer.cancel();
		}
	}

	/**
	 * Reset game
	 */
	public void cleanup() {
		players.cleanup();
		maps.destroy();

		events.catchup();
	}

	public static class Builder {
		Crossbow build;

		public Builder(JavaPlugin plugin) {
			build = new Crossbow(plugin);
		}

		/**
		 * 
		 * @param <T>
		 * @param gamePlayer          a class that extends
		 *                            {@link com.grahhnt.Crossbow.Players.GamePlayer} to
		 *                            allow for games to store custom player data
		 * @param teamClass           a class that extends {@link CrossbowTeam} to allow
		 *                            for games to store custom team data
		 * @param destroyOnDisconnect destroy GamePlayer instance upon disconnection,
		 *                            preventing rejoins from being possible
		 * @return
		 */
		public Builder playerManager(Class<? extends GamePlayer> gamePlayer, Class<? extends CrossbowTeam> teamClass,
				boolean destroyOnDisconnect) {
			build.players = new PlayerManager(build, gamePlayer, teamClass, destroyOnDisconnect);
			return this;
		}

		public Builder kitManager() {
			build.kits = new KitManager(build);
			return this;
		}

		public Builder eventManager() {
			build.events = new GameEventManager(build);
			return this;
		}

		public Builder scoreboardManager() {
			build.scoreboard = new ScoreboardManager(build);
			return this;
		}

		public Builder mapManager() {
			build.maps = new MapManager(build);
			return this;
		}

		public Builder commandManager() {
			build.commands = new CommandManager(build);
			return this;
		}

		public Builder permissionsManager(String prefix) {
			build.permissions = new PermissionsManager(build, prefix);
			return this;
		}

		/**
		 * Used for game settings and also to be published on ServerMetaAPI if present
		 * and loaded (add to plugin.yml)
		 * 
		 * @param metadata
		 * @return
		 */
		public Builder metadata(GameMetadata metadata) {
			build.metadata = metadata;
			return this;
		}

		/**
		 * Initialize NPC manager. Citizens is required to be enabled before this is
		 * called (add Citizens to (soft)depend in plugin.yml)
		 * 
		 * @return
		 */
		public Builder npcs() {
			if (!build.plugin.getServer().getPluginManager().isPluginEnabled("Citizens")) {
				throw new RuntimeException("Failed to initialize NPCs; Citizens plugin not found or not enabled");
			}
			build.entities = new EntityManager(build);
			return this;
		}

		public Crossbow build() {
			if (build.metadata == null) {
				build.metadata = new GameMetadata();
			}

			if (build.players == null || build.events == null || build.scoreboard == null || build.maps == null
					|| build.commands == null || build.kits == null || build.permissions == null) {
				throw new RuntimeException("Didn't configure a section.");
			}

			build.initialize();
			return build;
		}
	}
}
