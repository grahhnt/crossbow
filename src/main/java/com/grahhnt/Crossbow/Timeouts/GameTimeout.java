package com.grahhnt.Crossbow.Timeouts;

public interface GameTimeout {
	public String getName();
	
	public void start();
	public void end();
}
