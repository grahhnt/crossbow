package com.grahhnt.Crossbow;

public class GameMetadata {
	private String name = "Unnamed Game";
	private int minimumPlayers = 0;
	private int maximumPlayers = 0;
	
	private String sidebarFooter = null;

	public String getName() {
		return name;
	}

	public int[] getPlayers() {
		return new int[] { minimumPlayers, maximumPlayers };
	}

	public enum DisplayType {
		SCOREBOARD,
		/**
		 * ServerMetadataAPI
		 */
		SMA
	}

	public String getDisplayName(DisplayType type) {
		switch (type) {
		case SCOREBOARD:
			return CrossbowUtils.format("&e&l" + name);
		default:
			return name;
		}
	}
	
	public String getSidebarFooter() {
		return sidebarFooter;
	}

	public static class Builder {
		private GameMetadata build;

		public Builder() {
			build = new GameMetadata();
		}

		public Builder name(String name) {
			build.name = name;
			return this;
		}

		public Builder players(int min, int max) {
			build.minimumPlayers = min;
			build.maximumPlayers = max;
			return this;
		}
		
		public Builder sidebar(String footer) {
			build.sidebarFooter = footer;
			return this;
		}

		public GameMetadata build() {
			return build;
		}
	}
}
