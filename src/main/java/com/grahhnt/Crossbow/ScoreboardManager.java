package com.grahhnt.Crossbow;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.bukkit.Bukkit;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Scoreboard;

import com.grahhnt.Crossbow.Crossbow.GameState;
import com.grahhnt.Crossbow.GameMetadata.DisplayType;
import com.grahhnt.Crossbow.Players.GamePlayer;
import com.grahhnt.Crossbow.Events.GameEventHandler;
import com.grahhnt.Crossbow.Events.GameListener;
import com.grahhnt.Crossbow.Events.Game.GameScoreboardUpdateEvent;
import com.grahhnt.Crossbow.Events.Game.GameStateEvent;
import com.grahhnt.Crossbow.Events.Player.GamePlayerJoinEvent;
import com.grahhnt.Crossbow.Events.Player.GamePlayerScoreboardUpdateEvent;

public class ScoreboardManager implements GameListener {
	private Crossbow game;
	private org.bukkit.scoreboard.ScoreboardManager sbmanager = Bukkit.getScoreboardManager();

	public boolean showHealth = true;

	ScoreboardManager(Crossbow game) {
		this.game = game;
	}

	public void initialize() {
		game.events.registerEvents(this);
	}

	@GameEventHandler
	public void on(GamePlayerJoinEvent event) {
		if (event.getPlayer().scoreboard == null) {
			event.getPlayer().scoreboard = new GPSBManager(event.getPlayer());
		} else {
			event.getPlayer().scoreboard.update();
		}
	}

	@GameEventHandler
	public void on(GameStateEvent event) {
		updateAll();
	}

	public List<String> getSidebar(GameState state, GamePlayer player) {
		ArrayList<String> lines = new ArrayList<>();

		switch (state) {
		case LOBBY:
			lines.add("&fWaiting for players...");
			lines.add("&c" + game.players.connected().size() + " &e/ " + game.metadata.getPlayers()[1] + " players");
			break;
		case PREGAME:
			lines.add("&aStarting in &e" + game.secondsTilStart + " seconds...");
			lines.add("&c" + game.players.connected().size() + " &e/ " + game.metadata.getPlayers()[1] + " players");
			break;
		case GAME:
			break;
		case POSTGAME:
			break;
		default:
			lines.add("&cUnknown Game State:");
			lines.add(state.toString());
		}

		return lines;
	}

	public void updateAll() {
		ArrayList<GamePlayer> players = game.players.connected();
		for (GamePlayer player : players) {
			if (player.scoreboard == null) {
				game.getPlugin().getLogger().warning(player.getUsername() + " has no scoreboard?");
			} else {
				player.scoreboard.update();
			}
		}
	}

	/**
	 * GamePlayer Scoreboard Manager
	 * 
	 * @author grant
	 *
	 */
	public class GPSBManager {
		private GamePlayer player;

		Scoreboard sb = null;
		ArrayList<Objective> objectives = new ArrayList<>();
		ArrayList<org.bukkit.scoreboard.Team> teams = new ArrayList<>();

		public GPSBManager(GamePlayer player) {
			this.player = player;
			initialize();
		}

		void initialize() {
			sb = sbmanager.getNewScoreboard();

			for (Objective o : objectives) {
				o.unregister();
			}
			objectives = new ArrayList<>();

			unregisterIfExist("game", "game2", "health", "health2");

			if (showHealth) {
				Objective health = sb.registerNewObjective("health", "health", CrossbowUtils.format("&c\u2665"));
				health.setDisplaySlot(DisplaySlot.BELOW_NAME);
				objectives.add(health);

				Objective health2 = sb.registerNewObjective("health2", "health", "health2");
				health2.setDisplaySlot(DisplaySlot.PLAYER_LIST);
				objectives.add(health2);
			}

			update();
		}

		private void unregisterIfExist(String... objnames) {
			for (String objname : objnames) {
				if (sb.getObjective(objname) != null) {
					sb.getObjective(objname).unregister();
				}
			}
		}

		private Objective getGameObj() {
			Objective obj = null;

			for (Objective o : objectives) {
				if (obj != null)
					continue;

				if (o.getName().startsWith("game")) {
					obj = o;
				}
			}

			return obj;
		}

		public void update() {
			if (!player.isConnected())
				return;

			Objective oldObj = getGameObj();
			String newObjName = oldObj != null && oldObj.getName().equals("game") ? "game2" : "game";

			Objective objective = sb.registerNewObjective(newObjName, "dummy",
					game.metadata.getDisplayName(DisplayType.SCOREBOARD));

			// remove old scores
			// can remove heart values and cause flickering
			for (String s : sb.getEntries()) {
				sb.resetScores(s);
			}

			if (showHealth) {
				for (Objective obj : objectives) {
					if (obj.getName().startsWith("health")) {
						ArrayList<GamePlayer> players = game.players.connected();
						for (GamePlayer gp : players) {
							obj.getScore(gp.getUsername()).setScore((int) gp.getHealth());
						}
					}
				}
			}

			// maximum amount of lines that you can show on the sidebar
			int maxLines = 16;

			// header not toggleable rn, but it takes 2 lines
			maxLines -= 2;

			if (game.metadata.getSidebarFooter() != null)
				maxLines -= 2;

			ArrayList<String> header = new ArrayList<String>(List.of("&7" + game.serverDate));
			ArrayList<String> items = new ArrayList<String>(
					getSidebar(game.getGameState(), player).stream().limit(maxLines).collect(Collectors.toList()));
			ArrayList<String> footer = game.metadata.getSidebarFooter() == null ? new ArrayList<String>()
					: new ArrayList<String>(List.of(game.metadata.getSidebarFooter()));

			GamePlayerScoreboardUpdateEvent ev = game.events
					.emit(new GamePlayerScoreboardUpdateEvent(header, items, footer));
			if (ev.isCancelled() || !ev.getHasChanged()) {
				GameScoreboardUpdateEvent ev2 = game.events.emit(new GameScoreboardUpdateEvent(header, items, footer));
				header = new ArrayList<String>(ev2.getHeader());
				items = new ArrayList<String>(ev2.getItems());
				footer = new ArrayList<String>(ev2.getFooter());
			} else {
				header = new ArrayList<String>(ev.getHeader());
				items = new ArrayList<String>(ev.getItems());
				footer = new ArrayList<String>(ev.getFooter());
			}

			if (header.size() > 0 && header.get(header.size() - 1) != null)
				header.add(null);
			if (footer.size() > 0 && footer.get(0) != null)
				footer.add(0, null);

			items.addAll(0, header);
			items.addAll(footer);

			int i = items.size();
			String placeholder = "";
			for (String s : items) {
				if (s == null) {
					placeholder += "&a";
					s = placeholder;
				}

				objective.getScore(CrossbowUtils.format(s)).setScore(i);
				i--;
			}

			objective.setDisplaySlot(DisplaySlot.SIDEBAR);

			// attempt to remove flickering
			if (oldObj != null) {
				oldObj.unregister();
				objectives.remove(oldObj);
			}

			objectives.add(objective);
			player.getPlayer().setScoreboard(sb);
		}
	}
}
