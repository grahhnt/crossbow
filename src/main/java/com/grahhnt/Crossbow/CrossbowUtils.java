package com.grahhnt.Crossbow;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ThreadLocalRandom;
import java.util.function.Consumer;
import java.util.stream.Stream;

import org.bukkit.Color;
import org.bukkit.FireworkEffect;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Firework;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.FireworkMeta;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitTask;

import com.grahhnt.Crossbow.Items.CrossbowItem;
import com.grahhnt.Crossbow.Items.VanillaCrossbowItem;
import com.grahhnt.Crossbow.Map.RegionMapEntity;
import com.grahhnt.Crossbow.Players.GamePlayer;
import com.sk89q.worldedit.math.BlockVector3;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.ComponentBuilder.FormatRetention;
import net.md_5.bungee.api.chat.hover.content.Text;

public class CrossbowUtils {
	private static Crossbow game;

	CrossbowUtils(Crossbow instance) {
		game = instance;
	}

	public static interface Function<R> {
		R invoke();
	}

	public static String format(String str) {
		if (str == null)
			return null;
		return ChatColor.translateAlternateColorCodes('&', str);
	}

	public static String getPlayersAsList(List<GamePlayer> players) {
		ArrayList<String> displayNames = new ArrayList<>();

		for (GamePlayer player : players) {
			displayNames.add(player.getDisplayName());
		}

		return Arrays.toString(displayNames.toArray());
	}

	public static Firework spawnRandomFirework(Location loc) {
		Firework fw = (Firework) loc.getWorld().spawnEntity(loc, EntityType.FIREWORK);
		FireworkMeta fwm = fw.getFireworkMeta();
		fwm.setPower(0);

		Color[] colors = new Color[] { Color.AQUA, Color.BLUE, Color.FUCHSIA, Color.GREEN, Color.LIME, Color.MAROON,
				Color.NAVY, Color.OLIVE, Color.ORANGE, Color.PURPLE, Color.RED, Color.TEAL, Color.YELLOW };
		fwm.addEffect(FireworkEffect.builder()
				.withColor(List.of(colors).get(ThreadLocalRandom.current().nextInt(colors.length))).build());

		fw.setFireworkMeta(fwm);
		fw.detonate();
		return fw;
	}

	public static void copyDirectory(String sourceDirectoryLocation, String destinationDirectoryLocation)
			throws IOException {
		Files.walk(Paths.get(sourceDirectoryLocation)).forEach(source -> {
			Path destination = Paths.get(destinationDirectoryLocation,
					source.toString().substring(sourceDirectoryLocation.length()));
			try {
				Files.copy(source, destination);
			} catch (IOException e) {
				e.printStackTrace();
			}
		});
	}

	// https://stackoverflow.com/a/29175213/11138243
	public static void deleteDir(File file) {
		File[] contents = file.listFiles();
		if (contents != null) {
			for (File f : contents) {
				if (!Files.isSymbolicLink(f.toPath())) {
					deleteDir(f);
				}
			}
		}
		file.delete();
	}

	/**
	 * Fill area in with blocks
	 * 
	 * @param loc1
	 * @param loc2
	 * @param material
	 */
	public static void fill(Location loc1, Location loc2, Material material) {
		loopOverArea(loc1, loc2, location -> {
			location.getBlock().setType(material);
		});
	}

	public static Location getLocation(Map<String, Object> data, World world) {
		Location l = getLocation(data);
		l.setWorld(world);
		return l;
	}

	public static Location getLocation(Map<String, Object> data) {
		double x = Double.valueOf(data.get("x").toString());
		double y = Double.valueOf(data.get("y").toString());
		double z = Double.valueOf(data.get("z").toString());
		float pitch = Float.valueOf(data.get("pitch").toString());
		float yaw = Float.valueOf(data.get("yaw").toString());
		return new Location(null, x, y, z, pitch, yaw);
	}

	/**
	 * Get lowest point and highest point
	 * 
	 * If point1 has world, returned Locations will have World set
	 * 
	 * @param point1
	 * @param point2
	 * @return First index will be lower point, second highest
	 */
	public static Location[] lowerHigherCoords(Location point1, Location point2) {
		World world = point1.getWorld();

		double x1 = point1.getX();
		double y1 = point1.getY();
		double z1 = point1.getZ();

		double x2 = point2.getX();
		double y2 = point2.getY();
		double z2 = point2.getZ();

		// lower coords
		double xx = x1 < x2 ? x1 : x2;
		double yy = y1 < y2 ? y1 : y2;
		double zz = z1 < z2 ? z1 : z2;
		// higher coords
		double xxx = x1 < x2 ? x2 : x1;
		double yyy = y1 < y2 ? y2 : y1;
		double zzz = z1 < z2 ? z2 : z1;

		return new Location[] { new Location(world, xx, yy, zz), new Location(world, xxx, yyy, zzz) };
	}

	public static boolean isLocationInArea(Location point1, Location point2, Location needle) {
		if (needle.getWorld() != point1.getWorld())
			return false;

		Location low = lowerHigherCoords(point1, point2)[0];
		Location high = lowerHigherCoords(point1, point2)[1];

		// lower coords
		double xx = low.getX();
		double yy = low.getY();
		double zz = low.getZ();
		// higher coords
		double xxx = high.getX();
		double yyy = high.getY();
		double zzz = high.getZ();

		if (xx <= needle.getX() && needle.getX() <= xxx) {
			if (yy <= needle.getY() && needle.getY() <= yyy) {
				if (zz <= needle.getZ() && needle.getZ() <= zzz) {
					return true;
				}
			}
		}

		return false;
	}
	
	private static ArrayList<BukkitTask> tasks = new ArrayList<>();
	
	/**
	 * Clear timed tasks (eg. CrossbowUtils{@link #countdown(int, Consumer, Consumer)})
	 */
	public static void clearTasks() {
		for(BukkitTask task : tasks) {
			if(!task.isCancelled()) {
				task.cancel();				
			}
		}
		
		tasks.clear();
	}

	public static BukkitTask countdown(int seconds, Consumer<Integer> eachSecond, Consumer<Integer> finish) {
		eachSecond.accept(seconds);

		BukkitTask task = new BukkitRunnable() {
			int secondsTilFinish = seconds;

			@Override
			public void run() {
				secondsTilFinish--;

				if (secondsTilFinish <= 0) {
					cancel();
					finish.accept(seconds);
				} else {
					eachSecond.accept(secondsTilFinish);
				}
			}
		}.runTaskTimer(Crossbow.getInstance().getPlugin(), 20L, 20L);
		
		tasks.add(task);
		
		return task;
	}

	public static Stream<Location> areaBlockStream(Location point1, Location point2) {
		ArrayList<Location> blocks = new ArrayList<Location>();
		Location low = lowerHigherCoords(point1, point2)[0];
		Location high = lowerHigherCoords(point1, point2)[1];

		for (double x = low.getX(); x <= high.getX(); x++) {
			for (double y = low.getY(); y <= high.getY(); y++) {
				for (double z = low.getZ(); z <= high.getZ(); z++) {
					blocks.add(new Location(point1.getWorld(), x, y, z));
				}
			}
		}

		return blocks.stream();
	}

	public static Stream<Location> areaBlockStream(RegionMapEntity entity) {
		return areaBlockStream(entity.getFirstPoint(null), entity.getSecondPoint(null));
	}

	public static void loopOverArea(Location point1, Location point2, Consumer<Location> consumer) {
		areaBlockStream(point1, point2).forEach(consumer);
	}

	public static void loopOverArea(RegionMapEntity entity, Consumer<Location> consumer) {
		loopOverArea(entity.getFirstPoint(null), entity.getSecondPoint(null), consumer);
	}

	public static <T> T randomElement(List<T> list) {
		return list.get(game.getRandom().nextInt(list.size()));
	}

	/**
	 * Replaces ALL lower quality tools (by material) with the new item
	 * 
	 * @param inventory
	 * @param replacer
	 */
	public static void upgradeItems(Inventory inventory, CrossbowItem replacer) {
		VanillaCrossbowItem vreplacer = VanillaCrossbowItem.get(replacer.getType());
		int slot = -1;
		for (ItemStack item : inventory.getContents()) {
			slot++;
			if (item != null) {
				VanillaCrossbowItem vitem = VanillaCrossbowItem.get(item.getType());
				if (vreplacer.isBetter(vitem)) {
					inventory.setItem(slot, replacer);
				}
			}
		}
	}

	public static Location getCenteredLocation(Location location) {
		float yaw = location.getYaw();
		float pitch = location.getPitch();

		Location newLocation = location.clone().getBlock().getLocation().add(0.5, 0, 0.5);
		newLocation.setYaw(yaw);
		newLocation.setPitch(pitch);
		return newLocation;
	}

	public static String getTeleportFromLocation(Location location) {
		return location.getX() + " " + location.getY() + " " + location.getZ() + " " + location.getYaw() + " "
				+ location.getPitch();
	}

	public static class Chat {
		public static class Button {
			String text;
			ChatColor color;
			String tooltip = null;
			ClickEvent clickEvent = null;

			public Button(String text, ChatColor color) {
				this.text = text;
				this.color = color;
			}

			public Button setTooltip(String tooltip) {
				this.tooltip = tooltip;
				return this;
			}

			public Button setClick(ClickEvent clickEvent) {
				this.clickEvent = clickEvent;
				return this;
			}

			public BaseComponent[] build() {
				ComponentBuilder component = new ComponentBuilder("[" + text + "]").color(color).bold(true);
				if (tooltip != null) {
					component.event(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new Text(tooltip)));
				}
				if (clickEvent != null) {
					component.event(clickEvent);
				}
				return component.create();
			}
		}

		public static BaseComponent[] buttons(Button... buttons) {
			ComponentBuilder component = new ComponentBuilder();
			component.append(new ComponentBuilder("").reset().create());
			component.retain(FormatRetention.NONE);

			for (Button button : buttons) {
				component.append(button.build());
				component.append(" ");
			}

			return component.create();
		}
	}

//	public static PlayerProfile getPlayerProfileForTexture() {
//		PlayerProfile profile = Bukkit.getServer().createPlayerProfile(UUID.randomUUID());
//		profile.setTextures(textures);
//	}
}
