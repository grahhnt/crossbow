package com.grahhnt.Crossbow.Map;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;

import org.bukkit.Location;
import org.bukkit.World;

import com.grahhnt.Crossbow.Map.MapEntity.EditorDescriptor.ConfigValue;

public abstract class SinglePointMapEntity extends MapEntity {
	protected double x;
	protected double y;
	protected double z;
	protected float pitch = 0;
	protected float yaw = 0;

	protected SinglePointMapEntity(String id) {
		super(id);
		// TODO Auto-generated constructor stub
	}

	protected void populateCoords(Map<String, Object> data) {
		x = Double.valueOf(data.get("x").toString());
		y = Double.valueOf(data.get("y").toString());
		z = Double.valueOf(data.get("z").toString());
		if (data.containsKey("pitch"))
			pitch = Float.valueOf(data.get("pitch").toString());
		if (data.containsKey("yaw"))
			yaw = Float.valueOf(data.get("yaw").toString());
	}

	/**
	 * Get location instance from world specified
	 * 
	 * @param world
	 * @return
	 */
	public Location getLocation(World world) {
		return new Location(world, x, y, z, pitch, yaw);
	}

	public void setLocation(Location location) {
		x = location.getX();
		y = location.getY();
		z = location.getZ();
		pitch = location.getPitch();
		yaw = location.getYaw();
	}

	@Override
	protected LinkedHashMap<String, Object> serializeBase() {
		LinkedHashMap<String, Object> serial = super.serializeBase();

		serial.put("x", x);
		serial.put("y", y);
		serial.put("z", z);
		serial.put("pitch", pitch);
		serial.put("yaw", yaw);

		return serial;
	}

	@Override
	public EditorDescriptor getEditorDescriptor() {
		EditorDescriptor descriptor = super.getEditorDescriptor();

		descriptor.addConfigValues(
				new ConfigValue.LocationValue("location", new Location(null, x, y, z, yaw, pitch), null, true)
						.source(ConfigValue.Source.PARENT));

		return descriptor;
	}
}
