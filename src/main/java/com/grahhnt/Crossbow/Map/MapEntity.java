package com.grahhnt.Crossbow.Map;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;

import com.grahhnt.Crossbow.Crossbow;
import com.grahhnt.Crossbow.Map.Config.MapConfiguration;
import com.grahhnt.Crossbow.Players.CrossbowTeam;
import com.grahhnt.Crossbow.Players.GamePlayer;
import com.sk89q.worldedit.IncompleteRegionException;
import com.sk89q.worldedit.LocalSession;
import com.sk89q.worldedit.WorldEdit;
import com.sk89q.worldedit.bukkit.BukkitAdapter;
import com.sk89q.worldedit.regions.Region;

/**
 * Specifies what a MapEntity is
 * 
 * @author grant
 *
 */
public abstract class MapEntity {
	/**
	 * Unique, full caps, identifier for this entity
	 */
	String id;

	protected MapEntity(String id) {
		this.id = id;
	}

	public String getID() {
		return id;
	}

	protected LinkedHashMap<String, Object> serializeBase() {
		LinkedHashMap<String, Object> serial = new LinkedHashMap<>();

		serial.put("type", id);

		return serial;
	}

	/**
	 * Convert MapEntity data into a Map used for saving to config
	 * 
	 * @return
	 */
	public abstract Map<String, Object> serialize();

	/**
	 * Convert config into current MapEntity instance
	 * 
	 * @param data
	 */
	public abstract void deserialize(Map<String, Object> data);

	/**
	 * Spawn MapEntity in with custom logic
	 */
	public abstract void spawn(World world);

	/**
	 * Perform entity cleanup
	 */
	public abstract void destroy();

	/**
	 * Description object for editor usage
	 * 
	 * @return
	 */
	public EditorDescriptor getEditorDescriptor() {
		return new EditorDescriptor(this, null, null, null);
	}

	@Override
	public String toString() {
		return "MapEntity [id=" + id + "]";
	}

	public static class EditorDescriptor {
		String id = null;
		String friendlyName = null;
		Class<? extends MapEntity> parentClass = MapEntity.class;
		Material editorItem = Material.BLAZE_SPAWN_EGG;
		ArrayList<ConfigValue> config = new ArrayList<ConfigValue>();

		EditorDescriptor(MapEntity entity, String friendlyName, Class<? extends MapEntity> parentClass,
				Material editorItem) {
			id = entity.id;

			if (friendlyName == null) {
				this.friendlyName = entity.id;
			} else {
				this.friendlyName = friendlyName;
			}

			if (editorItem != null)
				this.editorItem = editorItem;

			if (parentClass == null) {
				if (entity instanceof SinglePointMapEntity) {
					this.parentClass = SinglePointMapEntity.class;
				} else if (entity instanceof RegionMapEntity) {
					this.parentClass = RegionMapEntity.class;
				}
			} else {
				this.parentClass = parentClass;
			}
		}

		public void addConfigValues(ConfigValue... config) {
			this.config.addAll(List.of(config));
		}

		public LinkedHashMap<String, Object> serialize() {
			LinkedHashMap<String, Object> serial = new LinkedHashMap<>();

			for (ConfigValue<?> c : config) {
				serial.putAll(c.serialize());
			}
			
			Bukkit.broadcastMessage(serial.toString());

			return serial;
		}

		public List<ConfigValue> getConfigValues() {
			return config;
		}

		public String getID() {
			return id;
		}

		public String getName() {
			return friendlyName;
		}

		public Class<? extends MapEntity> getParentClass() {
			return parentClass;
		}

		public Material getEditorItem() {
			return editorItem;
		}

		public static abstract class ConfigValue<T> {
			String key;
			T value;
			T defaultValue;
			boolean required;
			Source source = Source.ENTITY;

			public enum Source {
				/**
				 * This ConfigValue comes from the MapEntity (guest minigame)
				 */
				ENTITY,
				/**
				 * This ConfigValue comes from the parent (eg. SinglePointMapEntity)
				 */
				PARENT
			}

			public ConfigValue(String key, T value, T defaultValue, boolean required) {
				this.key = key;
				this.value = value;
				this.defaultValue = defaultValue;
				this.required = required;
			}

			public ConfigValue<T> source(Source source) {
				this.source = source;
				return this;
			}

			public Source getSource() {
				return source;
			}

			/**
			 * ConfigValue friendly name
			 * 
			 * @return
			 */
			public String getFriendlyName() {
				return null;
			}

			public String getKey() {
				return key;
			}

			public T getDefaultValue() {
				return defaultValue;
			}

			public T getValue() {
				return value;
			}

			public void setValue(T value) {
				this.value = value;
			}

			public boolean isRequired() {
				return required;
			}

			public abstract CompletableFuture<T> captureInput(GamePlayer player);

			public HashMap<String, Object> serialize() {
				HashMap<String, Object> serial = new HashMap<>();
				serial.put(getKey(), getValue() == null ? getDefaultValue() : getValue());
				return serial;
			}

			public static class StringValue extends ConfigValue<String> {
				public StringValue(String key, String value, String defaultValue, boolean required) {
					super(key, value, defaultValue, required);
				}

				@Override
				public String getFriendlyName() {
					return "String";
				}

				@Override
				public CompletableFuture<String> captureInput(GamePlayer player) {
					player.sendMessage("&aSend in chat your value for &7" + key);

					MapConfiguration config = MapConfiguration.getInstance(player);
					return config.captureChatInput();
				}
			}

			public static class IntegerValue extends ConfigValue<Integer> {
				public IntegerValue(String key, Integer value, Integer defaultValue, boolean required) {
					super(key, value, defaultValue, required);
				}

				@Override
				public String getFriendlyName() {
					return "Integer (whole number)";
				}

				@Override
				public CompletableFuture<Integer> captureInput(GamePlayer player) {
					player.sendMessage("&aSend in chat your value for &7" + key);

					MapConfiguration config = MapConfiguration.getInstance(player);
					return config.captureChatInput().thenApply(str -> Integer.valueOf(str));
				}
			}

			public static class FloatValue extends ConfigValue<Float> {
				public FloatValue(String key, Float value, Float defaultValue, boolean required) {
					super(key, value, defaultValue, required);
				}

				@Override
				public String getFriendlyName() {
					return "Float (decimal)";
				}

				@Override
				public CompletableFuture<Float> captureInput(GamePlayer player) {
					player.sendMessage("&aSend in chat your value for &7" + key);

					MapConfiguration config = MapConfiguration.getInstance(player);
					return config.captureChatInput().thenApply(str -> Float.valueOf(str));
				}
			}

			public static class LocationValue extends ConfigValue<Location> {
				public LocationValue(String key, Location value, Location defaultValue, boolean required) {
					super(key, value, defaultValue, required);
				}

				@Override
				public String getFriendlyName() {
					return "Location (x,y,z,pitch,yaw)";
				}

				@Override
				public CompletableFuture<Location> captureInput(GamePlayer player) {
					return CompletableFuture.completedFuture(player.getPlayer().getLocation());
				}

				@Override
				public LinkedHashMap<String, Object> serialize() {
					LinkedHashMap<String, Object> serial = new LinkedHashMap<>();

					Location l = getValue();
					serial.put("x", l.getX());
					serial.put("y", l.getY());
					serial.put("z", l.getZ());
					serial.put("pitch", l.getPitch());
					serial.put("yaw", l.getYaw());

					return serial;
				}
			}

			public static class RegionValue extends ConfigValue<Location[]> {
				public RegionValue(String key, Location[] value, boolean required) {
					super(key, value, null, required);
				}

				@Override
				public String getFriendlyName() {
					return "Region (Location -> Location)";
				}

				@Override
				public CompletableFuture<Location[]> captureInput(GamePlayer player) {
					LocalSession session = WorldEdit.getInstance().getSessionManager()
							.get(BukkitAdapter.adapt(player.getPlayer()));

					try {
						Region region = session.getSelection(session.getSelectionWorld());

						Location pos1 = BukkitAdapter.adapt(BukkitAdapter.adapt(session.getSelectionWorld()),
								region.getMinimumPoint());
						Location pos2 = BukkitAdapter.adapt(BukkitAdapter.adapt(session.getSelectionWorld()),
								region.getMaximumPoint());

						return CompletableFuture.completedFuture(new Location[] { pos1, pos2 });
					} catch (IncompleteRegionException e) {
						return CompletableFuture.failedFuture(e);
					}
				}

				@Override
				public LinkedHashMap<String, Object> serialize() {
					LinkedHashMap<String, Object> serial = new LinkedHashMap<>();

					Location[] l = getValue();
					for (int i = 0; i < l.length; i++) {
						serial.put("x" + (i + 1), l[i].getX());
						serial.put("y" + (i + 1), l[i].getY());
						serial.put("z" + (i + 1), l[i].getZ());
					}

					return serial;
				}
			}

			public static class TeamValue extends ConfigValue<CrossbowTeam> {
				public TeamValue(String key, CrossbowTeam value, boolean required) {
					super(key, value, null, required);
				}

				@Override
				public String getFriendlyName() {
					return "Team";
				}

				@Override
				public CompletableFuture<CrossbowTeam> captureInput(GamePlayer player) {
					player.sendMessage("&aSend in chat your value for &7" + key);
					player.sendMessage("&7Valid options: &f" + Crossbow.getInstance().players.getTeams().stream()
							.map(team -> team.getID()).collect(Collectors.toList()));

					MapConfiguration config = MapConfiguration.getInstance(player);
					return config.captureChatInput().thenApply(str -> Crossbow.getInstance().players.getTeam(str));
				}

				@Override
				public LinkedHashMap<String, Object> serialize() {
					LinkedHashMap<String, Object> serial = new LinkedHashMap<>();

					serial.put(getKey(), getValue().getID());

					return serial;
				}
			}
		}
	}
}
