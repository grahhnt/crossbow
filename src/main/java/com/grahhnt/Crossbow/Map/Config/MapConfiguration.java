package com.grahhnt.Crossbow.Map.Config;

import java.io.File;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;

import org.bukkit.Location;
import org.bukkit.NamespacedKey;
import org.bukkit.World;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.player.PlayerChatEvent;
import org.bukkit.persistence.PersistentDataType;

import com.grahhnt.Crossbow.Crossbow;
import com.grahhnt.Crossbow.CrossbowUtils;
import com.grahhnt.Crossbow.Exceptions.MapNotFoundException;
import com.grahhnt.Crossbow.Map.MapEntity;
import com.grahhnt.Crossbow.Map.MapManager.Map;
import com.grahhnt.Crossbow.Map.RegionMapEntity;
import com.grahhnt.Crossbow.Map.SinglePointMapEntity;
import com.grahhnt.Crossbow.Map.Config.Items.SaveMapItem;
import com.grahhnt.Crossbow.Map.Config.SpawnMapEntity.EntityEditItem;
import com.grahhnt.Crossbow.Players.GamePlayer;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.ComponentBuilder.FormatRetention;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.hover.content.Text;

public class MapConfiguration implements Listener {
	private static HashMap<GamePlayer, MapConfiguration> instances = new HashMap<>();

	public static MapConfiguration getInstance(GamePlayer player) {
		if (!instances.containsKey(player))
			instances.put(player, new MapConfiguration(player));
		return instances.get(player);
	}

	public static boolean hasInstance(GamePlayer player) {
		return instances.containsKey(player);
	}

	public static boolean hasInstance() {
		return instances.size() > 0;
	}

	GamePlayer player;
	Crossbow game;

	Map originalMap = null;
	EditableMap map = null;

	CompletableFuture<String> captureChatInput = null;

	MapEntity.EditorDescriptor editingEntity = null;
	MapEditorEntity activeEntity = null;

	MapConfiguration(GamePlayer player) {
		this.player = player;
		game = Crossbow.getInstance();

		game.getPlugin().getServer().getPluginManager().registerEvents(this, game.getPlugin());
	}

	public void destroy() {
		instances.remove(player);
		HandlerList.unregisterAll(this);
	}

	@EventHandler
	public void on(PlayerChatEvent event) {
		if (captureChatInput == null)
			return;

		event.setCancelled(true);
		captureChatInput.complete(event.getMessage());
		captureChatInput = null;
	}

	public CompletableFuture<String> captureChatInput() {
		CompletableFuture<String> future = new CompletableFuture<String>();
		captureChatInput = future;
		return future;
	}

	@EventHandler
	public void on(EntityDamageByEntityEvent event) {
		if (event.getEntity().getPersistentDataContainer() == null || !event.getEntity().getPersistentDataContainer()
				.has(NamespacedKey.fromString("crossbow:map_entity_id"), PersistentDataType.INTEGER)) {
			return;
		}

		if (!(event.getDamager() instanceof Player)) {
			return;
		}
		event.setCancelled(true);

		GamePlayer player = game.players.getPlayer((Player) event.getDamager());

		int entityID = event.getEntity().getPersistentDataContainer()
				.get(NamespacedKey.fromString("crossbow:map_entity_id"), PersistentDataType.INTEGER);
		MapEditorEntity entity = map.getMapEntities().get(entityID);

		activeEntity = entity;
		highlightEntity(entityID);
	}

	public BaseComponent[] clickableCoords(Location location) {
		DecimalFormat df = new DecimalFormat("#.##");

		return new ComponentBuilder(
				"[" + df.format(location.getX()) + ", " + df.format(location.getY()) + ", " + df.format(location.getZ())
						+ " (" + df.format(location.getYaw()) + "," + df.format(location.getPitch()) + ")]")
								.color(ChatColor.WHITE).underlined(true)
								.event(new HoverEvent(HoverEvent.Action.SHOW_TEXT,
										new Text("Click to teleport (centered on block)")))
								.event(new ClickEvent(ClickEvent.Action.RUN_COMMAND,
										"/tp " + player.getUsername() + " "
												+ CrossbowUtils.getTeleportFromLocation(
														CrossbowUtils.getCenteredLocation(location))))
								.retain(FormatRetention.NONE).create();
	}

	private void highlightEntity(int entityID) {
		MapEditorEntity entity = map.getMapEntities().get(entityID);

		for (Entity wentity : game.maps.getActiveWorld().getEntities()) {
			if (wentity.getPersistentDataContainer() != null
					&& wentity.getPersistentDataContainer().has(NamespacedKey.fromString("crossbow:map_entity_id"),
							PersistentDataType.INTEGER)
					&& wentity.getPersistentDataContainer().get(NamespacedKey.fromString("crossbow:map_entity_id"),
							PersistentDataType.INTEGER) == entityID) {
				wentity.setGlowing(true);
			} else {
				wentity.setGlowing(false);
			}
		}

		setEditingEntity(entity.getEntity().getEditorDescriptor());
//		player.getPlayer().performCommand("crossbow:map configure entity " + entityID);
	}

	private void removePreviewEntity(int entityID) {
		for (Entity wentity : game.maps.getActiveWorld().getEntities()) {
			if (wentity.getPersistentDataContainer() != null
					&& wentity.getPersistentDataContainer().has(NamespacedKey.fromString("crossbow:map_entity_id"),
							PersistentDataType.INTEGER)
					&& wentity.getPersistentDataContainer().get(NamespacedKey.fromString("crossbow:map_entity_id"),
							PersistentDataType.INTEGER) == entityID) {
				wentity.remove();
			}
		}
	}

	private ArmorStand[] getPreviewEntity(MapEditorEntity entity) {
		ArmorStand[] as = new ArmorStand[0];
		
		removePreviewEntity(entity.getID());

		if (entity.getEntity() instanceof SinglePointMapEntity) {
			as = new ArmorStand[] {
					getPreviewEntity(entity.getID(), entity, (SinglePointMapEntity) entity.getEntity()) };
		}

		if (entity.getEntity() instanceof RegionMapEntity) {
			as = getPreviewEntity(entity.getID(), entity, (RegionMapEntity) entity.getEntity());
		}

		return as;
	}

	private ArmorStand getPreviewEntity(int entityID, MapEditorEntity entity, SinglePointMapEntity mapEntity) {
		World world = game.maps.getActiveWorld();
		ArmorStand as = world.spawn(mapEntity.getLocation(world), ArmorStand.class);
		as.getPersistentDataContainer().set(NamespacedKey.fromString("crossbow:map_entity_id"),
				PersistentDataType.INTEGER, entityID);
		as.setCustomName(CrossbowUtils.format(entity.getEntity().getID() + (entity.isModified() ? "&4&l*" : "")
				+ (entity.getStatus() != null ? " " + entity.getStatus() : "")));
		as.setCustomNameVisible(true);
		as.setGravity(false);
		return as;
	}

	private ArmorStand[] getPreviewEntity(int entityID, MapEditorEntity entity, RegionMapEntity mapEntity) {
		World world = game.maps.getActiveWorld();

		ArmorStand as1 = world.spawn(CrossbowUtils.getCenteredLocation(mapEntity.getFirstPoint(world)),
				ArmorStand.class);
		as1.getPersistentDataContainer().set(NamespacedKey.fromString("crossbow:map_entity_id"),
				PersistentDataType.INTEGER, entityID);
		as1.setCustomName(CrossbowUtils.format(entity.getEntity().getID() + (entity.isModified() ? "&4&l*" : "")
				+ " &cPoint 1" + (entity.getStatus() != null ? " " + entity.getStatus() : "")));
		as1.setCustomNameVisible(true);
		as1.setGravity(false);

		ArmorStand as2 = world.spawn(CrossbowUtils.getCenteredLocation(mapEntity.getSecondPoint(world)),
				ArmorStand.class);
		as2.getPersistentDataContainer().set(NamespacedKey.fromString("crossbow:map_entity_id"),
				PersistentDataType.INTEGER, entityID);
		as2.setCustomName(CrossbowUtils.format(entity.getEntity().getID() + (entity.isModified() ? "&4&l*" : "")
				+ " &cPoint 2" + (entity.getStatus() != null ? " " + entity.getStatus() : "")));
		as2.setCustomNameVisible(true);
		as2.setGravity(false);

		return new ArmorStand[] { as1, as2 };
	}

	public void changeMap(Map map_) {
		try {
			game.maps.changeMap(map_);
		} catch (MapNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		this.map = new EditableMap(map_);
		originalMap = map;

		int entityID = 0;
		for (MapEditorEntity entity : map.getMapEntities()) {
			getPreviewEntity(entity);
//			if (entity instanceof SinglePointMapEntity) {
//				SinglePointMapEntity singlePoint = (SinglePointMapEntity) entity;
//				getPreviewEntity(entityID, singlePoint);
//			} else if (entity instanceof RegionMapEntity) {
//				RegionMapEntity region = (RegionMapEntity) entity;
//				getPreviewEntity(entityID, region);
//			} else {
//				game.players.broadcast("&cUnknown MapEntity type " + entity.getClass().getName());
//			}
			entityID++;
		}

		player.getInventory().clear();
		player.getInventory().give(game.items.get(SpawnMapEntity.Item.class));
		player.getInventory().give(game.items.get(SaveMapItem.class));
	}

	public MapEntity.EditorDescriptor getEditingEntity() {
		return editingEntity;
	}

	public void setEditingEntity(MapEntity.EditorDescriptor editingEntity) {
		this.editingEntity = editingEntity;
		player.getInventory().remove(game.items.get(EntityEditItem.class));

		if (editingEntity != null)
			player.getInventory().give(game.items.get(EntityEditItem.class));
	}

	public MapEditorEntity editMapEntity(MapEntity.EditorDescriptor entity) {
		int entityID = 0;
		MapEditorEntity editorEntity = null;
		MapEntity mapEntity = null;
		if(activeEntity == null) {
			entityID = map.getMapEntities().size();
			mapEntity = game.maps.getEntity(entity.getID(), entity.serialize());
			editorEntity = new MapEditorEntity(entityID, mapEntity);
			map.getMapEntities().add(editorEntity);
		} else {
			entityID = activeEntity.getID();
			mapEntity = game.maps.getEntity(entity.getID(), entity.serialize());
			editorEntity = new MapEditorEntity(entityID, mapEntity);
			map.getMapEntities().add(entityID, editorEntity);
			map.getMapEntities().remove(entityID + 1);
		}

		getPreviewEntity(editorEntity);

		return editorEntity;
	}

//	public MapEditorEntity addMapEntity(MapEntity.EditorDescriptor entity, Location location) {
//		HashMap<String, Object> config = new HashMap<>();
//
//		if (entity.getParentClass() == SinglePointMapEntity.class) {
//			config.put("x", location.getX());
//			config.put("y", location.getY());
//			config.put("z", location.getZ());
//			config.put("yaw", location.getYaw());
//			config.put("pitch", location.getPitch());
//		}
//
//		MapEntity mapEntity = game.maps.getEntity(entity.getID(), config);
//		map.getEntities().add(mapEntity);
//		return new MapEditorEntity(map.getEntities().size(), mapEntity);
//	}

	public void saveMap() {
		FileConfiguration config = Crossbow.getInstance().getPlugin().getConfig();

		config.set("maps." + map.getID(), map.serialize());

		Crossbow.getInstance().getPlugin().saveConfig();
	}

	public class EditableMap extends Map {
		UUID id;
		String name;
		File folder;
		MapAuthor author = null;
		Location defaultSpawn;

		ArrayList<MapEditorEntity> entities;

		public EditableMap(Map map) {
			super(map.getID());
			name = map.getName();
			folder = map.getFolder();
			author = map.getMapAuthor();
			defaultSpawn = map.getSpawnLocation(null);

			entities = new ArrayList<>();

			int entityID = 0;
			for (MapEntity entity : map.getEntities()) {
				entities.add(new MapEditorEntity(entityID, entity));
				entityID++;
			}
		}

		@Override
		public ArrayList<MapEntity> getEntities() {
			throw new RuntimeException("EditableMap#getEntities() call");
		}

		public ArrayList<MapEditorEntity> getMapEntities() {
			return entities;
		}

		private java.util.Map<String, Object> serializeLocation(Location location) {
			LinkedHashMap<String, Object> serial = new LinkedHashMap<>();

			serial.put("x", location.getX());
			serial.put("y", location.getY());
			serial.put("z", location.getZ());
			serial.put("pitch", location.getPitch());
			serial.put("yaw", location.getYaw());

			return serial;
		}

		public java.util.Map<String, Object> serialize() {
			LinkedHashMap<String, Object> config = new LinkedHashMap<>();

			config.put("name", name);
			if (author != null) {
				config.put("author", author.serialize());
			}
			config.put("world", folder.getName());
			config.put("defaultSpawn", serializeLocation(defaultSpawn));

			ArrayList<java.util.Map<String, Object>> entitiesSerialized = new ArrayList<>();

			for (MapEditorEntity ent : entities) {
				entitiesSerialized.add(ent.getEntity().serialize());
			}

			config.put("entities", entitiesSerialized);

			return config;
		}
	}

	public enum EntityStatus {
		DELETE, CREATE
	}

	public class MapEditorEntity {
		private int entityID;
		private boolean modified = false;
		private EntityStatus status = null;
		private MapEntity entity;

		MapEditorEntity(int entityID, MapEntity entity) {
			this.entityID = entityID;
			this.entity = entity;
		}

		public boolean isModified() {
			return modified;
		}

		public EntityStatus getStatus() {
			return status;
		}

		public void reset() {
			entity = originalMap.getEntities().get(entityID);
			modified = false;
			status = null;

			getPreviewEntity(this);
		}

		public int getID() {
			return entityID;
		}

		public MapEntity getEntity() {
			return entity;
		}

		public boolean hasPosition() {
			return (entity instanceof SinglePointMapEntity) || (entity instanceof RegionMapEntity);
		}

		public void setSinglePoint() {
			if (!(entity instanceof SinglePointMapEntity)) {
				throw new IllegalArgumentException("Not SinglePointMapEntity");
			}

			SinglePointMapEntity singlePoint = (SinglePointMapEntity) entity;

			removePreviewEntity(entityID);
			singlePoint.setLocation(player.getPlayer().getLocation());
			modified = true;
			getPreviewEntity(this);
		}

		public void setRegion(Location pos1, Location pos2) {
			if (!(entity instanceof RegionMapEntity)) {
				throw new IllegalArgumentException("Not RegionMapEntity");
			}

			RegionMapEntity region = (RegionMapEntity) entity;

			removePreviewEntity(entityID);
			region.setFirstPoint(pos1);
			region.setSecondPoint(pos2);
			modified = true;
			getPreviewEntity(this);
		}
	}
}
