package com.grahhnt.Crossbow.Map.Config;

import java.io.File;

import org.bukkit.Material;

import com.grahhnt.Crossbow.Crossbow;
import com.grahhnt.Crossbow.GUI.CrossbowGUI;
import com.grahhnt.Crossbow.Map.MapManager.Map;
import com.grahhnt.Crossbow.Players.GamePlayer;

public class MapManagerGUI extends CrossbowGUI {
	public MapManagerGUI(GamePlayer player) {
		super("Map Manager", 5);
		setOwner(player);

		Crossbow game = Crossbow.getInstance();
		
		CrossbowGUI.Item newmap = new CrossbowGUI.Item(Material.LIME_WOOL, "&aCreate Map", 1);
		newmap.onClick(event -> {
			event.setCancelled(true);
			player.getPlayer().closeInventory();
			
			MapConfiguration mapConfig = MapConfiguration.getInstance(player);
			player.sendMessage("&aSend in chat your map folder name &7It should be placed in plugins/WoolWars/maps");
			mapConfig.captureChatInput().thenAccept(mapname -> {
				File file = new File(game.getPlugin().getDataFolder(), "maps/" + mapname + "/level.dat");
				if(!file.exists()) {
					player.sendMessage("&cThat folder appears to not be a world file");
					return;
				}
				
				player.sendMessage("&7&oNot implemented, yet.");
			});
		});
		addItem(newmap);

		for (Map map : game.maps.all()) {
			CrossbowGUI.Item guiitem = new CrossbowGUI.Item(Material.MAP, "&f" + map.getName(), 1);
			guiitem.onClick(event -> {
				event.setCancelled(true);
				MapConfiguration mapConfig = MapConfiguration.getInstance(player);
				mapConfig.changeMap(map);
			});
			addItem(guiitem);
		}
	}
}
