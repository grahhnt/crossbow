package com.grahhnt.Crossbow.Map.Config.Items;

import org.bukkit.Material;
import org.bukkit.event.player.PlayerInteractEvent;

import com.grahhnt.Crossbow.Crossbow;
import com.grahhnt.Crossbow.Items.CrossbowItem;
import com.grahhnt.Crossbow.Map.Config.MapConfiguration;
import com.grahhnt.Crossbow.Players.GamePlayer;

public class SaveMapItem extends CrossbowItem {
	public static String id = "MAP_EDITOR_SAVE_MAP";

	SaveMapItem() {
		super(Material.GREEN_WOOL, 1);
		setName("&rSave Map");
		setLore("&7Right-click with this to save the map");
	}

	@Override
	public void onUse(PlayerInteractEvent event) {
		event.setCancelled(true);

		GamePlayer player = Crossbow.getInstance().players.getPlayer(event.getPlayer());
		MapConfiguration config = MapConfiguration.getInstance(player);
		config.saveMap();
		player.sendMessage("&8&oSaved map");
	}
}
