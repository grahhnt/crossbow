package com.grahhnt.Crossbow.Map.Config;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.event.player.PlayerInteractEvent;

import com.grahhnt.Crossbow.Crossbow;
import com.grahhnt.Crossbow.GUI.CrossbowGUI;
import com.grahhnt.Crossbow.GUI.CrossbowGUI.Item;
import com.grahhnt.Crossbow.Items.CrossbowItem;
import com.grahhnt.Crossbow.Map.MapEntity;
import com.grahhnt.Crossbow.Map.MapEntity.EditorDescriptor.ConfigValue;
import com.grahhnt.Crossbow.Players.GamePlayer;

public class SpawnMapEntity {
	public static class GUI extends CrossbowGUI {
		GUI(GamePlayer player, Location spawnLocation) {
			super("Spawn Map Entity", 4);
			setOwner(player);

			Crossbow game = Crossbow.getInstance();

			MapConfiguration configuration = MapConfiguration.getInstance(player);

			for (java.util.Map.Entry<String, MapEntity.EditorDescriptor> entity : game.maps.getEntityDescriptors()
					.entrySet()) {
				MapEntity.EditorDescriptor descriptor = entity.getValue();

				CrossbowGUI.Item item = new CrossbowGUI.Item(descriptor.getEditorItem(), "&r" + descriptor.getName(),
						1);
				item.onClick(event -> {
					event.setCancelled(true);
					player.getPlayer().closeInventory();
					player.sendMessage(descriptor.getName());

					if (descriptor.getConfigValues().size() > 0) {
						configuration.setEditingEntity(descriptor);
					}

					new EntityEditGUI(player).open(player);

//					configuration.addMapEntity(descriptor, spawnLocation);
				});
				addItem(item);
			}
		}
	}

	public static class EntityEditGUI extends CrossbowGUI {
		GamePlayer player;
		MapEntity.EditorDescriptor descriptor;

		EntityEditGUI(GamePlayer player) {
			super("Editing Entity", 4);
			this.player = player;

			MapConfiguration configuration = MapConfiguration.getInstance(player);
			descriptor = configuration.getEditingEntity();

			setName("Editing Entity " + descriptor.getName());

			draw();
		}
		
		private boolean canSave() {
			boolean canSave = true;
			
			for(ConfigValue cvalue : descriptor.getConfigValues()) {
				if(cvalue.getValue() == null && cvalue.isRequired()) {
					canSave = false;
				}
			}
			
			return canSave;
		}

		@SuppressWarnings("unchecked")
		private void draw() {
			super.clear();

			for (ConfigValue cvalue : descriptor.getConfigValues()) {
				CrossbowGUI.Item item = new CrossbowGUI.Item(Material.NAME_TAG, cvalue.getKey(), 1,
						"&7Type: &f" + cvalue.getFriendlyName(), "&7Required By: &f" + cvalue.getSource(), "",
						"&7Value: &f" + cvalue.getValue(), "&7Default Value: &f" + cvalue.getDefaultValue(),
						cvalue.isRequired() ? "&cRequired" : "&8&oOptional");
				item.onClick(event -> {
					event.setCancelled(true);
					player.getPlayer().closeInventory();

					cvalue.captureInput(player).exceptionally(e -> {
						String error = ((Throwable) e).getMessage();
						if (error == null) {
							error = ((Throwable) e).getClass().getCanonicalName();
						}

						player.sendMessage("&cFailed to get value; &7" + error);
						return cvalue.getValue();
					}).thenAccept(value -> {
						cvalue.setValue(value);
						open(player);
						draw();
					});
				});
				addItem(item);
			}

			CrossbowGUI.Item saveItem = new CrossbowGUI.Item(Material.GREEN_WOOL, "&fSpawn Entity", 1);
			if(!canSave()) {
				saveItem.setType(Material.RED_WOOL);
				saveItem.setName("&cSpawn Entity");
				saveItem.setLore("&7One or more of the required values is &cnull");
			}
			saveItem.onClick(event -> {
				event.setCancelled(true);
				
				if(!canSave()) {
					player.sendMessage("&cOne or more of the required values is null; &4&lNOT SAVED.");
					return;
				}
				
				MapConfiguration configuration = MapConfiguration.getInstance(player);
				configuration.editMapEntity(descriptor);
				configuration.setEditingEntity(null);
			});
			addItem(saveItem);
		}
	}

	public static class EntityEditItem extends CrossbowItem {
		public static String id = "MAP_EDITOR_ENTITY_EDIT";

		EntityEditItem() {
			super(Material.NAME_TAG, 1);
			setName("&rEdit Current Entity");
		}

		@Override
		public void onUse(PlayerInteractEvent event) {
			event.setCancelled(true);
			GamePlayer gp = Crossbow.getInstance().players.getPlayer(event.getPlayer());

			new EntityEditGUI(gp).open(gp);
		}
	}

	public static class Item extends CrossbowItem {
		public static String id = "MAP_EDITOR_SPAWN_ENTITY";

		Item() {
			super(Material.BLAZE_SPAWN_EGG, 1);
			setName("&rSpawn Map Entity");
			setLore("&7Right-click to spawn map entity");
		}

		@Override
		public void onUse(PlayerInteractEvent event) {
			event.setCancelled(true);
			GamePlayer gp = Crossbow.getInstance().players.getPlayer(event.getPlayer());

			Location location = gp.getPlayer().getLocation();
			if (event.getClickedBlock() != null) {
				location = event.getClickedBlock().getLocation().add(event.getBlockFace().getDirection());
			}

			new GUI(gp, location).open(gp);
		}
	}
}
