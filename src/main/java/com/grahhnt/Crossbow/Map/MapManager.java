package com.grahhnt.Crossbow.Map;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;
import java.util.Optional;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.WorldCreator;
import org.bukkit.configuration.MemorySection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.permissions.Permission;
import org.bukkit.permissions.PermissionDefault;

import com.grahhnt.Crossbow.Crossbow;
import com.grahhnt.Crossbow.CrossbowComponent;
import com.grahhnt.Crossbow.CrossbowUtils;
import com.grahhnt.Crossbow.PermissionsManager;
import com.grahhnt.Crossbow.Events.GameEventHandler;
import com.grahhnt.Crossbow.Events.GameListener;
import com.grahhnt.Crossbow.Events.Map.MapChangeEvent;
import com.grahhnt.Crossbow.Events.Player.GamePlayerJoinEvent;
import com.grahhnt.Crossbow.Exceptions.MapNotFoundException;
import com.grahhnt.Crossbow.Map.MapManager.Map;
import com.grahhnt.Crossbow.Map.MapManager.Map.MapAuthor;
import com.grahhnt.Crossbow.Players.GamePlayer;

public class MapManager extends CrossbowComponent implements GameListener {
	private Map activeMap = null;
	private World activeWorld = null;
	private ArrayList<Map> maps = new ArrayList<>();
	/**
	 * All MapEntities indexed by ID
	 */
	private HashMap<String, Class<? extends MapEntity>> entities = new HashMap<>();

	public static class Permissions {
		private static final String prefix = "maps.";
		/**
		 * Configure maps
		 */
		public static final String MAP_CONFIGURE = prefix + "configure";
		/**
		 * Change maps before game starts
		 */
		public static final String MAP_CHANGE = prefix + "change";
	}

	public MapManager(Crossbow game) {
		super(game);
	}

	@Override
	public void initialize() {
		activeWorld = Bukkit.getWorld("world");

		game.events.registerEvents(this);

		registerPermission(new Permission(PermissionsManager.node(Permissions.MAP_CONFIGURE), "Configure Maps",
				PermissionDefault.OP));
		registerPermission(
				new Permission(PermissionsManager.node(Permissions.MAP_CHANGE), "Change Map", PermissionDefault.OP));
	}

	public void destroy() {
		CrossbowUtils.deleteDir(new File(Bukkit.getWorldContainer(), "active_game"));
	}

	@GameEventHandler
	public void on(GamePlayerJoinEvent event) {
		if (event.isRejoin())
			return;

		Player player = event.getPlayer().getPlayer();
		player.teleport(getSpawnpoint(event.getPlayer()));
	}

	public void registerEntity(String id, Class<? extends MapEntity> entity) {
		entities.put(id, entity);
	}

	public HashMap<String, MapEntity.EditorDescriptor> getEntityDescriptors() {
		HashMap<String, MapEntity.EditorDescriptor> descriptors = new HashMap<>();

		for (Entry<String, Class<? extends MapEntity>> entity : entities.entrySet()) {
			MapEntity entityInstance = null;

			try {
				entityInstance = (MapEntity) entity.getValue().getConstructors()[0].newInstance();
			} catch (InstantiationException | IllegalAccessException | IllegalArgumentException
					| InvocationTargetException | SecurityException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			MapEntity.EditorDescriptor descriptor = entityInstance.getEditorDescriptor();

			descriptors.put(entity.getKey(), descriptor);
		}

		return descriptors;
	}

	/**
	 * Gets all MapEntities that were registered with Crossbow from guest minigame
	 * 
	 * @return
	 */
	public List<Class<? extends MapEntity>> getEntities() {
		return new ArrayList<>(entities.values());
	}

	/**
	 * Returns MapEntity class from ID
	 * 
	 * @param id
	 * @return
	 */
	public Class<? extends MapEntity> getEntity(String id) {
		return entities.get(id.toUpperCase());
	}

	/**
	 * Returns MapEntity instance from ID & config
	 * 
	 * @param id
	 * @param config
	 * @return
	 */
	public MapEntity getEntity(String id, java.util.Map<String, Object> config) {
		if (getEntity(id) == null)
			return null;

		Class<? extends MapEntity> entity = getEntity(id);
		MapEntity entityInstance = null;

		try {
			entityInstance = (MapEntity) entity.getConstructors()[0].newInstance();
		} catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException
				| SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if (entityInstance != null) {
			entityInstance.deserialize(config);
		}

		return entityInstance;
	}

	public World getActiveWorld() {
		return activeWorld;
	}

	/**
	 * Get player's respawn point.
	 * <p>
	 * Could be used to get team's spawnpoint or game world spawnpoint
	 * 
	 * @param player
	 * @return
	 */
	public Location getSpawnpoint(GamePlayer player) {
		if (activeMap == null) {
			createVoidSpawn();
			return new Location(activeWorld, 0.5, 80, 0.5);
		}

		// TODO: get default spawnpoint from map class
		return new Location(activeWorld, 0.5, 80, 0.5);
	}

	private void createVoidSpawn() {
		Location spawnpoint = new Location(activeWorld, 0, 80, 0);
		for (int x = 0; x < 3; x++) {
			for (int z = 0; z < 3; z++) {
				activeWorld.getBlockAt(spawnpoint.clone().subtract(x - 1, 1, z - 1)).setType(Material.STONE);
				activeWorld.getBlockAt(spawnpoint.clone().subtract(x - 1, 0, z - 1)).setType(Material.AIR);
				activeWorld.getBlockAt(spawnpoint.clone().subtract(x - 1, -1, z - 1)).setType(Material.AIR);
			}
		}
	}

	public ArrayList<Map> all() {
		return maps;
	}

	public boolean doesMapExist(Map map) {
		return maps.stream().filter(m -> m.id.equals(map.id)).findFirst().isPresent();
	}

	/**
	 * Get current map
	 * 
	 * @return
	 */
	public Map getMap() {
		return activeMap;
	}

	public int loadMap(Map map) {
		// this makes it so much easier
		// removes any existing maps and replace it with the new one
		maps.removeIf(m -> m.id.equals(map.id));

		maps.add(map);

		return maps.size();
	}

	public void changeMap(String name) throws MapNotFoundException {
		Map map = null;

		if (!name.equals("none")) {
			Optional<Map> omap = maps.stream().filter(m -> m.name.equals(name)).findFirst();
			if (!omap.isPresent()) {
				throw new MapNotFoundException();
			}

			map = omap.get();
		}

		changeMap(map);
	}

	public void changeMap(Map map) throws MapNotFoundException {
		MapChangeEvent.Pre ev = game.events.emit(new MapChangeEvent.Pre(map));
		if (ev.isCancelled()) {
			return;
		}
		map = ev.getMap();

		// if there was a previously active map, cleanup any entities it has spawned
		if (activeMap != null) {
			activeMap.cleanup();
		}

		// reset back to default
		activeWorld = Bukkit.getWorld("world");
		activeMap = null;

		// move all players to default world
		ArrayList<GamePlayer> players = game.players.connected();
		for (GamePlayer player : players) {
			player.getPlayer().teleport(new Location(activeWorld, 0.5, 80, 0.5, 0, 0));
		}

		// unload world
		Bukkit.getServer().unloadWorld("active_game", false);
		CrossbowUtils.deleteDir(new File(Bukkit.getWorldContainer(), "active_game"));

		if (map != null) {
			try {
				CrossbowUtils.copyDirectory(map.folder.toPath().toString(),
						new File(Bukkit.getWorldContainer(), "active_game").toPath().toString());
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			activeWorld = Bukkit.createWorld(new WorldCreator("active_game"));
			activeMap = map;

			MapChangeEvent.Post ev2 = game.events.emit(new MapChangeEvent.Post(map));

			if (!ev2.isCancelled()) {
				// move to loaded world
				for (GamePlayer player : players) {
					player.getPlayer().teleport(map.getSpawnLocation(activeWorld));
				}
			}
		} else {
			// map is already the "no map world", so no need to teleport players
			// but still fire the MapChangeEvent.Post event for any listeners waiting for
			// map change
			MapChangeEvent.Post ev2 = game.events.emit(new MapChangeEvent.Post(map));
		}
	}

	public void loadMaps(FileConfiguration config) {
		File dataFolder = game.getPlugin().getDataFolder();

		for (String mapid : config.getConfigurationSection("maps").getKeys(false)) {
			String name = config.getString("maps." + mapid + ".name");
			String folder = config.getString("maps." + mapid + ".world");
			MapAuthor author = null;
			if (config.contains("maps." + mapid + ".author")) {
				author = new Map.MapAuthor(config.getString("maps." + mapid + ".author.id"),
						config.getString("maps." + mapid + ".author.name"));
			}

			Map map = new Map.Builder(mapid).name(name).folder(new File(new File(dataFolder, "maps"), folder))
					.author(author)
					.defaultSpawn(CrossbowUtils.getLocation(
							((MemorySection) config.get("maps." + mapid + ".defaultSpawn")).getValues(false)))
					.defaultMap(config.getBoolean("maps." + mapid + ".default")).build();

			if (config.contains("maps." + mapid + ".entities")) {
				for (java.util.Map<String, Object> entity : (List<java.util.Map<String, Object>>) config
						.getConfigurationSection("maps." + mapid).getList("entities")) {
					MapEntity entityInstance = game.maps.getEntity((String) entity.get("type"), entity);
					if (entityInstance == null) {
						game.getPlugin().getLogger().info("[Map Loading] Entity " + entity.get("type") + " not found");
					} else {
						map.addEntity(entityInstance);
					}
				}
			}

			loadMap(map);
		}
		
		Bukkit.getScheduler().runTask(game.getPlugin(), () -> {
			if(getDefaultMap() != null) {
				try {
					changeMap(getDefaultMap());
				} catch (MapNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
	}
	
	public Map getDefaultMap() {
		return maps.stream().filter(map -> map.isDefault()).findFirst().orElse(null);
	}

	public static class Map {
		private String id = null;
		private String name;
		private File folder;
		private MapAuthor author = null;
		private Location defaultSpawn = new Location(null, 0.5, 80, 0.5, 0, 0);
		private boolean defaultMap = false;

		// current map's entities
		private ArrayList<MapEntity> entities = new ArrayList<>();

		protected Map(String id) {
			this.id = id;
		}

		public String getID() {
			return id;
		}

		public String getName() {
			return name;
		}

		public File getFolder() {
			return folder;
		}

		public MapAuthor getMapAuthor() {
			return author;
		}

		public boolean isDefault() {
			return defaultMap;
		}

		public void addEntity(MapEntity entity) {
			entities.add(entity);
		}

		public MapEntity[] getEntities(String id) {
			return entities.stream().filter(m -> m.id.equals(id)).toArray(MapEntity[]::new);
		}

		public List<MapEntity> getEntities() {
			return entities;
		}

		public void cleanup() {
			for (MapEntity entity : entities) {
				entity.destroy();
			}
		}

		public Location getSpawnLocation(World world) {
			if (defaultSpawn == null)
				return new Location(world, 0.5, 80, 0.5, 0, 0);

			Location l = defaultSpawn.clone();
			l.setWorld(world);
			return l;
		}

		@Override
		public Map clone() {
			Map map = new Map(id);

			map.name = name;
			map.folder = folder;
			map.author = author;
			map.defaultSpawn = defaultSpawn;
			map.entities = new ArrayList<>(entities);

			return map;
		}

		@Override
		public String toString() {
			String str = "Map[" + name + "] (" + id + ") in " + folder.getPath();
			if (author != null) {
				str += " built by " + author.toString();
			} else {
				str += " with no author";
			}
			return str;
		}

		public static class MapAuthor {
			public String id;
			public String name;

			public MapAuthor(String id, String name) {
				this.id = id;
				this.name = name;
			}

			public java.util.Map<String, Object> serialize() {
				HashMap<String, Object> serial = new HashMap<>();
				serial.put("id", id);
				serial.put("name", name);
				return serial;
			}

			@Override
			public String toString() {
				return "MapAuthor [id=" + id + ", name=" + name + "]";
			}
		}

		public static class Builder {
			private Map build;

			public Builder(String id) {
				build = new Map(id);
			}

			public Builder name(String name) {
				build.name = name;
				return this;
			}

			public Builder folder(File folder) {
				if (!folder.isDirectory())
					throw new RuntimeException("Map folder is not a folder");
				build.folder = folder;
				return this;
			}

			public Builder author(MapAuthor author) {
				build.author = author;
				return this;
			}

			public Builder defaultSpawn(Location location) {
				location.setWorld(null);
				build.defaultSpawn = location;
				return this;
			}

			public Builder defaultMap(boolean defaultMap) {
				build.defaultMap = defaultMap;
				return this;
			}

			public Map build() {
				return build;
			}
		}
	}
}
