package com.grahhnt.Crossbow.Map;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;

import org.bukkit.Location;
import org.bukkit.World;

import com.grahhnt.Crossbow.CrossbowUtils;
import com.grahhnt.Crossbow.Map.MapEntity.EditorDescriptor.ConfigValue;

public abstract class RegionMapEntity extends MapEntity {
	protected int x1;
	protected int y1;
	protected int z1;
	protected int x2;
	protected int y2;
	protected int z2;

	protected RegionMapEntity(String id) {
		super(id);
		// TODO Auto-generated constructor stub
	}

	protected void populateRegionCoords(Map<String, Object> data) {
		x1 = Double.valueOf(data.get("x1").toString()).intValue();
		y1 = Double.valueOf(data.get("y1").toString()).intValue();
		z1 = Double.valueOf(data.get("z1").toString()).intValue();
		x2 = Double.valueOf(data.get("x2").toString()).intValue();
		y2 = Double.valueOf(data.get("y2").toString()).intValue();
		z2 = Double.valueOf(data.get("z2").toString()).intValue();
	}

	public Location getFirstPoint(World world) {
		return new Location(world, x1, y1, z1);
	}

	public Location getSecondPoint(World world) {
		return new Location(world, x2, y2, z2);
	}

	public void setFirstPoint(Location location) {
		x1 = location.getBlockX();
		y1 = location.getBlockY();
		z1 = location.getBlockZ();
	}

	public void setSecondPoint(Location location) {
		x2 = location.getBlockX();
		y2 = location.getBlockY();
		z2 = location.getBlockZ();
	}

	public boolean isLocationWithin(Location location) {
		return CrossbowUtils.isLocationInArea(getFirstPoint(location.getWorld()), getSecondPoint(location.getWorld()),
				location);
	}

	@Override
	protected LinkedHashMap<String, Object> serializeBase() {
		LinkedHashMap<String, Object> serial = super.serializeBase();

		serial.put("x1", x1);
		serial.put("y1", y1);
		serial.put("z1", z1);
		serial.put("x2", x2);
		serial.put("y2", y2);
		serial.put("z2", z2);

		return serial;
	}

	@Override
	public EditorDescriptor getEditorDescriptor() {
		EditorDescriptor descriptor = super.getEditorDescriptor();

		descriptor.addConfigValues(new ConfigValue.RegionValue("region",
				new Location[] { new Location(null, x1, y1, z1), new Location(null, x2, y2, z2) }, true)
						.source(ConfigValue.Source.PARENT));

		return descriptor;
	}
}
