package com.grahhnt.Crossbow.Commands;

import java.util.Arrays;

import com.grahhnt.Crossbow.Crossbow;
import com.grahhnt.Crossbow.Players.CrossbowTeam;
import com.grahhnt.Crossbow.Players.GamePlayer;

public class TeamCommand extends CrossbowCommand {
	TeamCommand(Crossbow game) {
		super(game, "team");
		this.description = "Team";
		this.usageMessage = "/team [team]";
	}

	@Command(name = "")
	public void handle(GamePlayer player, String[] args) {
		String msg = "";
		if (player.getTeam() == null) {
			msg += "&eYou are not on a team";
		} else {
			msg += "&eYou are on team &f" + player.getTeam().getName() + " &7(" + player.getTeam().getID() + ")";
		}
		msg += "\n&8&oChange team with &2&o/team join";
		if (player.getTeam() != null)
			msg += "\n&8&oLeave team with &2&o/team leave";
		player.sendMessage(msg);
	}

	@Command(name = "join")
	public void joinTeam(GamePlayer player, String[] args) {
		if (args.length == 0) {
			player.sendMessage("&cMissing team. &7"
					+ Arrays.toString(game.players.getTeams().stream().map(t -> t.getID()).toArray()));
			return;
		}

		CrossbowTeam team = game.players.getTeam(args[0].toUpperCase());
		if (team == null) {
			player.sendMessage("&cUnknown team");
			return;
		}

		boolean success = team.addPlayer(player);
		if (success) {
			player.sendMessage("&aJoined team!");
		} else {
			player.sendMessage("&cFailed to join team");
		}
	}

	@Command(name = "leave")
	public void leaveTeam(GamePlayer player, String[] args) {
		if (player.getTeam() == null) {
			player.sendMessage("&cYou are not on a team.");
			return;
		}

		boolean success = player.getTeam().removePlayer(player);
		if (success) {
			player.sendMessage("&aLeft team!");
		} else {
			player.sendMessage("&cFailed to leave team");
		}
	}
}
