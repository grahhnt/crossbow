package com.grahhnt.Crossbow.Commands;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.command.defaults.BukkitCommand;
import org.bukkit.entity.Player;

import com.google.common.collect.Lists;
import com.grahhnt.Crossbow.Crossbow;
import com.grahhnt.Crossbow.PermissionsManager;
import com.grahhnt.Crossbow.Players.GamePlayer;

public class CrossbowCommand extends BukkitCommand {
	Crossbow game;

	public CrossbowCommand(Crossbow game, String name) {
		super(name);
		this.game = game;
	}

	private class SubcommandMethod {
		Method method;
		String[] argsPassed;
	}

	@Override
	public boolean execute(CommandSender sender, String alias, String[] args) {
		GamePlayer player = game.players.getPlayer((Player) sender);
		ArrayList<SubcommandMethod> possible = new ArrayList<>();

		for (Method method : getClass().getDeclaredMethods()) {
			if (method.isAnnotationPresent(Command.class)) {
				Command m = method.getAnnotation(Command.class);
				List<String> methodNames = Lists.asList(m.name(), m.alias());
				boolean shouldExec = false;

				String[] argsPassed = new String[0];

				if (args.length == 0) {
					if (methodNames.contains("")) {
						shouldExec = true;
					}
				} else {
					argsPassed = Arrays.copyOfRange(args, 1, args.length);
					if (methodNames.contains(args[0])) {
						shouldExec = true;
					}
				}

				if (!shouldExec && methodNames.contains("*")) {
					shouldExec = true;
					argsPassed = args;
				}

				// if permissions are set for this (sub)command, all of them should be existant
				// on player's user
				if (m.permissions().length > 0) {
					if (shouldExec) {
						shouldExec = Arrays.asList(m.permissions()).stream().allMatch(
								permission -> player.getPlayer().hasPermission(PermissionsManager.node(permission)));
					}
				}

				if (shouldExec) {
					SubcommandMethod subm = new SubcommandMethod();
					subm.method = method;
					subm.argsPassed = argsPassed;
					possible.add(subm);
				}
			}
		}

		// sort catch-alls (*) to bottom of possible, prioritizing more detailed aliases
		Collections.sort(possible, new Comparator<SubcommandMethod>() {
			@Override
			public int compare(SubcommandMethod m1, SubcommandMethod m2) {
				Command m1m = m1.method.getAnnotation(Command.class);
				Command m2m = m2.method.getAnnotation(Command.class);

				List<String> m1_names = Lists.asList(m1m.name(), m1m.alias());
				List<String> m2_names = Lists.asList(m2m.name(), m2m.alias());

				// both methods are catch alls
				if (m1_names.contains("*") && m2_names.contains("*"))
					return 0;

				// m1 is catch all
				if (m1_names.contains("*") && !m2_names.contains("*"))
					return 1;

				// m2 is catch all
				if (!m1_names.contains("*") && m2_names.contains("*"))
					return -1;

				return 0;
			}
		});

		if (possible.size() > 0) {
			SubcommandMethod method = possible.get(0);
			try {
				method.method.invoke(this, player, method.argsPassed);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} else {
			player.sendMessage("&cInvalid usage. &7" + usageMessage);
		}

		return true;
	}

	@Target(ElementType.METHOD)
	@Retention(RetentionPolicy.RUNTIME)
	public static @interface Command {
		/**
		 * First argument, if astrisk, catch all uncaught
		 * 
		 * @return
		 */
		String name();

		String[] alias() default {};

		/**
		 * Permission nodes needed to execute this command. Empty list means no
		 * permission required
		 * 
		 * @return
		 */
		String[] permissions() default {};
	}
}
