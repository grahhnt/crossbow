package com.grahhnt.Crossbow.Commands;

import java.util.Arrays;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.apache.commons.lang.StringUtils;

import com.grahhnt.Crossbow.Crossbow;
import com.grahhnt.Crossbow.Exceptions.MapNotFoundException;
import com.grahhnt.Crossbow.GUI.CrossbowGUI;
import com.grahhnt.Crossbow.GUI.MapSelectorGUI;
import com.grahhnt.Crossbow.Map.MapManager;
import com.grahhnt.Crossbow.Map.MapManager.Map;
import com.grahhnt.Crossbow.Map.Config.MapConfiguration;
import com.grahhnt.Crossbow.Map.Config.MapManagerGUI;
import com.grahhnt.Crossbow.Players.GamePlayer;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;

public class MapCommand extends CrossbowCommand {
	MapCommand(Crossbow game) {
		super(game, "map");
		this.description = "Map selector";
		this.usageMessage = "/map [name]";
	}

	@Command(name = "*", permissions = { MapManager.Permissions.MAP_CHANGE })
	public void handle(GamePlayer player, String[] args) {
		if (args.length == 0) {
			CrossbowGUI gui = new MapSelectorGUI(player);
			gui.open(player);
			return;
		}

		try {
			game.maps.changeMap(Arrays.stream(args).collect(Collectors.joining(" ")));
			player.sendMessage("map changed");
		} catch (MapNotFoundException e) {
			player.sendMessage("map not found");
		}
	}

	@Command(name = "configure", alias = { "config" }, permissions = { MapManager.Permissions.MAP_CONFIGURE })
	public void configure(GamePlayer player, String[] args) {
		MapConfiguration mapconfig = MapConfiguration.getInstance(player);

		BaseComponent[] empty = new ComponentBuilder(" ").color(ChatColor.WHITE).bold(false).underlined(false)
				.event((ClickEvent) null).event((HoverEvent) null).create();
		Function<String[], String> fullCommand = (String[] a) -> {
			return "crossbow:map configure " + StringUtils.join(a, " ");
		};

		if (args.length == 0) {
			player.sendMessage("&8&oOpening manager...");
			new MapManagerGUI(player).open(player);
		} else {
			switch (args[0]) {
			case "save":
				player.sendMessage("&8&oSaving map...");
				mapconfig.saveMap();
				player.sendMessage("&aSaved map!");
				break;
			case "exit":
				try {
					game.maps.changeMap((Map) null);
				} catch (MapNotFoundException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				mapconfig.destroy();
				player.sendMessage("&8&oExited map editor");
				break;
			case "entity":
				// update entity

//				int entityID = Integer.valueOf(args[1]);
//				MapEditorEntity mapEditorEntity = mapconfig.getMapEditorEntity(entityID);
//
//				if (args.length == 2) {
//					ComponentBuilder component = new ComponentBuilder();
//					component.append(TextComponent.fromLegacyText(CrossbowUtils
//							.format("&eID: &f" + mapEditorEntity.getEntity().getID() + " &7(" + entityID + ")\n")));
//
//					World world = game.maps.getActiveWorld();
//
//					ComponentBuilder positionComponent = new ComponentBuilder("Position: ").color(ChatColor.WHITE);
//					positionComponent.retain(FormatRetention.NONE);
//
//					if (mapEditorEntity.hasPosition()) {
//						if (mapEditorEntity.getEntity() instanceof SinglePointMapEntity) {
//							positionComponent.append(mapconfig.clickableCoords(
//									((SinglePointMapEntity) mapEditorEntity.getEntity()).getLocation(world)));
//							positionComponent.append(empty);
//							positionComponent.append(new CrossbowUtils.Chat.Button("Change", ChatColor.AQUA)
//									.setClick(new ClickEvent(ClickEvent.Action.RUN_COMMAND,
//											"/" + fullCommand.apply(args) + " setsinglepoint"))
//									.build());
//						} else if (mapEditorEntity.getEntity() instanceof RegionMapEntity) {
//							positionComponent.append(mapconfig.clickableCoords(
//									((RegionMapEntity) mapEditorEntity.getEntity()).getFirstPoint(world)));
//							positionComponent.append(empty);
//							positionComponent.append(mapconfig.clickableCoords(
//									((RegionMapEntity) mapEditorEntity.getEntity()).getSecondPoint(world)));
//							positionComponent.append(empty);
//							positionComponent.append(new CrossbowUtils.Chat.Button("Change", ChatColor.AQUA)
//									.setClick(new ClickEvent(ClickEvent.Action.RUN_COMMAND,
//											"/" + fullCommand.apply(args) + " setregion"))
//									.build());
//						}
//					} else {
//						positionComponent
//								.append(new ComponentBuilder("unknown").color(ChatColor.GRAY).italic(true).create());
//					}
//					component.append(positionComponent.create());
//
//					player.sendMessage(component.create());
//				} else {
//					switch (args[2]) {
//					case "setregion":
//						if (args.length == 3) {
//							LocalSession session = WorldEdit.getInstance().getSessionManager().get(BukkitAdapter.adapt(player.getPlayer()));
//							
//							try {
//								Region region = session.getSelection(session.getSelectionWorld());
//								
//								Location pos1 = BukkitAdapter.adapt(BukkitAdapter.adapt(session.getSelectionWorld()), region.getMinimumPoint());
//								Location pos2 = BukkitAdapter.adapt(BukkitAdapter.adapt(session.getSelectionWorld()), region.getMaximumPoint());
//								
//								mapEditorEntity.setRegion(pos1, pos2);
//							} catch (IncompleteRegionException e) {
//								// TODO Auto-generated catch block
//								e.printStackTrace();
//							}
//						}
//						break;
//					case "setsinglepoint":
//						if (args.length == 3) {
//							ComponentBuilder component = new ComponentBuilder();
//							component.retain(FormatRetention.NONE);
//							component.append(
//									new ComponentBuilder("Set Position ").color(ChatColor.GOLD).bold(true).create());
//							component.append(new ComponentBuilder(
//									"(" + mapEditorEntity.getEntity().getID() + " " + entityID + ")\n")
//											.color(ChatColor.GRAY).create());
//							component.append(empty);
//							component.append(mapconfig.clickableCoords(player.getPlayer().getLocation()));
//							component.append(TextComponent.fromLegacyText("�r\n"));
//							component.append(CrossbowUtils.Chat.buttons(
//									new CrossbowUtils.Chat.Button("Center X,Z", ChatColor.YELLOW).setTooltip("")
//											.setClick(new ClickEvent(ClickEvent.Action.RUN_COMMAND,
//													"/" + fullCommand.apply(args) + " centerxz")),
//									new CrossbowUtils.Chat.Button("Yaw", ChatColor.GOLD).setTooltip("")
//											.setClick(new ClickEvent(ClickEvent.Action.RUN_COMMAND,
//													"/" + fullCommand.apply(args) + " yaw")),
//									new CrossbowUtils.Chat.Button("Pitch", ChatColor.AQUA).setClick(new ClickEvent(
//											ClickEvent.Action.RUN_COMMAND, "/" + fullCommand.apply(args) + " pitch")),
//									new CrossbowUtils.Chat.Button("Save", ChatColor.GREEN).setClick(new ClickEvent(
//											ClickEvent.Action.RUN_COMMAND, "/" + fullCommand.apply(args) + " save"))));
//
//							player.sendMessage(component.create());
//						} else {
//							switch (args[3]) {
//							case "centerxz":
//								player.getPlayer()
//										.teleport(CrossbowUtils.getCenteredLocation(player.getPlayer().getLocation()));
//								player.getPlayer().performCommand(fullCommand
//										.apply(Arrays.asList(args).subList(0, args.length - 1).toArray(String[]::new)));
//								break;
//							case "yaw":
//								if (args.length == 4) {
//									ComponentBuilder component = new ComponentBuilder();
//
//									component.append(new ComponentBuilder("Set Yaw\n").color(ChatColor.GOLD).bold(true)
//											.create());
//
//									component
//											.append(CrossbowUtils.Chat.buttons(
//													new CrossbowUtils.Chat.Button("North", ChatColor.GREEN)
//															.setClick(new ClickEvent(
//																	ClickEvent.Action.RUN_COMMAND,
//																	"/" + fullCommand.apply(args) + " north")),
//													new CrossbowUtils.Chat.Button("East", ChatColor.YELLOW)
//															.setClick(new ClickEvent(ClickEvent.Action.RUN_COMMAND,
//																	"/" + fullCommand.apply(args) + " east")),
//													new CrossbowUtils.Chat.Button("South", ChatColor.RED)
//															.setClick(new ClickEvent(ClickEvent.Action.RUN_COMMAND,
//																	"/" + fullCommand.apply(args) + " south")),
//													new CrossbowUtils.Chat.Button("West", ChatColor.BLUE)
//															.setClick(new ClickEvent(ClickEvent.Action.RUN_COMMAND,
//																	"/" + fullCommand.apply(args) + " west"))));
//
//									player.sendMessage(component.create());
//								} else {
//									Location target = player.getPlayer().getLocation().clone();
//									switch (args[4]) {
//									case "north":
//										target.setYaw(180);
//										break;
//									case "east":
//										target.setYaw(-90);
//										break;
//									case "south":
//										target.setYaw(0);
//										break;
//									case "west":
//										target.setYaw(90);
//										break;
//									}
//									player.getPlayer().teleport(target);
//									player.getPlayer().performCommand(fullCommand.apply(
//											Arrays.asList(args).subList(0, args.length - 2).toArray(String[]::new)));
//								}
//								break;
//							case "pitch":
//								if (args.length == 4) {
//									ComponentBuilder component = new ComponentBuilder();
//
//									component.append(new ComponentBuilder("Set Pitch\n").color(ChatColor.GOLD)
//											.bold(true).create());
//
//									component.append(CrossbowUtils.Chat.buttons(
//											new CrossbowUtils.Chat.Button("-90 (Down)", ChatColor.GREEN)
//													.setClick(new ClickEvent(ClickEvent.Action.RUN_COMMAND,
//															"/" + fullCommand.apply(args) + " -90")),
//											new CrossbowUtils.Chat.Button("-45", ChatColor.YELLOW)
//													.setClick(new ClickEvent(ClickEvent.Action.RUN_COMMAND,
//															"/" + fullCommand.apply(args) + " -45")),
//											new CrossbowUtils.Chat.Button("0 (Forward)", ChatColor.RED)
//													.setClick(new ClickEvent(ClickEvent.Action.RUN_COMMAND,
//															"/" + fullCommand.apply(args) + " 0")),
//											new CrossbowUtils.Chat.Button("45", ChatColor.BLUE)
//													.setClick(new ClickEvent(ClickEvent.Action.RUN_COMMAND,
//															"/" + fullCommand.apply(args) + " 45")),
//											new CrossbowUtils.Chat.Button("90 (Up)", ChatColor.BLUE)
//													.setClick(new ClickEvent(ClickEvent.Action.RUN_COMMAND,
//															"/" + fullCommand.apply(args) + " 90"))));
//
//									player.sendMessage(component.create());
//								} else {
//									Location target = player.getPlayer().getLocation().clone();
//									switch (args[4]) {
//									case "-90":
//										target.setPitch(-90);
//										break;
//									case "-45":
//										target.setPitch(-45);
//										break;
//									case "0":
//										target.setPitch(0);
//										break;
//									case "45":
//										target.setPitch(45);
//										break;
//									case "90":
//										break;
//									}
//									player.getPlayer().teleport(target);
//									player.getPlayer().performCommand(fullCommand.apply(
//											Arrays.asList(args).subList(0, args.length - 2).toArray(String[]::new)));
//								}
//								break;
//							case "save":
//								mapEditorEntity.setSinglePoint();
//								break;
//							}
//						}
//						break;
//					}
//				}
				break;
			}
		}
	}
}
