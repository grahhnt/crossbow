package com.grahhnt.Crossbow.Commands;

import com.grahhnt.Crossbow.Crossbow;
import com.grahhnt.Crossbow.Exceptions.EventCanceledException;
import com.grahhnt.Crossbow.Exceptions.NoMapSelectedException;
import com.grahhnt.Crossbow.Players.GamePlayer;

public class StartCommand extends CrossbowCommand {
	public StartCommand(Crossbow game) {
		super(game, "start");

		this.description = "Start a game";
		this.usageMessage = "/start";
	}

	@Command(name = "")
	public void start(GamePlayer player, String[] args) {
		try {
			game.startGame(-1);
		} catch (NoMapSelectedException e) {
			player.sendMessage("&cNo map selected; cannot start");
		} catch (EventCanceledException e) {
			if (e.getMessage() == null) {
				player.sendMessage("&cFailed to start game &8&oEvent Canceled with no reason");
			} else {
				player.sendMessage("&cFailed to start game; " + e.getMessage());
			}
		}
	}

	@Command(name = "now")
	public void startNow(GamePlayer player, String[] args) {
		try {
			game.startGame(0);
		} catch (NoMapSelectedException e) {
			player.sendMessage("&cNo map selected; cannot start");
		} catch (EventCanceledException e) {
			if (e.getMessage() == null) {
				player.sendMessage("&cFailed to start game &8&oEvent Canceled with no reason");
			} else {
				player.sendMessage("&cFailed to start game; " + e.getMessage());
			}
		}
	}
}
