package com.grahhnt.Crossbow.Commands;

import java.lang.reflect.Field;

import org.bukkit.Bukkit;
import org.bukkit.command.CommandMap;
import org.bukkit.command.defaults.BukkitCommand;

import com.grahhnt.Crossbow.Crossbow;
import com.grahhnt.Crossbow.CrossbowComponent;

/**
 * Built-in commands.
 * 
 * @author grant
 *
 */
public class CommandManager extends CrossbowComponent {
	public CommandManager(Crossbow game) {
		super(game);
	}

	@Override
	public void initialize() {
		registerCommand("crossbow", new MapCommand(game));
		registerCommand("crossbow", new StartCommand(game));
		registerCommand("crossbow", new TestCommand(game));
		registerCommand("crossbow", new TeamCommand(game));
	}

	public void registerCommand(String namespace, BukkitCommand command) {
		try {
			final Field bukkitCommandMap = Bukkit.getServer().getClass().getDeclaredField("commandMap");

			bukkitCommandMap.setAccessible(true);
			CommandMap commandMap = (CommandMap) bukkitCommandMap.get(Bukkit.getServer());

			commandMap.register(command.getName(), namespace, command);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
