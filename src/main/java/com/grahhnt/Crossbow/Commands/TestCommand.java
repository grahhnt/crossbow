package com.grahhnt.Crossbow.Commands;

import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.NamespacedKey;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.persistence.PersistentDataType;

import com.grahhnt.Crossbow.Crossbow;
import com.grahhnt.Crossbow.Crossbow.GameState;
import com.grahhnt.Crossbow.Entities.MagicZombie;
import com.grahhnt.Crossbow.Items.CrossbowItem;
import com.grahhnt.Crossbow.Items.MagicBedrock;
import com.grahhnt.Crossbow.Items.VanillaCrossbowItem;
import com.grahhnt.Crossbow.Players.CrossbowTeam;
import com.grahhnt.Crossbow.Players.GamePlayer;

import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;

public class TestCommand extends CrossbowCommand {
	CrossbowItem item;

	public TestCommand(Crossbow game) {
		super(game, "test");

		this.description = "Test command";
		this.usageMessage = "/test";

		item = game.items.get(MagicBedrock.class);
	}

	@Command(name = "makemewinner")
	public void winner(GamePlayer player, String[] args) {
		player.getTeam().setWinner(true);
		game.setGameState(GameState.POSTGAME);
	}

	@Command(name = "whatitem")
	public void whatitem(GamePlayer player, String[] args) {
		ItemStack item = player.getPlayer().getInventory().getItemInMainHand();
		if (item == null || item.getAmount() == 0) {
			player.sendMessage("&cYou are not holding anything");
			return;
		}

		ItemMeta meta = item.getItemMeta();
		String itemid = meta.getPersistentDataContainer().get(NamespacedKey.fromString("crossbow:itemid"),
				PersistentDataType.STRING);
		player.sendMessage("crossbow:itemid " + itemid);
	}

	@Command(name = "zombie")
	public void zombie(GamePlayer player, String[] args) {
		new MagicZombie().spawn(player.getPlayer().getLocation());
	}

	@Command(name = "item")
	public void item(GamePlayer player, String[] args) {
		player.getInventory().give(item);
		player.sendMessage("given item");
	}

	@Command(name = "updateitems")
	public void update(GamePlayer player, String[] args) {
		game.items.updateItems(player.getPlayer().getInventory());
	}

	@Command(name = "uuid")
	public void uuid(GamePlayer player, String[] args) {
		player.getPlayer().spigot().sendMessage(new ComponentBuilder("[random uuid]")
				.event(new ClickEvent(ClickEvent.Action.SUGGEST_COMMAND, UUID.randomUUID().toString())).create());
	}
	
	@Command(name = "compare")
	public void compare(GamePlayer player, String[] args) {
		Bukkit.broadcastMessage(VanillaCrossbowItem.get(Material.STONE_PICKAXE).isBetter(VanillaCrossbowItem.get(Material.WOODEN_PICKAXE)) + "");
		Bukkit.broadcastMessage(VanillaCrossbowItem.get(Material.STONE_PICKAXE).isBetter(VanillaCrossbowItem.get(Material.STONE_PICKAXE)) + "");
		Bukkit.broadcastMessage(VanillaCrossbowItem.get(Material.STONE_PICKAXE).isBetter(VanillaCrossbowItem.get(Material.IRON_PICKAXE)) + "");
	}
	
	@Command(name = "teams")
	public void teams(GamePlayer player, String[] args) {
		for(CrossbowTeam team : game.players.getTeams()) {
			Bukkit.broadcastMessage(team.toString());
		}
	}
}
