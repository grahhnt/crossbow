package com.grahhnt.Crossbow.Players;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

/**
 * Declares things that can win with general identifiers
 * 
 * @author grant
 *
 */
public interface Winnable {
	/**
	 * What name to display when winner is shown
	 * 
	 * @return
	 */
	public String getWinningName();

	/**
	 * Determine if this instance is a winner
	 * 
	 * @return
	 */
	public boolean isWinner();

	/**
	 * Set if this instance is a winner
	 * 
	 * @param isWinner
	 */
	public void setWinner(boolean isWinner);

	/**
	 * Player(s) associated with this instance
	 * 
	 * @return
	 */
	public List<GamePlayer> getWinningPlayers();

	public static String winnerString(List<Winnable> winners) {
		ArrayList<String> winnerStrings = new ArrayList<>();

		for (Winnable winner : winners) {
			winnerStrings.add(winner.getWinningName());
		}

		return StringUtils.join(winnerStrings, ", ");
	}
}
