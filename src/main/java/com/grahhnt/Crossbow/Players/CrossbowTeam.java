package com.grahhnt.Crossbow.Players;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.bukkit.GameMode;

import com.grahhnt.Crossbow.Crossbow;
import com.grahhnt.Crossbow.Events.Player.GamePlayerTeamEvent;
import com.grahhnt.Crossbow.Players.CrossbowTeam.Member;

public class CrossbowTeam implements Winnable {
	private String id;
	private String name;
	private String nameColor = "";
	private ArrayList<Member> players = new ArrayList<>();
	private int maxPlayers = -1;
	private boolean isWinner = false;

	public CrossbowTeam(String id, String name) {
		this.id = id;
		this.name = name;
	}

	public String getNameColor() {
		return nameColor;
	}

	protected void setNameColor(String nameColor) {
		this.nameColor = nameColor;
	}

	public String getID() {
		return id;
	}

	public String getName() {
		return name;
	}

	public int getMaxPlayers() {
		return maxPlayers;
	}

	@Override
	public String getWinningName() {
		return name;
	}

	@Override
	public boolean isWinner() {
		return isWinner;
	}

	@Override
	public void setWinner(boolean isWinner) {
		this.isWinner = isWinner;
	}

	@Override
	public List<GamePlayer> getWinningPlayers() {
		return players.stream().map(m -> m.player).collect(Collectors.toList());
	}

	/**
	 * Ran when {@link PlayerManager#cleanup()} is executed. Used to reset any
	 * running game specific data (like if they're the winner or not)
	 */
	public void reset() {
		setWinner(false);
	}

	/**
	 * -1 means no limit
	 * 
	 * @param maxPlayers
	 */
	public void setMaxPlayers(int maxPlayers) {
		this.maxPlayers = maxPlayers;
	}

	/**
	 * Try to add player to team
	 * 
	 * @param player
	 * @return If the event succeeded
	 */
	public boolean addPlayer(GamePlayer player) {
		GamePlayerTeamEvent event = Crossbow.getInstance().events
				.emit(new GamePlayerTeamEvent(player, player.getTeam(), this));

		if (!event.isCancelled()) {
			Member teamMember = new Member();
			teamMember.player = player;
			players.add(teamMember);
			player.setTeam(event.getToTeam());
			return true;
		}
		return false;
	}

	/**
	 * Try to remove player from team
	 * 
	 * @param player
	 * @return If the event succeeded
	 */
	public boolean removePlayer(GamePlayer player) {
		Optional<Member> found = players.stream().filter(m -> m.player.getUUID().equals(player.getUUID())).findFirst();
		if (found.isEmpty())
			return false;

		GamePlayerTeamEvent event = Crossbow.getInstance().events
				.emit(new GamePlayerTeamEvent(player, player.getTeam(), null));

		if (!event.isCancelled()) {
			players.remove(found.get());
			player.setTeam(null);
			return true;
		}
		return false;
	}

	public boolean hasPlayer(GamePlayer player) {
		Stream<Member> pl = players.stream()
				.filter(p -> p.getPlayer().getPlayer().getUniqueId() == player.getPlayer().getUniqueId());
		return pl.count() > 0;
	}

	public List<Member> getPlayers() {
		return Collections.unmodifiableList(players);
	}

	public List<Member> getAlivePlayers() {
		return getPlayers().stream().filter(
				m -> m.getPlayer().isConnected() && m.getPlayer().getPlayer().getGameMode() == GameMode.SURVIVAL)
				.collect(Collectors.toList());
	}

	public static class Member {
		GamePlayer player;
		/**
		 * Borrowing this idea from Matrix
		 * 
		 * Ability permission can be identified by (ability power level >= team member
		 * power level)
		 */
		int power = 0;

		public GamePlayer getPlayer() {
			return player;
		}

		public void setPower(int power) {
			this.power = power;
		}

		public int getPower() {
			return power;
		}

		@Override
		public String toString() {
			return player.toString();
		}
	}

	@Override
	public String toString() {
		return "CrossbowTeam [id=" + id + ", name=" + name + ", nameColor=" + nameColor + ", players=" + players
				+ ", maxPlayers=" + maxPlayers + ", isWinner=" + isWinner + "]";
	}
}
