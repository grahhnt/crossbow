package com.grahhnt.Crossbow.Players;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;
import java.util.function.Consumer;
import java.util.stream.Collectors;

import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.attribute.Attribute;
import org.bukkit.block.Block;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;

import com.grahhnt.Crossbow.CrossbowUtils;
import com.grahhnt.Crossbow.ScoreboardManager;
import com.grahhnt.Crossbow.Events.GameEvent;
import com.grahhnt.Crossbow.Events.Player.GamePlayerDeathEvent;

import net.md_5.bungee.api.chat.BaseComponent;

public class GamePlayer implements Winnable {
	private final UUID uuid;
	private Player player;
	private boolean canReconnect = true;
	private boolean isConnected = true;
	private boolean isWinner = false;

	public ScoreboardManager.GPSBManager scoreboard = null;
	private GPInventory inventory = null;
	private CrossbowTeam team = null;
	private LastDamageCause lastDamage = null;

	/**
	 * Internally tracked listeners for instant events
	 * 
	 * Keyed with GameEvent class name
	 */
	private HashMap<String, ArrayList<Consumer<?>>> temporaryListeners = new HashMap<>();

	public GamePlayer(Player player) {
		uuid = player.getUniqueId();
		this.player = player;
		this.inventory = new GPInventory(this);
	}
	
	public UUID getUUID() {
		return uuid;
	}

	/**
	 * Called by {@link PlayerManager#remove(GamePlayer)}; used to preform player
	 * cleanup
	 */
	public void destroy() {
		if (getTeam() != null)
			getTeam().removePlayer(this);
	}
	
	/**
	 * Heal player by amount
	 * @param amount 1.0 = half heart
	 */
	public void heal(double amount) {
		double health = getHealth() + amount;
		if(health > getPlayer().getAttribute(Attribute.GENERIC_MAX_HEALTH).getValue()) {
			health = getPlayer().getAttribute(Attribute.GENERIC_MAX_HEALTH).getValue();
		}
		getPlayer().setHealth(health);
	}
	
	public boolean hasPermission(String node) {
		return getPlayer().hasPermission(node);
	}

	public LastDamageCause getLastDamage() {
		return lastDamage;
	}

	@Override
	public String getWinningName() {
		return getDisplayName();
	}

	@Override
	public boolean isWinner() {
		return isWinner;
	}

	@Override
	public void setWinner(boolean isWinner) {
		this.isWinner = isWinner;
	}

	@Override
	public List<GamePlayer> getWinningPlayers() {
		return List.of(this);
	}
	
	public void setCanReconnect(boolean canReconnect) {
		this.canReconnect = canReconnect;
	}
	
	public boolean canReconnect() {
		return canReconnect;
	}

	void setTeam(CrossbowTeam team) {
		this.team = team;
	}

	public CrossbowTeam getTeam() {
		return team;
	}

	/**
	 * Get Bukkit player. After a player disconnects, this.player is no longer
	 * linked to the connected player
	 * 
	 * @return
	 */
	public Player getPlayer() {
		return Bukkit.getPlayer(player.getUniqueId());
	}
	
	public OfflinePlayer getOfflinePlayer() {
		return Bukkit.getOfflinePlayer(uuid);
	}

	/**
	 * Set connected state; managed with eventlisteners
	 * <p>
	 * Subscribe to GamePlayerJoinEvent or GamePlayerDisconnectEvent
	 * 
	 * @param isConnected
	 */
	public void setConnected(boolean isConnected) {
		this.isConnected = isConnected;
	}

	public boolean isConnected() {
		return isConnected;
	}

	public String getDisplayName() {
		return getPlayer() != null ? getPlayer().getDisplayName() : getOfflinePlayer().getName();
	}

	public String getUsername() {
		return getPlayer() != null ? getPlayer().getName() : getOfflinePlayer().getName();
	}

	public double getHealth() {
		return getPlayer() != null ? getPlayer().getHealth() : 0;
	}

	public GPInventory getInventory() {
		return inventory;
	}
	
	public void sendMessage(BaseComponent[] component) {
		getPlayer().spigot().sendMessage(component);
	}

	public void sendMessage(String str) {
		getPlayer().sendMessage(CrossbowUtils.format(str));
	}

	public List<String> getSidebar() {
		return new ArrayList<String>();
	}

	public void kill(Consumer<GamePlayerDeathEvent> deathHandler) {
		if (deathHandler != null)
			addInstantListener(GamePlayerDeathEvent.class, deathHandler);

		getPlayer().damage(getHealth());
	}

	public String getDeathMessage() {
		if (lastDamage == null) {
			return getDisplayName() + " died.";
		}

		Calendar thirdy = Calendar.getInstance();
		thirdy.add(Calendar.SECOND, -30);
		if (lastDamage.getTimestamp().after(thirdy)) {
			if (lastDamage.getPlayer() != null) {
				return lastDamage.getPlayer().getDisplayName() + " killed " + getDisplayName() + " via "
						+ lastDamage.getCause();
			} else if (lastDamage.getBlockDamager() != null) {
				return lastDamage.getBlockDamager().getType() + " killed " + getDisplayName() + " via "
						+ lastDamage.getCause();
			} else if (lastDamage.getEntityDamager() != null) {
				return lastDamage.getEntityDamager().getName() + " killed " + getDisplayName() + " via "
						+ lastDamage.getCause();
			} else {
				return getDisplayName() + " died via " + lastDamage.getCause();
			}
		} else {
			return getDisplayName() + " died.";
		}
	}

	public void sendTitle(String title, String subtitle) {
		sendTitle(title, subtitle, true);
	}

	public void sendTitle(String title, String subtitle, boolean showInChat) {
		sendMessage(CrossbowUtils.format(title) + " " + CrossbowUtils.format(subtitle));
		getPlayer().sendTitle(CrossbowUtils.format(title), CrossbowUtils.format(subtitle), 10, 70, 20);
	}

	public LastDamageCause getLastDamageCause() {
		return lastDamage;
	}

	public LastDamageCause setLastDamageCause(DamageCause cause, GamePlayer damager, double amount) {
		lastDamage = new LastDamageCause(cause, damager, null, null, amount, getPlayer().getHealth());
		return lastDamage;
	}

	public LastDamageCause setLastDamageCause(DamageCause cause, Block damager, double amount) {
		lastDamage = new LastDamageCause(cause, null, damager, null, amount, getPlayer().getHealth());
		return lastDamage;
	}

	public LastDamageCause setLastDamageCause(DamageCause cause, Entity damager, double amount) {
		lastDamage = new LastDamageCause(cause, null, null, damager, amount, getPlayer().getHealth());
		return lastDamage;
	}

	public LastDamageCause setLastDamageCause(LastDamageCause cause) {
		lastDamage = cause;
		return lastDamage;
	}

	public <T extends GameEvent> List<Consumer<T>> getInstantListeners(Class<T> event) {
		if (temporaryListeners.get(event.getName()) == null) {
			return new ArrayList<Consumer<T>>();
		}

		List<Consumer<T>> listeners = temporaryListeners.get(event.getName()).stream().map(f -> (Consumer<T>) f)
				.collect(Collectors.toList());

		if (listeners != null)
			temporaryListeners.get(event.getName()).clear();

		return listeners;
	}

	private <T extends GameEvent> void addInstantListener(Class<T> event, Consumer<T> listener) {
		if (!temporaryListeners.containsKey(event.getName())) {
			temporaryListeners.put(event.getName(), new ArrayList<>());
		}
		temporaryListeners.get(event.getName()).add(listener);
	}

	@Override
	public String toString() {
		return getPlayer().getName();
	}

	public class LastDamageCause {
		/**
		 * DamageCause.CUSTOM is used when an unknown death source happened
		 */
		private DamageCause cause = null;
		private GamePlayer playerDamager = null;
		private Block blockDamager = null;
		private Entity entityDamager = null;
		private double amount = 0;
		private double finalHealth = 0;
		private Calendar timestamp = Calendar.getInstance();

		LastDamageCause(DamageCause cause, GamePlayer playerDamager, Block blockDamager, Entity entityDamager,
				double amount, double finalHealth) {
			this.cause = cause;
			this.playerDamager = playerDamager;
			this.blockDamager = blockDamager;
			this.entityDamager = entityDamager;
			this.amount = amount;
			this.finalHealth = finalHealth;
		}

		public double getFinalHealth() {
			return finalHealth;
		}

		public double getAmount() {
			return amount;
		}

		public DamageCause getCause() {
			return cause;
		}

		public GamePlayer getPlayer() {
			return playerDamager;
		}

		public Block getBlockDamager() {
			return blockDamager;
		}

		public Entity getEntityDamager() {
			return entityDamager;
		}

		public Calendar getTimestamp() {
			return timestamp;
		}
	}
}
