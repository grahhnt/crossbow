package com.grahhnt.Crossbow.Players;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitTask;

import com.grahhnt.Crossbow.Crossbow;
import com.grahhnt.Crossbow.Crossbow.GameState;
import com.grahhnt.Crossbow.CrossbowComponent;
import com.grahhnt.Crossbow.Events.GameEventHandler;
import com.grahhnt.Crossbow.Events.GameListener;
import com.grahhnt.Crossbow.Events.Player.GamePlayerDestroyEvent;
import com.grahhnt.Crossbow.Events.Player.GamePlayerDisconnectEvent;
import com.grahhnt.Crossbow.Events.Player.GamePlayerJoinEvent;

public class PlayerManager extends CrossbowComponent implements GameListener {
	private HashMap<UUID, GamePlayer> players = new HashMap<>();
	private ArrayList<CrossbowTeam> teams = new ArrayList<>();

	public enum TeamFillMethod {
		/**
		 * Mostly equal team counts
		 */
		ROUNDROBIN,
		/**
		 * Fill first available team, then go to next team and fill
		 */
		FILL_FIRST
	}

	private boolean destroyOnDisconnect = false;
	private boolean disconnectTimeoutEnable = true;
	private int disconnectTimeout = 5;
	private TeamFillMethod teamFillMethod = TeamFillMethod.ROUNDROBIN;

	/**
	 * Why specify classes?
	 * 
	 * Specifying classes allows for initialization to be called when a new
	 * player/team is registered on the child class
	 */
	private Class<? extends GamePlayer> GamePlayerClass = GamePlayer.class;
	private Class<? extends CrossbowTeam> TeamClass = CrossbowTeam.class;

	/**
	 * Only created by Crossbow
	 * 
	 * @param plugin
	 * @param GamePlayerClass     Can be null
	 * @param destroyOnDisconnect once a player disconnects; destroy GamePlayer
	 *                            instance, making rejoins impossible
	 */
	public PlayerManager(Crossbow plugin, Class<? extends GamePlayer> GamePlayerClass,
			Class<? extends CrossbowTeam> TeamClass, boolean destroyOnDisconnect) {
		super(plugin);

		// if argument is null, use build in class
		if (GamePlayerClass != null)
			this.GamePlayerClass = GamePlayerClass;

		if (TeamClass != null)
			this.TeamClass = TeamClass;

		this.destroyOnDisconnect = destroyOnDisconnect;
	}
	
	public void configReconnectTimeout(boolean disconnectTimeoutEnable, int disconnectTimeout) {
		this.disconnectTimeoutEnable = disconnectTimeoutEnable;
		this.disconnectTimeout = disconnectTimeout;
	}

	// setup anything
	@Override
	public void initialize() {
		game.events.registerEvents(this);
	}

	@GameEventHandler
	public void on(GamePlayerDisconnectEvent event) {
		if (destroyOnDisconnect) {
			event.setDestroyPlayer(true);
		}
	}

	HashMap<UUID, BukkitTask> disconnectTimer = new HashMap<>();

	@GameEventHandler
	public void handleDuringGameReconnect(GamePlayerJoinEvent event) {
		if (disconnectTimer.containsKey(event.getPlayer().getUUID())) {
			disconnectTimer.get(event.getPlayer().getUUID()).cancel();
			broadcast(event.getPlayer().getUsername() + " &ereconnected");
			disconnectTimer.remove(event.getPlayer().getUUID());
		}
	}

	@GameEventHandler
	public void handleDuringGameDisconnect(GamePlayerDisconnectEvent event) {
		if (disconnectTimer.containsKey(event.getPlayer().getPlayer().getUniqueId())) {
			disconnectTimer.get(event.getPlayer().getPlayer().getUniqueId()).cancel();
			disconnectTimer.remove(event.getPlayer().getPlayer().getUniqueId());
		}

		if (game.getGameState() == GameState.GAME && disconnectTimeoutEnable) {
			broadcast(event.getPlayer().getUsername() + " &edisconnected; They have &f" + disconnectTimeout
					+ " seconds &eto reconnect.");

			disconnectTimer.put(event.getPlayer().getPlayer().getUniqueId(),
					Bukkit.getScheduler().runTaskLater(game.getPlugin(), () -> {
						broadcast(event.getPlayer().getUsername() + " &ewas disconnected for too long");
						remove(event.getPlayer());
						disconnectTimer.remove(event.getPlayer().getUUID());
					}, 20L * disconnectTimeout));
		}
	}

//	private HashMap<UUID, BukkitTask> reconnectCounters = new HashMap<>();
//
//	@GameEventHandler
//	public void handleDuringGameDisconnect(GamePlayerDisconnectEvent event) {
//		GamePlayer player = event.getPlayer();
//		// player has just disconnected and still can reconnect, start timeout and
//		// countdown
//		//
//		// if countdown gets to zero with them still disconnected, set
//		// GamePlayer#canReconnect to false, check if enough players are still on each
//		// team, and resume gameplay
//		if (player.canReconnect()) {
//			if (reconnectCounters.containsKey(player.getPlayer().getUniqueId()))
//				return;
//			
//			player.setCanReconnect(false);
//
//			BukkitTask task = Bukkit.getScheduler().runTaskLater(game.getPlugin(), () -> {
//				reconnectCounters.remove(player.getPlayer().getUniqueId());
//				if (!player.isConnected()) {
//					player.setCanReconnect(false);
//					game.players.broadcast(
//							"&cPlayer " + player.getDisplayName() + " &cwas removed due to not reconnecting.");
//
//					game.events.emit(new GamePlayerReconnectDeny(player));
//				}
//			}, 20L * 30);
//
//			reconnectCounters.put(player.getPlayer().getUniqueId(), task);
//			
//			game.setTimeout(GamePlayerDisconnectTimeout.class);
//		}
//	}

	public CrossbowTeam determineTeam(GamePlayer player) {
		switch (teamFillMethod) {
		case ROUNDROBIN:
			CrossbowTeam lowest = null;
			for (CrossbowTeam team : teams) {
				if (lowest == null) {
					lowest = team;
				} else {
					if (lowest.getPlayers().size() > team.getPlayers().size()) {
						lowest = team;
					}
				}
			}
			return lowest;
		case FILL_FIRST:
			Optional<CrossbowTeam> team = teams.stream()
					.filter(t -> t.getMaxPlayers() == -1 || (t.getMaxPlayers() <= (t.getPlayers().size() + 1)))
					.findFirst();
			if (team.isEmpty())
				return null;
			team.get().addPlayer(player);
			return team.get();
		}
		return null;
	}

	/**
	 * Future decisions to put a new player on a team will use this method
	 * 
	 * @param method
	 */
	public void setTeamFillMethod(TeamFillMethod method) {
		this.teamFillMethod = method;
	}

	public TeamFillMethod getTeamFillMethod() {
		return teamFillMethod;
	}

	public void remove(GamePlayer player) {
		player.destroy();
		players.remove(player.getUUID());
		game.events.emit(new GamePlayerDestroyEvent(player));
	}

	public void registerTeam(CrossbowTeam team) {
		teams.add(team);
	}

	public <T extends CrossbowTeam> List<T> getTeams() {
		return teams.stream().map(t -> (T) t).collect(Collectors.toList());
	}

	public <T extends CrossbowTeam> T getTeam(String id) {
		return (T) teams.stream().filter(t -> t.getID().equals(id)).findFirst().get();
	}

	public <T extends GamePlayer> T getPlayer(Player bplayer) {
		if (game.entities.getRegistry().isNPC(bplayer))
			return null;

		if (!players.containsKey(bplayer.getUniqueId())) {
			try {
				players.put(bplayer.getUniqueId(),
						GamePlayerClass.getDeclaredConstructor(Player.class).newInstance(bplayer));
			} catch (InstantiationException | IllegalAccessException | IllegalArgumentException
					| InvocationTargetException | NoSuchMethodException | SecurityException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		return (T) players.get(bplayer.getUniqueId());
	}

	public boolean hasPlayer(Player bplayer) {
		return players.containsKey(bplayer.getUniqueId());
	}

	public ArrayList<GamePlayer> connected() {
		ArrayList<GamePlayer> connected = new ArrayList<>();

		for (GamePlayer player : players.values()) {
			if (player.isConnected())
				connected.add(player);
		}

		return connected;
	}

	public int size() {
		return players.size();
	}

	public <U extends GamePlayer> ArrayList<U> all() {
		ArrayList<U> all = new ArrayList<>();

		for (GamePlayer player : players.values()) {
			all.add((U) player);
		}

		return all;
	}

	public void kick() {
		for (GamePlayer player : players.values()) {
			player.getPlayer().kickPlayer("Goodbye");
		}
	}

	public void cleanup() {
		players.values().stream().forEach(player -> {
			player.destroy();
		});
		players.clear();
		teams.stream().forEach(team -> {
			team.reset();
		});
	}

	public void broadcast(String message) {
		for (GamePlayer player : players.values()) {
			if (!player.isConnected())
				continue;
			player.sendMessage(message);
		}
	}

	public void broadcast(String title, String subtitle) {
		for (GamePlayer player : players.values()) {
			if (!player.isConnected())
				continue;
			player.sendTitle(title, subtitle);
		}
	}

	public void broadcast(Sound sound) {
		for (GamePlayer player : connected()) {
			player.getPlayer().playSound(player.getPlayer().getLocation(), sound, 1, 1);
		}
	}
}
