package com.grahhnt.Crossbow.Players;

import org.bukkit.inventory.EquipmentSlot;
import org.bukkit.inventory.ItemStack;

import com.grahhnt.Crossbow.Items.CrossbowItem;
import com.grahhnt.Crossbow.Kits.InventoryLayout;

public class GPInventory {
	GamePlayer player;
	
	GPInventory(GamePlayer player) {
		this.player = player;
	}
	
	public void remove(CrossbowItem item) {
		int slot = 0;
		for(ItemStack item2 : player.getPlayer().getInventory().getContents()) {
			if(item.isSame(item2)) {
				player.getPlayer().getInventory().setItem(slot, null);
			}
			slot++;
		}
	}
	
	public void give(CrossbowItem item) {
		player.getPlayer().getInventory().addItem(item);
	}
	
	public void set(EquipmentSlot slot, CrossbowItem item) {
		player.getPlayer().getInventory().setItem(slot, item);
	}
	
	public void set(int slot, CrossbowItem item) {
		player.getPlayer().getInventory().setItem(slot, item);
	}
	
	public void clear() {
		player.getPlayer().getInventory().clear();
	}
	
	public void setKit(InventoryLayout kit) {
		clear();
		
		for(InventoryLayout.Item item : kit.getItems()) {
			CrossbowItem citem = null;
			
			if(item instanceof InventoryLayout.TeamItem) {
				citem = ((InventoryLayout.TeamItem) item).getItem(player.getTeam());
			} else {
				citem = item.getItem();
			}
			
			set(item.getSlot(), citem);
		}
	}
}
