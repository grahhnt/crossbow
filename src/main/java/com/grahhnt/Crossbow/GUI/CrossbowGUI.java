package com.grahhnt.Crossbow.GUI;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;
import java.util.stream.Collectors;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import com.grahhnt.Crossbow.Crossbow;
import com.grahhnt.Crossbow.CrossbowUtils;
import com.grahhnt.Crossbow.Players.GamePlayer;

public class CrossbowGUI implements Listener {
	private static Crossbow game;

	/**
	 * When player is set, unregister eventlisteners upon gui being closed
	 */
	private GamePlayer owner = null;
	private Inventory inventory;
	private String name = null;
	private int rows = 0;
	private HashMap<Integer, Item> items = new HashMap<>();

	public CrossbowGUI(String name, int rows) {
		this.name = name;
		this.rows = rows;

		inventory = Bukkit.createInventory(null, rows * 9, name);

		game.getPlugin().getServer().getPluginManager().registerEvents(this, game.getPlugin());
	}

	/**
	 * Internal.
	 * 
	 * @deprecated
	 * @param game
	 */
	public CrossbowGUI(Crossbow game) {
		CrossbowGUI.game = game;
	}

	/**
	 * When owner is set, unregister EventListeners upon gui being closed
	 * 
	 * @param player
	 */
	public void setOwner(GamePlayer player) {
		this.owner = player;
	}

	private void reinstance() {
		inventory = Bukkit.createInventory(null, rows * 9, name);

		redraw();
	}

	public void redraw() {
		for (Map.Entry<Integer, Item> entry : items.entrySet()) {
			setItem(entry.getValue(), entry.getKey());
		}
	}

	public void setName(String name) {
		this.name = name;
		reinstance();
	}

	public void setRows(int rows) {
		this.rows = rows;
		reinstance();
	}

	public void open(GamePlayer player) {
		player.getPlayer().openInventory(inventory);
	}

	public void clear() {
		inventory.clear();
		items.clear();
	}

	public void setItem(Item item, int slot) {
		inventory.setItem(slot, item);
		items.put(slot, item);
	}

	public void addItem(Item item) {
		int slot = inventory.firstEmpty();
		setItem(item, slot);
	}

	public static class Item extends ItemStack {
		private ArrayList<Consumer<InventoryClickEvent>> clickHandlers = new ArrayList<>();

		public Item(Material material, String name, int amount, String... lore) {
			super(material, amount);

			ItemMeta meta = getItemMeta();
			meta.setDisplayName(CrossbowUtils.format(name));
			meta.setLore(List.of(lore).stream().map(l -> CrossbowUtils.format(l)).collect(Collectors.toList()));
			setItemMeta(meta);
		}

		public void setName(String name) {
			ItemMeta meta = getItemMeta();
			meta.setDisplayName(CrossbowUtils.format(name));
			setItemMeta(meta);
		}

		public void setLore(String... lore) {
			ItemMeta meta = getItemMeta();
			meta.setLore(List.of(lore).stream().map(l -> CrossbowUtils.format(l)).collect(Collectors.toList()));
			setItemMeta(meta);
		}

		public void onClick(Consumer<InventoryClickEvent> consumer) {
			clickHandlers.add(consumer);
		}
	}

	@EventHandler
	public void on(InventoryClickEvent event) {
		if (event.getInventory() != inventory)
			return;

		if (items.get(event.getRawSlot()) != null) {
			ArrayList<Consumer<InventoryClickEvent>> handlers = items.get(event.getRawSlot()).clickHandlers;
			for (Consumer<InventoryClickEvent> handler : handlers) {
				handler.accept(event);
			}
		}
	}

	/**
	 * If owner is set, destroy eventhandlers upon closing the inventory
	 * 
	 * @param event
	 */
	@EventHandler
	public void on(InventoryCloseEvent event) {
		if (event.getInventory() != inventory)
			return;
		if (owner == null)
			return;
		HandlerList.unregisterAll(this);
	}
}
