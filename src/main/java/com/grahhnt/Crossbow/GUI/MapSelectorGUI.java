package com.grahhnt.Crossbow.GUI;

import org.bukkit.Material;

import com.grahhnt.Crossbow.Crossbow;
import com.grahhnt.Crossbow.Exceptions.MapNotFoundException;
import com.grahhnt.Crossbow.Map.MapManager.Map;
import com.grahhnt.Crossbow.Players.GamePlayer;

public class MapSelectorGUI extends CrossbowGUI {
	public MapSelectorGUI(GamePlayer player) {
		super("Map Selector", 4);
		setOwner(player);
		
		Crossbow game = Crossbow.getInstance();
		
		CrossbowGUI.Item nomap = new CrossbowGUI.Item(Material.BARRIER, "&fNo Map", 1);
		nomap.onClick(event -> {
			event.setCancelled(true);
			player.getPlayer().closeInventory();
			try {
				game.maps.changeMap((Map) null);
			} catch (MapNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		});
		addItem(nomap);
		
		for (Map map : game.maps.all()) {
			CrossbowGUI.Item guiitem = new CrossbowGUI.Item(Material.MAP, "&f" + map.getName(), 1);
			guiitem.onClick(event -> {
				event.setCancelled(true);
				player.getPlayer().closeInventory();
				try {
					game.maps.changeMap(map);
				} catch (MapNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			});
			addItem(guiitem);
		}
	}
}
