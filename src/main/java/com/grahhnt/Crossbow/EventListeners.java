package com.grahhnt.Crossbow;

import java.util.function.Consumer;

import org.bukkit.Location;
import org.bukkit.attribute.Attribute;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByBlockEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.player.PlayerRespawnEvent;
import org.bukkit.projectiles.ProjectileSource;

import com.grahhnt.Crossbow.Crossbow.GameState;
import com.grahhnt.Crossbow.Events.Player.GamePlayerDamageEvent;
import com.grahhnt.Crossbow.Events.Player.GamePlayerDeathEvent;
import com.grahhnt.Crossbow.Events.Player.GamePlayerDisconnectEvent;
import com.grahhnt.Crossbow.Events.Player.GamePlayerJoinEvent;
import com.grahhnt.Crossbow.Events.Player.GamePlayerRespawnEvent;
import com.grahhnt.Crossbow.Players.GamePlayer;
import com.grahhnt.Crossbow.Players.GamePlayer.LastDamageCause;

public class EventListeners implements Listener {
	private Crossbow game;

	EventListeners(Crossbow game) {
		this.game = game;
	}

	private void determineLastDamageCause(EntityDamageEvent event) {
		GamePlayer player = game.players.getPlayer((Player) event.getEntity());

		if (event instanceof EntityDamageByBlockEvent) {
			player.setLastDamageCause(event.getCause(), ((EntityDamageByBlockEvent) event).getDamager(),
					event.getFinalDamage());
		} else if (event instanceof EntityDamageByEntityEvent) {
			EntityDamageByEntityEvent EDBEE = (EntityDamageByEntityEvent) event;
			if (EDBEE.getDamager() instanceof Player) {
				// casting casting casting casting
				player.setLastDamageCause(event.getCause(),
						(GamePlayer) game.players.getPlayer((Player) EDBEE.getDamager()), event.getFinalDamage());
			} else if (EDBEE.getDamager() instanceof Projectile) {
				// if the damager is a projectile (includes thrown potions), try and get the
				// owner of the projectile
				ProjectileSource owner = ((Projectile) EDBEE.getDamager()).getShooter();
				GamePlayer damager = null;
				if (owner instanceof Player) {
					damager = game.players.getPlayer((Player) owner);
				}
				player.setLastDamageCause(event.getCause(), damager, event.getFinalDamage());
			} else {
				player.setLastDamageCause(event.getCause(), EDBEE.getDamager(), event.getFinalDamage());
			}
		} else {
			player.setLastDamageCause(event.getCause(), (GamePlayer) null, event.getFinalDamage());
		}
	}

	@EventHandler(priority = EventPriority.HIGHEST)
	public void onPlayerDamage(EntityDamageEvent event) {
		if (!(event.getEntity() instanceof Player))
			return;

		GamePlayer player = game.players.getPlayer((Player) event.getEntity());
		if (player == null) {
			// it is most likely an npc
			return;
		}

		boolean resultsInDeath = player.getPlayer().getHealth() - event.getFinalDamage() <= 0;

		// First Pass
		// ----------
		//
		// Executing this now will allow any listeners to GamePlayerDamageEvent to get
		// who damaged the player
		//
		// This will be executed again after the event is processed to allow for
		// GamePlayerDamageEvent listeners to change damage values and have it be
		// re-calculated
		determineLastDamageCause(event);
		// </First Pass>

		GamePlayerDamageEvent damageEvent = game.events
				.emit(new GamePlayerDamageEvent(player, resultsInDeath, event.getCause(), event.getFinalDamage()));
		if (damageEvent.isCancelled()) {
			event.setCancelled(true);
			return;
		}
		event.setDamage(damageEvent.getAmount());
		resultsInDeath = player.getPlayer().getHealth() - event.getFinalDamage() <= 0;

		LastDamageCause _orig = player.getLastDamage();

		// Second Pass
		// -----------
		//
		// This is executed post GamePlayerDamageEvent to recalculate the last damage
		// cause returned by the damage event.
		determineLastDamageCause(event);
		// </Second Pass>

		if (resultsInDeath) {
			GamePlayerDeathEvent deathEvent = game.events.emit(new GamePlayerDeathEvent(player, false, true));
			for (Consumer<GamePlayerDeathEvent> devent : player.getInstantListeners(GamePlayerDeathEvent.class)) {
				devent.accept(deathEvent);
			}
			if (deathEvent.isCancelled()) {
				event.setCancelled(true);
				player.setLastDamageCause(_orig);
				return;
			}

			if (deathEvent.shouldDropInventory()) {
				player.getPlayer().getInventory().forEach(item -> {
					if (item != null) {
						player.getPlayer().getWorld().dropItemNaturally(player.getPlayer().getLocation(), item);
					}
				});
				player.getInventory().clear();
			}

			if (deathEvent.shouldInstantRespawn()) {
				event.setCancelled(true);

				game.players.broadcast(deathEvent.getDeathMessage());

				player.getPlayer().setHealth(player.getPlayer().getAttribute(Attribute.GENERIC_MAX_HEALTH).getValue());
				player.getPlayer().setFoodLevel(20);

				GamePlayerRespawnEvent respawnEvent = game.events
						.emit(new GamePlayerRespawnEvent(player, game.maps.getSpawnpoint(player)));
				if (!respawnEvent.isCancelled()) {
					player.getPlayer().teleport(respawnEvent.getRespawnLocation());
				}
			}
		}
	}

	/**
	 * Shouldn't get fired; but if it does, notify minigame
	 * 
	 * @param event
	 */
	@EventHandler
	public void onPlayerDeath(PlayerDeathEvent event) {
		GamePlayer player = game.players.getPlayer((Player) event.getEntity());
//		player.setLastDamageCause(DamageCause.CUSTOM, (GamePlayer) null, player.getHealth());
		GamePlayerDeathEvent ev = game.events.emit(new GamePlayerDeathEvent(player, false, true));

		event.setDeathMessage(ev.getDeathMessage());

		if (ev.shouldDropInventory()) {
			event.setKeepInventory(false);
			event.setDroppedExp(0);
		} else {
			event.setKeepInventory(true);
			event.setDroppedExp(0);
			event.getDrops().clear();
		}
	}

	@EventHandler
	public void onPlayerJoin(PlayerJoinEvent event) {
		boolean playerWasAlreadyHere = game.players.hasPlayer(event.getPlayer());
		GamePlayer gplayer = game.players.getPlayer(event.getPlayer());
		gplayer.setConnected(true);

		GamePlayerJoinEvent ev = game.events.emit(new GamePlayerJoinEvent(gplayer, playerWasAlreadyHere));
		if (ev.isCancelled()) {
			event.getPlayer()
					.kickPlayer(ev.getCancelledReason() == null ? "Failed to join game." : ev.getCancelledReason());
		}
	}

	@EventHandler
	public void onPlayerLeave(PlayerQuitEvent event) {
		if (game.players.hasPlayer(event.getPlayer())) {
			GamePlayer gplayer = game.players.getPlayer(event.getPlayer());
			gplayer.setConnected(false);

			// TODO: make destruction configurable
			GamePlayerDisconnectEvent ev = game.events.emit(new GamePlayerDisconnectEvent(gplayer,
					game.getGameState() == GameState.LOBBY || game.getGameState() == GameState.PREGAME));

			if (ev.willDestroy()) {
				game.players.remove(gplayer);
			}
		}
	}

	@EventHandler
	public void onExplosion(EntityDamageByEntityEvent event) {
		// if an explosion has the cosmetic tag on it, don't apply damage
		// used by end-game fireworks
		if (event.getDamager().hasMetadata("cosmetic")) {
			event.setCancelled(true);
		}
	}

	@EventHandler
	public void onRespawn(PlayerRespawnEvent event) {
		GamePlayer gplayer = game.players.getPlayer(event.getPlayer());
		Location respawnLocation = event.getRespawnLocation();

		if (respawnLocation.getWorld() != game.maps.getActiveWorld()) {
			respawnLocation = game.maps.getSpawnpoint(gplayer);
		}

		GamePlayerRespawnEvent ev = game.events.emit(new GamePlayerRespawnEvent(gplayer, respawnLocation));
		if (!ev.isCancelled()) {
			// if event isn't cancelled, respawn player to new location
			event.setRespawnLocation(ev.getRespawnLocation());
		}
	}
}
