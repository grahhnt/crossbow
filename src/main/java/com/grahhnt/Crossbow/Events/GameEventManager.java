package com.grahhnt.Crossbow.Events;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;

import org.bukkit.entity.Player;

import com.grahhnt.Crossbow.Crossbow;
import com.grahhnt.Crossbow.Crossbow.GameState;
import com.grahhnt.Crossbow.CrossbowComponent;
import com.grahhnt.Crossbow.Components.GameComponent;
import com.grahhnt.Crossbow.Events.GameEventHandler.EventPriority;
import com.grahhnt.Crossbow.Events.Game.GameStateEvent;
import com.grahhnt.Crossbow.Events.Player.GamePlayerJoinEvent;
import com.grahhnt.Crossbow.Players.GamePlayer;

public class GameEventManager extends CrossbowComponent implements GameListener {
	private ArrayList<ListenerWrapper> listeners = new ArrayList<>();

	private HashMap<GameState, ArrayList<GameComponent.GameState>> componentHooks = new HashMap<>();
	private HashMap<Class<? extends GameEvent>, ArrayList<GameComponent.GameEvent>> eventComponentHooks = new HashMap<>();

	public GameEventManager(Crossbow game) {
		super(game);

		for (GameState state : GameState.values()) {
			componentHooks.put(state, new ArrayList<GameComponent.GameState>());
		}
	}

	// setup anything
	@Override
	public void initialize() {
		registerEvents(this);
	}

	@GameEventHandler(priority = EventPriority.LOW)
	public void onStateChange(GameStateEvent event) {
		// cleanup components for previous state
		for (GameComponent.GameState component : componentHooks.get(event.getOldState())) {
			if (component.hasRan()) {
				component.setHasRan(false);
				component.shutdown();
			}
		}
		// if event permits, start new components
		if (event.shouldRunComponents()) {
			for (GameComponent.GameState component : componentHooks.get(event.getNewState())) {
				component.setHasRan(true);
				component.execute();
			}
		}
	}

	/**
	 * Components to run on GameEvents
	 * 
	 * @param event
	 * @param component
	 */
	public void registerComponentHook(Class<? extends GameEvent> event, GameComponent.GameEvent component) {
		if (!eventComponentHooks.containsKey(event)) {
			eventComponentHooks.put(event, new ArrayList<>());
		}
		eventComponentHooks.get(event).add(component);
	}

	/**
	 * Components to run on GameStates
	 * 
	 * @param state
	 * @param component
	 */
	public void registerComponentHook(GameState state, GameComponent.GameState component) {
		componentHooks.get(state).add(component);
	}

	/**
	 * Register component via Class
	 * <p>
	 * GameComponent class must have constructor taking Crossbow instance as only
	 * argument.
	 * <p>
	 * If you have a different layout of GameComponent constructor, use
	 * {@link #registerComponentHook(GameState, GameComponent)}
	 * 
	 * @param state
	 * @param component
	 * @throws InstantiationException
	 * @throws IllegalAccessException
	 * @throws IllegalArgumentException
	 * @throws InvocationTargetException
	 * @throws NoSuchMethodException
	 * @throws SecurityException
	 */
	public void registerComponentHook(GameState state, Class<? extends GameComponent.GameState> component)
			throws InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException,
			NoSuchMethodException, SecurityException {
		GameComponent.GameState componentInstance = component.getConstructor(Crossbow.class).newInstance(game);
		componentHooks.get(state).add(componentInstance);
	}

	/**
	 * Register component via Class
	 * <p>
	 * GameComponent class must have constructor taking Crossbow instance as only
	 * argument.
	 * <p>
	 * If you have a different layout of GameComponent constructor, use
	 * {@link #registerComponentHook(GameState, GameComponent)}
	 * 
	 * @param state
	 * @param component
	 * @throws InstantiationException
	 * @throws IllegalAccessException
	 * @throws IllegalArgumentException
	 * @throws InvocationTargetException
	 * @throws NoSuchMethodException
	 * @throws SecurityException
	 */
	public void registerComponentHook(Class<? extends GameEvent> event,
			Class<? extends GameComponent.GameEvent> component) throws InstantiationException, IllegalAccessException,
			IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException {
		GameComponent.GameEvent componentInstance = component.getConstructor(Crossbow.class).newInstance(game);

		if (!eventComponentHooks.containsKey(event)) {
			eventComponentHooks.put(event, new ArrayList<>());
		}
		eventComponentHooks.get(event).add(componentInstance);
	}

	public void unregisterComponentHook(GameState state, Class<? extends GameComponent.GameState> component) {
		GameComponent.GameState comp = null;
		for (GameComponent.GameState c : componentHooks.get(state)) {
			if (c.getClass() == component) {
				comp = c;
			}
		}

		componentHooks.get(state).remove(comp);
	}

	public void unregisterComponentHook(Class<? extends GameEvent> event,
			Class<? extends GameComponent.GameEvent> component) {
		GameComponent.GameEvent comp = null;
		for (GameComponent.GameEvent c : eventComponentHooks.get(event)) {
			if (c.getClass() == component) {
				comp = c;
			}
		}

		eventComponentHooks.get(event).remove(comp);
	}

	/**
	 * Trigger any events that need to be fired when everything is setup for the
	 * minigame plugin
	 * 
	 * (eg at the end of JavaPlugin#onEnable)
	 */
	public void catchup() {
		for (Player player : game.getPlugin().getServer().getOnlinePlayers()) {
			boolean playerWasAlreadyHere = game.players.hasPlayer(player);
			GamePlayer gplayer = game.players.getPlayer(player);
			gplayer.setConnected(true);

			GamePlayerJoinEvent event = emit(new GamePlayerJoinEvent(gplayer, playerWasAlreadyHere));
			if (event.isCancelled()) {
				player.kickPlayer("Failed to join game.");
			}
		}
	}

	public void registerEvents(GameListener listener) {
		ListenerWrapper wrap = new ListenerWrapper();
		wrap.listener = listener;

		for (Method method : listener.getClass().getDeclaredMethods()) {
			if (method.isAnnotationPresent(GameEventHandler.class)) {
				if (method.getParameterCount() == 1
						&& GameEvent.class.isAssignableFrom(method.getParameterTypes()[0])) {
					// already checked above
					wrap.add((Class<? extends GameEvent>) method.getParameterTypes()[0], method);
				}
			}
		}

		listeners.add(wrap);
	}

	/**
	 * Emit GameEvent
	 * 
	 * @param event
	 * @return if the event was cancelled or not
	 */
	public <T extends GameEvent> T emit(T event) {
		for (ListenerWrapper listener : listeners) {
			if (listener.events.containsKey(event.getClass())) {
				ArrayList<Method> methods = listener.events.get(event.getClass());
				methods.sort(new SortEvents());

				for (Method method : methods) {
					if (event instanceof GameEvent.Cancellable && ((GameEvent.Cancellable) event).isCancelled())
						continue;

					try {
						method.invoke(listener.listener, event);
					} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		}

		if (eventComponentHooks.containsKey(event.getClass())) {
			for (GameComponent.GameEvent component : eventComponentHooks.get(event.getClass())) {
				component.execute(event);
			}
		}

		return event;
	}

	private class SortEvents implements Comparator<Method> {
		@Override
		public int compare(Method m1, Method m2) {
			GameEventHandler a1 = m1.getAnnotation(GameEventHandler.class);
			GameEventHandler a2 = m2.getAnnotation(GameEventHandler.class);
			if (a1.priority() == a2.priority()) {
				return 0;
			} else {
				return a1.priority().compareTo(a2.priority());
			}
		}
	}

	private class ListenerWrapper {
		public GameListener listener;
		public HashMap<Class<? extends GameEvent>, ArrayList<Method>> events = new HashMap<>();

		public void add(Class<? extends GameEvent> event, Method method) {
			if (!events.containsKey(event)) {
				events.put(event, new ArrayList<Method>());
			}

			events.get(event).add(method);
		}
	}
}
