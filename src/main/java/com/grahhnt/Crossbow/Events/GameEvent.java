package com.grahhnt.Crossbow.Events;

public abstract class GameEvent {
	private String eventName = null;

	protected GameEvent(String eventName) {
		this.eventName = eventName;
	}

	public String getEventName() {
		return eventName;
	}

	public static class Cancellable extends GameEvent {
		private boolean cancelled = false;
		private String cancelledReason = null;

		protected Cancellable(String eventName) {
			super(eventName);
		}

		/**
		 * Recommended to specify reason
		 * 
		 * @param cancelled
		 */
		public void setCancelled(boolean cancelled) {
			setCancelled(cancelled, null);
		}

		/**
		 * Set cancelation and cancelation reason
		 * 
		 * @param cancelled
		 * @param reason
		 */
		public void setCancelled(boolean cancelled, String reason) {
			this.cancelled = cancelled;
			this.cancelledReason = reason;
		}

		public boolean isCancelled() {
			return cancelled;
		}

		public String getCancelledReason() {
			return cancelledReason;
		}
	}
}
