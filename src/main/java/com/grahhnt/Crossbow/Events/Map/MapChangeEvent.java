package com.grahhnt.Crossbow.Events.Map;

import com.grahhnt.Crossbow.Events.GameEvent;
import com.grahhnt.Crossbow.Map.MapManager.Map;

public class MapChangeEvent {
	/**
	 * Emitted before map load is triggered.
	 * <p>
	 * Can be cancelled or set map to anything else.
	 * 
	 * @author grant
	 *
	 */
	public static class Pre extends GameEvent.Cancellable {
		private Map map;

		public Pre(Map map) {
			super("MapChangeEvent.Pre");

			this.map = map;
		}

		public Map getMap() {
			return map;
		}

		public void setMap(Map map) {
			this.map = map;
		}
	}

	/**
	 * Emitted after map change is successful.
	 * <p>
	 * If not cancelled, teleports all players to default spawnpoint in world
	 * 
	 * @author grant
	 *
	 */
	public static class Post extends GameEvent.Cancellable {
		private Map map;

		public Post(Map map) {
			super("MapChangeEvent.Post");

			this.map = map;
		}

		public Map getMap() {
			return map;
		}
	}
}
