package com.grahhnt.Crossbow.Events.Player;

import com.grahhnt.Crossbow.Events.GameEvent;
import com.grahhnt.Crossbow.Players.GamePlayer;

/**
 * Fired when a player takes too long to reconnect.
 * 
 * Can be used to competitive ban a player
 * 
 * @author grant
 *
 */
public class GamePlayerReconnectDeny extends GameEvent {
	GamePlayer player;
	
	public GamePlayerReconnectDeny(GamePlayer player) {
		super("GamePlayerReconnectDeny");
		
		this.player = player;
	}
	
	public GamePlayer getPlayer() {
		return player;
	}
}
