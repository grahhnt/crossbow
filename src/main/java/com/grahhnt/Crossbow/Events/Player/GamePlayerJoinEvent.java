package com.grahhnt.Crossbow.Events.Player;

import com.grahhnt.Crossbow.Events.GameEvent;
import com.grahhnt.Crossbow.Players.GamePlayer;

public class GamePlayerJoinEvent extends GameEvent.Cancellable {
	GamePlayer player;
	boolean isRejoin;

	public GamePlayerJoinEvent(GamePlayer player, boolean isRejoin) {
		super("GamePlayerJoinEvent");
		this.player = player;
		this.isRejoin = isRejoin;
	}
	
	public GamePlayer getPlayer() {
		return player;
	}
	
	public boolean isRejoin() {
		return isRejoin;
	}
}
