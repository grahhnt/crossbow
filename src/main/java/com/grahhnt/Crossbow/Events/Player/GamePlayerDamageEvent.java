package com.grahhnt.Crossbow.Events.Player;

import org.bukkit.event.entity.EntityDamageEvent.DamageCause;

import com.grahhnt.Crossbow.Events.GameEvent;
import com.grahhnt.Crossbow.Players.GamePlayer;

public class GamePlayerDamageEvent extends GameEvent.Cancellable {
	GamePlayer player;
	/**
	 * If the event concludes with true; {@link GamePlayerDeathEvent} will be fired
	 * after
	 */
	boolean willResultInDeath;
	/**
	 * What caused damage
	 */
	DamageCause cause;
	/**
	 * Amount of damage
	 */
	double amount;

	public GamePlayerDamageEvent(GamePlayer player, boolean willResultInDeath, DamageCause cause, double amount) {
		super("GamePlayerDamageEvent");
		this.player = player;
		this.willResultInDeath = willResultInDeath;
		this.cause = cause;
		this.amount = amount;
	}
	
	public GamePlayer getPlayer() {
		return player;
	}
	
	public boolean willResultInDeath() {
		return willResultInDeath;
	}
	
	public DamageCause getCause() {
		return cause;
	}
	
	public double getAmount() {
		return amount;
	}
	
	public void setAmount(double amount) {
		this.amount = amount;
	}
}
