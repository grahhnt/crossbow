package com.grahhnt.Crossbow.Events.Player;

import com.grahhnt.Crossbow.Events.GameEvent;
import com.grahhnt.Crossbow.Players.GamePlayer;

/**
 * Sent when a player dies
 * 
 * @author grant
 *
 */
public class GamePlayerDeathEvent extends GameEvent.Cancellable {
	GamePlayer player;
	boolean shouldInstantRespawn;
	boolean dropInventory;
	String deathMessage;

	public GamePlayerDeathEvent(GamePlayer player, boolean shouldInstantRespawn, boolean dropInventory) {
		super("GamePlayerDeathEvent");

		this.player = player;
		this.shouldInstantRespawn = shouldInstantRespawn;
		this.dropInventory = dropInventory;
		deathMessage = player.getDeathMessage();
	}

	public GamePlayer getPlayer() {
		return player;
	}

	public boolean shouldInstantRespawn() {
		return shouldInstantRespawn;
	}

	public void setShouldInstantRespawn(boolean shouldInstantRespawn) {
		this.shouldInstantRespawn = shouldInstantRespawn;
	}

	public boolean shouldDropInventory() {
		return dropInventory;
	}

	public void setShouldDropInventory(boolean shouldDropInventory) {
		dropInventory = shouldDropInventory;
	}
	
	public String getDeathMessage() {
		return deathMessage;
	}
	
	public void setDeathMessage(String deathMessage) {
		this.deathMessage = deathMessage;
	}
}
