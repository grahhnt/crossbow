package com.grahhnt.Crossbow.Events.Player;

import com.grahhnt.Crossbow.Events.GameEvent;
import com.grahhnt.Crossbow.Players.CrossbowTeam;
import com.grahhnt.Crossbow.Players.GamePlayer;

public class GamePlayerTeamEvent extends GameEvent.Cancellable {
	private GamePlayer player;
	private CrossbowTeam fromTeam = null;
	private CrossbowTeam toTeam = null;

	public GamePlayerTeamEvent(GamePlayer player, CrossbowTeam fromTeam, CrossbowTeam toTeam) {
		super("GamePlayerTeamEvent");
		this.player = player;
		this.fromTeam = fromTeam;
		this.toTeam = toTeam;
	}

	public GamePlayer getPlayer() {
		return player;
	}

	public CrossbowTeam getFromTeam() {
		return fromTeam;
	}

	/**
	 * Change target team.
	 * 
	 * @param team
	 */
	public void setTeam(CrossbowTeam team) {
		this.toTeam = team;
	}

	public CrossbowTeam getToTeam() {
		return toTeam;
	}
}
