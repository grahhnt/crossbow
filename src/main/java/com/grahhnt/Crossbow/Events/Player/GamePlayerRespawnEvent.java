package com.grahhnt.Crossbow.Events.Player;

import org.bukkit.Location;

import com.grahhnt.Crossbow.Events.GameEvent;
import com.grahhnt.Crossbow.Players.GamePlayer;

public class GamePlayerRespawnEvent extends GameEvent.Cancellable {
	private GamePlayer player;
	private Location respawnLocation;

	public GamePlayerRespawnEvent(GamePlayer player, Location respawnLocation) {
		super("GamePlayerRespawnEvent");
		this.player = player;
		this.respawnLocation = respawnLocation;
	}

	public GamePlayer getPlayer() {
		return player;
	}

	/**
	 * Set respawn location.
	 * <p>
	 * If the event isn't cancelled, this location will be used
	 * 
	 * @param location
	 */
	public void setRespawnLocation(Location location) {
		this.respawnLocation = location;
	}

	/**
	 * Get respawn location given by Crossbow.
	 * <p>
	 * Overrides vanilla respawn to prevent respawns in the wrong world
	 * 
	 * @return
	 */
	public Location getRespawnLocation() {
		return respawnLocation;
	}
}
