package com.grahhnt.Crossbow.Events.Player;

import com.grahhnt.Crossbow.Events.GameEvent;
import com.grahhnt.Crossbow.Players.GamePlayer;

public class GamePlayerDisconnectEvent extends GameEvent {
	private GamePlayer player;
	private boolean willDestroy = false;

	public GamePlayerDisconnectEvent(GamePlayer player, boolean willDestroy) {
		super("GamePlayerDisconnectEvent");
		this.player = player;
		this.willDestroy = willDestroy;
	}
	
	public GamePlayer getPlayer() {
		return player;
	}
	
	public boolean willDestroy() {
		return willDestroy;
	}
	
	public void setDestroyPlayer(boolean willDestroy) {
		this.willDestroy = willDestroy;
	}
}
