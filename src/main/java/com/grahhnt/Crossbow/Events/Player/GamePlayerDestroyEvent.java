package com.grahhnt.Crossbow.Events.Player;

import com.grahhnt.Crossbow.Events.GameEvent;
import com.grahhnt.Crossbow.Players.GamePlayer;

public class GamePlayerDestroyEvent extends GameEvent {
	private GamePlayer player;

	public GamePlayerDestroyEvent(GamePlayer player) {
		super("GamePlayerDestroyEvent");
		this.player = player;
	}
	
	public GamePlayer getPlayer() {
		return player;
	}
}
