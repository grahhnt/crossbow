package com.grahhnt.Crossbow.Events.Player;

import java.util.List;

import com.grahhnt.Crossbow.Events.GameEvent;
import com.grahhnt.Crossbow.Events.Game.GameScoreboardUpdateEvent;

/**
 * Called for each player when the scoreboard is being updated for them
 * 
 * If this isn't canceled, {@link GameScoreboardUpdateEvent} will be used for
 * their scoreboard
 * 
 * @author grant
 *
 */
public class GamePlayerScoreboardUpdateEvent extends GameEvent.Cancellable {
	private boolean hasChanged = false;
	private List<String> header;
	private List<String> items;
	private List<String> footer;

	public GamePlayerScoreboardUpdateEvent(List<String> header, List<String> items, List<String> footer) {
		super("GamePlayerScoreboardUpdateEvent");
		this.header = header;
		this.items = items;
		this.footer = footer;
	}

	public void setItems(List<String> items) {
		hasChanged = true;
		this.items = items;
	}

	public List<String> getItems() {
		return items;
	}

	public void setHeader(List<String> header) {
		hasChanged = true;
		this.header = header;
	}

	public List<String> getHeader() {
		return header;
	}

	public void setFooter(List<String> footer) {
		hasChanged = true;
		this.footer = footer;
	}

	public List<String> getFooter() {
		return footer;
	}
	
	public boolean getHasChanged() {
		return hasChanged;
	}
}
