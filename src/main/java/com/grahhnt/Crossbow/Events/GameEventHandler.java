package com.grahhnt.Crossbow.Events;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface GameEventHandler {
	public enum EventPriority {
		/**
		 * Execute first
		 */
		HIGH,
		NORMAL,
		/**
		 * Execute last
		 */
		LOW
	}
	
	GameEventHandler.EventPriority priority() default EventPriority.NORMAL;
}