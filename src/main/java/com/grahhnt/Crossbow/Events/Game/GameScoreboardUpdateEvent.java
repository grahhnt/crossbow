package com.grahhnt.Crossbow.Events.Game;

import java.util.List;

import com.grahhnt.Crossbow.Events.GameEvent;
import com.grahhnt.Crossbow.Events.Player.GamePlayerScoreboardUpdateEvent;

/**
 * Called each time the scoreboard is being updated
 * 
 * This can be overriden by {@link GamePlayerScoreboardUpdateEvent}
 * 
 * @author grant
 *
 */
public class GameScoreboardUpdateEvent extends GameEvent {
	private List<String> header;
	private List<String> items;
	private List<String> footer;

	public GameScoreboardUpdateEvent(List<String> header, List<String> items, List<String> footer) {
		super("GameScoreboardUpdateEvent");
		this.header = header;
		this.items = items;
		this.footer = footer;
	}

	public void setItems(List<String> items) {
		this.items = items;
	}

	public List<String> getItems() {
		return items;
	}

	public void setHeader(List<String> header) {
		this.header = header;
	}

	public List<String> getHeader() {
		return header;
	}

	public void setFooter(List<String> footer) {
		this.footer = footer;
	}

	public List<String> getFooter() {
		return footer;
	}
}
