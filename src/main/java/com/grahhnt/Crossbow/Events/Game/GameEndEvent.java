package com.grahhnt.Crossbow.Events.Game;

import com.grahhnt.Crossbow.Events.GameEvent;

/**
 * Fired upon game end. Can be used to send players to lobby
 * 
 * Cannot be canceled.
 * 
 * @author grant
 *
 */
public class GameEndEvent extends GameEvent {
	public GameEndEvent() {
		super("GameEndEvent");
	}
}
