package com.grahhnt.Crossbow.Events.Game;

import com.grahhnt.Crossbow.Events.GameEvent;

/**
 * Fired when game is trying to start to allow for minigame plugin to do checks
 * before countdown starts
 * 
 * @author grant
 *
 */
public class GameStartEvent extends GameEvent.Cancellable {
	public GameStartEvent() {
		super("GameStartEvent");
	}
}
