package com.grahhnt.Crossbow.Events.Game;

import com.grahhnt.Crossbow.Crossbow.GameState;
import com.grahhnt.Crossbow.Events.GameEvent;

public class GameStateEvent extends GameEvent.Cancellable {
	private GameState oldState;
	private GameState newState;
	private boolean runComponents = true;

	public GameStateEvent(GameState oldState, GameState newState) {
		super("GameStateEvent");
		this.oldState = oldState;
		this.newState = newState;
	}
	
	public GameState getOldState() {
		return oldState;
	}

	public GameState getNewState() {
		return newState;
	}
	
	public boolean shouldRunComponents() {
		return runComponents;
	}
	
	public void setRunComponents(boolean runComponents) {
		this.runComponents = runComponents;
	}
}
