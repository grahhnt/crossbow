package com.grahhnt.Crossbow.Components.General;

import com.grahhnt.Crossbow.Crossbow;
import com.grahhnt.Crossbow.Components.GameComponent;
import com.grahhnt.Crossbow.Events.Player.GamePlayerDeathEvent;

/**
 * Instantly respawns the player when not in GameState.GAME
 * 
 * @author grant
 *
 */
public class InstantRespawn extends GameComponent.GameEvent {
	public InstantRespawn(Crossbow game) {
		super(game);
	}

	@Override
	public void execute(com.grahhnt.Crossbow.Events.GameEvent event) {
		if (game.getGameState() != Crossbow.GameState.GAME) {
			GamePlayerDeathEvent death = (GamePlayerDeathEvent) event;
			death.setShouldInstantRespawn(true);
		}
	}

}
