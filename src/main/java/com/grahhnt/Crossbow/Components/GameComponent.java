package com.grahhnt.Crossbow.Components;

import com.grahhnt.Crossbow.Crossbow;

public class GameComponent {
	private boolean hasRan = false;
	protected Crossbow game;

	protected GameComponent(Crossbow game) {
		this.game = game;
	}

	public boolean hasRan() {
		return hasRan;
	}

	/**
	 * Internal
	 * 
	 * @deprecated
	 * @param hasRan
	 */
	public void setHasRan(boolean hasRan) {
		this.hasRan = hasRan;
	}

	/**
	 * GameState changes can execute a shutdown method
	 * 
	 * @author grant
	 *
	 */
	public static abstract class GameState extends GameComponent {
		protected GameState(Crossbow game) {
			super(game);
		}

		public abstract void execute();

		public abstract void shutdown();
	}

	/**
	 * GameEvent changes don't have a shutdown/"post" method
	 * 
	 * @author grant
	 *
	 */
	public static abstract class GameEvent extends GameComponent {
		protected GameEvent(Crossbow game) {
			super(game);
		}
		
		public abstract void execute(com.grahhnt.Crossbow.Events.GameEvent event);
	}
}