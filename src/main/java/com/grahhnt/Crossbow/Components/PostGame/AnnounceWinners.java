package com.grahhnt.Crossbow.Components.PostGame;

import java.util.List;

import com.grahhnt.Crossbow.Crossbow;
import com.grahhnt.Crossbow.CrossbowUtils;
import com.grahhnt.Crossbow.Components.GameComponent;
import com.grahhnt.Crossbow.Players.GamePlayer;
import com.grahhnt.Crossbow.Players.Winnable;

public class AnnounceWinners extends GameComponent.GameState {

	public AnnounceWinners(Crossbow game) {
		super(game);
	}

	private boolean isWinner(GamePlayer player) {
		return player.isWinner() || player.getTeam().isWinner();
	}

	@Override
	public void execute() {
		List<Winnable> winners = game.getWinners();
		for (GamePlayer player : game.players.connected()) {
			player.getPlayer().sendTitle(CrossbowUtils.format(isWinner(player) ? "&a&lWinner!" : "&c&lGame Over"),
					CrossbowUtils.format(Winnable.winnerString(winners)), 10, 70, 20);
		}
	}

	@Override
	public void shutdown() {
		// TODO Auto-generated method stub

	}

}
