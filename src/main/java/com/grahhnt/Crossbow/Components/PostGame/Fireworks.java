package com.grahhnt.Crossbow.Components.PostGame;

import org.bukkit.entity.Firework;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitTask;

import com.grahhnt.Crossbow.Crossbow;
import com.grahhnt.Crossbow.CrossbowUtils;
import com.grahhnt.Crossbow.Components.GameComponent;

public class Fireworks extends GameComponent.GameState {
	private BukkitTask fireworksTimer = null;
	
	public Fireworks(Crossbow game) {
		super(game);
	}

	@Override
	public void execute() {
		if (fireworksTimer != null) {
			fireworksTimer.cancel();
			fireworksTimer = null;
		}

		fireworksTimer = new BukkitRunnable() {
			@Override
			public void run() {
				game.getWinners().forEach(winner -> {
					winner.getWinningPlayers().forEach(player -> {
						Firework fw = CrossbowUtils.spawnRandomFirework(player.getPlayer().getLocation());
						fw.setMetadata("cosmetic", new FixedMetadataValue(game.getPlugin(), true));
					});
				});
			}
		}.runTaskTimer(game.getPlugin(), 0L, 5L);
	}
	
	@Override
	public void shutdown() {
		if (fireworksTimer != null) {
			fireworksTimer.cancel();
			fireworksTimer = null;
		}
	}
}
