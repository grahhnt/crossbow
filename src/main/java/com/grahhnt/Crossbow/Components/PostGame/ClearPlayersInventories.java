package com.grahhnt.Crossbow.Components.PostGame;

import com.grahhnt.Crossbow.Crossbow;
import com.grahhnt.Crossbow.Components.GameComponent;

public class ClearPlayersInventories extends GameComponent.GameEvent {
	public ClearPlayersInventories(Crossbow game) {
		super(game);
	}

	@Override
	public void execute(com.grahhnt.Crossbow.Events.GameEvent event) {
		game.players.all().forEach(player -> {
			player.getInventory().clear();
		});
	}
	
}
