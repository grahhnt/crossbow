package com.grahhnt.Crossbow.Components.PostGame;

import org.bukkit.Bukkit;

import com.grahhnt.Crossbow.Crossbow;
import com.grahhnt.Crossbow.Components.GameComponent;
import com.grahhnt.Crossbow.Exceptions.MapNotFoundException;
import com.grahhnt.Crossbow.Map.MapManager.Map;

/**
 * Send all connected players to lobby causing active_game world to be unloaded
 * and deleted
 * 
 * @author grant
 *
 */
public class ResetMap extends GameComponent.GameEvent {

	public ResetMap(Crossbow game) {
		super(game);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void execute(com.grahhnt.Crossbow.Events.GameEvent event) {
		try {
			game.maps.changeMap((Map) null);

			Bukkit.getScheduler().runTask(game.getPlugin(), () -> {
				if (game.maps.getDefaultMap() != null) {
					try {
						game.maps.changeMap(game.maps.getDefaultMap());
					} catch (MapNotFoundException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			});
		} catch (MapNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
