package com.grahhnt.Crossbow.Components.Team;

import org.bukkit.Bukkit;

import com.grahhnt.Crossbow.Crossbow;
import com.grahhnt.Crossbow.Components.GameComponent;
import com.grahhnt.Crossbow.Events.Player.GamePlayerDamageEvent;
import com.grahhnt.Crossbow.Players.GamePlayer;

public class NoFriendlyFire extends GameComponent.GameEvent {

	public NoFriendlyFire(Crossbow game) {
		super(game);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void execute(com.grahhnt.Crossbow.Events.GameEvent event) {
		GamePlayerDamageEvent damage = (GamePlayerDamageEvent) event;
		GamePlayer victim = damage.getPlayer();
		GamePlayer damager = victim.getLastDamage() != null ? victim.getLastDamage().getPlayer() : null;
		
		if(damager != null) {
			if(damager.getTeam().hasPlayer(victim)) {
				damage.setCancelled(true);
				damager.sendMessage("&cFriendly fire is disabled");
			}
		}
	}

}
