package com.grahhnt.Crossbow.Components;

import java.util.List;
import java.util.stream.Collectors;

import com.grahhnt.Crossbow.Crossbow;
import com.grahhnt.Crossbow.CrossbowUtils.Function;
import com.grahhnt.Crossbow.Players.Winnable;

public class Functions {
	public static final Function<List<Winnable>> playerWinner = () -> {
		return Crossbow.getInstance().players.all().stream().filter(player -> player.isWinner())
				.collect(Collectors.toList());
	};

	public static final Function<List<Winnable>> teamWinner = () -> {
		return Crossbow.getInstance().players.getTeams().stream().filter(team -> team.isWinner())
				.collect(Collectors.toList());
	};
}
