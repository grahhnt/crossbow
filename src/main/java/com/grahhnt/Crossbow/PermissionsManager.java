package com.grahhnt.Crossbow;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;

import org.bukkit.Bukkit;
import org.bukkit.permissions.Permission;

import com.grahhnt.Crossbow.Players.GamePlayer;

public class PermissionsManager extends CrossbowComponent {
	private static String prefix;
	private HashMap<CrossbowComponent, ArrayList<Permission>> permissions = new HashMap<>();

	PermissionsManager(Crossbow game, String prefix) {
		super(game);
		this.prefix = prefix;
	}
	
	public static String getPrefix() {
		return prefix;
	}

	@Override
	public void initialize() {

	}

	@Override
	public void destroy() {
		for (Entry<CrossbowComponent, ArrayList<Permission>> entry : permissions.entrySet()) {
			for (Permission perm : entry.getValue()) {
				Bukkit.getServer().getPluginManager().removePermission(perm);
			}
		}
		permissions.clear();
	}

	public void registerPermission(CrossbowComponent component, Permission permission) {
		if (!permissions.containsKey(component)) {
			permissions.put(component, new ArrayList<>());
		}

		permissions.get(component).add(permission);
	}

	public boolean hasPermission(GamePlayer player, String permission) {
		return player.getPlayer().hasPermission(permission);
	}
	
	public static String node(String node) {
		return prefix + "." + node;
	}
}
