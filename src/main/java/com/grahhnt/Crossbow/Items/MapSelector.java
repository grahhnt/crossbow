package com.grahhnt.Crossbow.Items;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerInteractEvent;

import com.grahhnt.Crossbow.Crossbow;
import com.grahhnt.Crossbow.PermissionsManager;
import com.grahhnt.Crossbow.GUI.MapSelectorGUI;
import com.grahhnt.Crossbow.Map.MapManager;
import com.grahhnt.Crossbow.Players.GamePlayer;

public class MapSelector extends CrossbowItem {
	public static String id = "MAP_SELECTOR";

	MapSelector() {
		super(Material.MAP, 1);
		setName("&rChange Map");
		setLore("&7Right-click with this to change the map");
	}

	private void handle(Player player) {
		GamePlayer gp = Crossbow.getInstance().players.getPlayer(player);
		if (gp.hasPermission(PermissionsManager.node(MapManager.Permissions.MAP_CHANGE))) {
			new MapSelectorGUI(gp).open(gp);
		} else {
			gp.sendMessage("&cNo permission.");
		}
	}

	@Override
	public void onUse(PlayerInteractEvent event) {
		event.setCancelled(true);
		handle(event.getPlayer());
	}

	@Override
	public void onClick(InventoryClickEvent event) {
		event.setCancelled(true);
		handle((Player) event.getWhoClicked());
	}
}
