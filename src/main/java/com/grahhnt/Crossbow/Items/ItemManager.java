package com.grahhnt.Crossbow.Items;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.UUID;
import java.util.function.Consumer;

import org.bukkit.Material;
import org.bukkit.NamespacedKey;
import org.bukkit.event.EventHandler;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.persistence.PersistentDataType;

import com.grahhnt.Crossbow.Crossbow;
import com.grahhnt.Crossbow.CrossbowComponent;
import com.grahhnt.Crossbow.Events.GameEventHandler;
import com.grahhnt.Crossbow.Events.Player.GamePlayerJoinEvent;

public class ItemManager extends CrossbowComponent implements org.bukkit.event.Listener, com.grahhnt.Crossbow.Events.GameListener {
	private static HashMap<String, CrossbowItem> items = new HashMap<>();

	public ItemManager(Crossbow game) {
		super(game);
	}

	@Override
	public void initialize() {
		game.getPlugin().getServer().getPluginManager().registerEvents(this, game.getPlugin());
		game.events.registerEvents(this);
	}

	void registerItem(CrossbowItem item) {
		items.put(item.id, item);
	}

	public CrossbowItem get(UUID id) {
		return items.get(id);
	}

	public CrossbowItem get(Class<? extends CrossbowItem> clazz) {
		// get crossbow item id from class passed
		String id = null;
		try {
			id = (String) clazz.getDeclaredField("id").get(null);
		} catch (IllegalArgumentException | IllegalAccessException | NoSuchFieldException | SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (id == null)
			return null;

		// get crossbowitem instance
		CrossbowItem item = items.get(id);

		if (item == null) {
			// create CrossbowItem instance if not found
			try {
				Constructor<? extends CrossbowItem> c = clazz.getDeclaredConstructor();
				c.setAccessible(true);
				item = c.newInstance();
			} catch (InstantiationException | IllegalAccessException | IllegalArgumentException
					| InvocationTargetException | NoSuchMethodException | SecurityException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			items.put(id, item);
		}

		return item;
	}

	public CrossbowItem getOrCreate(String id, Material material, int count) {
		if (items.containsKey(id)) {
			return items.get(id);
		}

		return new CrossbowItem(id, material, count);
	}

	private CrossbowItem getItemFromMeta(ItemStack item) {
		// ignore interaction if the player isn't holding anything
		if (item == null || item.getItemMeta() == null || item.getItemMeta().getPersistentDataContainer() == null)
			return null;

		if (!item.getItemMeta().getPersistentDataContainer().has(NamespacedKey.fromString("crossbow:itemid"),
				PersistentDataType.STRING)) {
			return null;
		}

		// get crossbow itemid, if not, ignore
		String itemid = item.getItemMeta().getPersistentDataContainer().get(NamespacedKey.fromString("crossbow:itemid"),
				PersistentDataType.STRING);
		if (itemid == null)
			return null;

		// get crossbow item, if not, ignore
		CrossbowItem citem = items.get(itemid);
		if (citem == null)
			return null;

		return citem;
	}

	/**
	 * Updates all CrossbowItems in inventory. Will replace CrossbowItems with fresh
	 * instance
	 * 
	 * @param inventory
	 */
	public void updateItems(Inventory inventory) {
		int slot = -1;
		for (ItemStack item : inventory.getContents()) {
			slot++;
			if (item == null)
				continue;

			CrossbowItem citem = getItemFromMeta(item);
			if (citem == null)
				continue;

			inventory.setItem(slot, citem);
		}
	}

	@GameEventHandler
	public void playerJoin(GamePlayerJoinEvent event) {
		updateItems(event.getPlayer().getPlayer().getInventory());
	}

	@EventHandler
	public void itemUse(PlayerInteractEvent event) {
		CrossbowItem item = getItemFromMeta(event.getItem());
		if (item == null)
			return;

		for (Consumer<PlayerInteractEvent> handler : item.useHandlers) {
			handler.accept(event);
		}

		item.onUse(event);
	}

	@EventHandler
	public void inventoryClick(InventoryClickEvent event) {
		CrossbowItem item = getItemFromMeta(event.getCurrentItem());
		if (item == null)
			return;

		for (Consumer<InventoryClickEvent> handler : item.clickHandlers) {
			handler.accept(event);
		}

		item.onClick(event);
	}
}
