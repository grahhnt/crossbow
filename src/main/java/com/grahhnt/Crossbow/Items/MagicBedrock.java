package com.grahhnt.Crossbow.Items;

import org.bukkit.Material;
import org.bukkit.entity.Enderman;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.event.player.PlayerInteractEvent;

public class MagicBedrock extends CrossbowItem {
	public static String id = "MAGIC_BEDROCK";
	
	MagicBedrock() {
		super(Material.BEDROCK, 1);
		setName("&dMagic Bedrock");
	}
	
	@Override
	public void onUse(PlayerInteractEvent ev) {
		ev.setCancelled(true);
		ev.getPlayer().getLocation().add(0, 2, 0);
		ev.getPlayer().sendMessage("magic bedrock!");
	}
	
	@Override
	public void onClick(InventoryClickEvent ev) {
		if (ev.getClickedInventory().getType() != InventoryType.PLAYER)
			return;

		ev.setCancelled(true);
		ev.getWhoClicked().getWorld().spawn(ev.getWhoClicked().getLocation(), Enderman.class);
		ev.getWhoClicked().sendMessage("it's the magic bedrock!");
	}
}
