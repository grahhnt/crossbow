package com.grahhnt.Crossbow.Items;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.UUID;
import java.util.function.Consumer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.commons.lang3.text.WordUtils;
import org.bukkit.Material;
import org.bukkit.NamespacedKey;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.PotionMeta;
import org.bukkit.persistence.PersistentDataType;
import org.bukkit.potion.PotionData;
import org.bukkit.potion.PotionEffect;

import com.grahhnt.Crossbow.Crossbow;
import com.grahhnt.Crossbow.CrossbowUtils;

public class CrossbowItem extends ItemStack {
	String id = null;
	Crossbow game;

	CrossbowItem(String id, Material material, int count) {
		super(material, count);
		this.id = id;

		this.game = Crossbow.getInstance();
		game.items.registerItem(this);

		ItemMeta meta = getItemMeta();
		// save itemid into item data
		meta.getPersistentDataContainer().set(NamespacedKey.fromString("crossbow:itemid"), PersistentDataType.STRING,
				id);
		setItemMeta(meta);
	}

	public static class Potion extends CrossbowItem {
		public Potion(int count, boolean splash) {
			super(splash ? Material.SPLASH_POTION : Material.POTION, count);
		}

		/**
		 * Set base potion effect
		 * 
		 * @param potion
		 * @return
		 */
		public Potion potion(PotionData potion) {
			PotionMeta meta = (PotionMeta) getItemMeta();
			meta.setBasePotionData(potion);
			setItemMeta(meta);
			return this;
		}

		/**
		 * Add custom potion effect
		 * 
		 * @param potion
		 * @param overwrite
		 * @return
		 */
		public Potion potion(PotionEffect potion, boolean overwrite) {
			PotionMeta meta = (PotionMeta) getItemMeta();
			meta.addCustomEffect(potion, overwrite);
			setItemMeta(meta);
			return this;
		}

		/**
		 * Add custom potion effect without overwriting
		 * 
		 * @param potion
		 * @return
		 */
		public Potion potion(PotionEffect potion) {
			return potion(potion, false);
		}
	}

	public boolean isSame(ItemStack item) {
		if (item == null)
			return false;

		ItemMeta meta = item.getItemMeta();
		if (meta.getPersistentDataContainer() == null)
			return false;
		if (!meta.getPersistentDataContainer().has(NamespacedKey.fromString("crossbow:itemid"),
				PersistentDataType.STRING))
			return false;

		return meta.getPersistentDataContainer()
				.get(NamespacedKey.fromString("crossbow:itemid"), PersistentDataType.STRING).equals(id);
	}

	public void setName(String name) {
		ItemMeta meta = getItemMeta();
		meta.setDisplayName(CrossbowUtils.format(name));
		setItemMeta(meta);
	}

	public void setLore(String... lore) {
		ItemMeta meta = getItemMeta();
		ArrayList<String> ll = new ArrayList<>();

		for (String l : lore) {
			ll.add(l != null ? CrossbowUtils.format(l) : "�a");
		}

		meta.setLore(ll);
		setItemMeta(meta);
	}

	/**
	 * Wrap lore with a default of 30
	 * <p>
	 * Preserves color and formatting codes.
	 */
	public void wrapLore() {
		wrapLore(30);
	}

	/**
	 * Wrap lore with custom length.
	 * <p>
	 * Preserves color and formatting codes.
	 * 
	 * @param maxLength
	 */
	public void wrapLore(int maxLength) {
		ItemMeta meta = getItemMeta();

		Pattern lastColor = Pattern.compile("�[0-9a-f]");
		Pattern lastFormat = Pattern.compile("(?<=�[0-9a-f])�[k-or]");
		Pattern anyFormatting = Pattern.compile("�[0-9a-fk-or]");

		meta.setLore(meta.getLore().stream()
				.map((String l) -> WordUtils.wrap(l, (int) (maxLength + anyFormatting.matcher(l).results().count())))
				.flatMap((String s) -> {
					String[] lines = s.split("\n");
					int linec = -1;
					for (String line : lines) {
						linec++;
						if (linec == 0)
							continue;

						if (!line.matches("^&[0-9a-fk-or]")) { // doesn't start with a color/format code and isn't line
																// 1
							String lastFColor = null;
							Matcher cmatcher = lastColor.matcher(lines[linec - 1]);
							while (cmatcher.find()) {
								lastFColor = cmatcher.group();
							}
							String lastFFormat = null;
							Matcher fmatcher = lastFormat.matcher(lines[linec - 1]);
							while (fmatcher.find()) {
								lastFFormat = fmatcher.group();
							}

							if (lastFFormat != null) {
								lines[linec] = lastFFormat + lines[linec];
							}
							if (lastFColor != null) {
								lines[linec] = lastFColor + lines[linec];
							}
						}
					}
					Stream<String> out = Arrays.stream(lines);

					return out;
				}).collect(Collectors.toList()));

		setItemMeta(meta);
	}

	public CrossbowItem(Material material, int count) {
		this(UUID.randomUUID().toString(), material, count);
	}

	public CrossbowItem enchantment(Enchantment enchantment, int level) {
		addEnchantment(enchantment, level);
		return this;
	}

	ArrayList<Consumer<PlayerInteractEvent>> useHandlers = new ArrayList<>();

	public void handleUse(Consumer<PlayerInteractEvent> consumer) {
		useHandlers.add(consumer);
	}

	ArrayList<Consumer<InventoryClickEvent>> clickHandlers = new ArrayList<>();

	public void handleClick(Consumer<InventoryClickEvent> consumer) {
		clickHandlers.add(consumer);
	}

	/**
	 * Fired when a player interacts with the world with the item.
	 * <p>
	 * Overridable for class-based CrossbowItems.
	 * 
	 * @param event
	 */
	public void onUse(PlayerInteractEvent event) {

	}

	/**
	 * Fired when a player interacts with the item in an inventory.
	 * <p>
	 * Overridable for class-based CrossbowItems
	 * 
	 * @param event
	 */
	public void onClick(InventoryClickEvent event) {

	}
}
