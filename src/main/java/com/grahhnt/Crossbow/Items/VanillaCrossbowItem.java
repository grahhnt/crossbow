package com.grahhnt.Crossbow.Items;

import java.util.Arrays;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

/**
 * Vanilla Minecraft items with extra metadata used for operations, like
 * checking if a Material is a pickaxe
 * 
 * @author grant
 *
 */
public class VanillaCrossbowItem extends CrossbowItem {

	private Armor armorType = null;
	private Tool toolType = null;
	/**
	 * What this item is made out of
	 */
	private ToolMaterial toolMaterial = null;
	private ArmorMaterial armorMaterial = null;

	public enum Tool {
		SWORD, PICKAXE, AXE, SHOVEL, HOE
	}

	public enum Armor {
		HELMET, CHESTPLATE, LEGGINGS, BOOTS
	}

	public enum ToolMaterial {
		WOOD, STONE, GOLD, IRON, DIAMOND, NETHERITE
	}

	public enum ArmorMaterial {
		LEATHER, CHAINMAIL, GOLD, IRON, DIAMOND, NETHERITE
	}

	private VanillaCrossbowItem(Material material, int amount) {
		super(material, amount);

		// determine what type of tool it is
		switch (material) {
		case WOODEN_SWORD:
		case STONE_SWORD:
		case GOLDEN_SWORD:
		case IRON_SWORD:
		case DIAMOND_SWORD:
		case NETHERITE_SWORD:
			toolType = Tool.SWORD;
			break;
		case WOODEN_PICKAXE:
		case STONE_PICKAXE:
		case GOLDEN_PICKAXE:
		case IRON_PICKAXE:
		case DIAMOND_PICKAXE:
		case NETHERITE_PICKAXE:
			toolType = Tool.PICKAXE;
			break;
		case WOODEN_AXE:
		case STONE_AXE:
		case GOLDEN_AXE:
		case IRON_AXE:
		case DIAMOND_AXE:
		case NETHERITE_AXE:
			toolType = Tool.AXE;
			break;
		case WOODEN_SHOVEL:
		case STONE_SHOVEL:
		case GOLDEN_SHOVEL:
		case IRON_SHOVEL:
		case DIAMOND_SHOVEL:
		case NETHERITE_SHOVEL:
			toolType = Tool.SHOVEL;
			break;
		case WOODEN_HOE:
		case STONE_HOE:
		case GOLDEN_HOE:
		case IRON_HOE:
		case DIAMOND_HOE:
		case NETHERITE_HOE:
			toolType = Tool.HOE;
			break;
		case LEATHER_HELMET:
		case CHAINMAIL_HELMET:
		case GOLDEN_HELMET:
		case IRON_HELMET:
		case DIAMOND_HELMET:
		case NETHERITE_HELMET:
			armorType = Armor.HELMET;
			break;
		case LEATHER_CHESTPLATE:
		case CHAINMAIL_CHESTPLATE:
		case GOLDEN_CHESTPLATE:
		case IRON_CHESTPLATE:
		case DIAMOND_CHESTPLATE:
		case NETHERITE_CHESTPLATE:
			armorType = Armor.CHESTPLATE;
			break;
		case LEATHER_LEGGINGS:
		case CHAINMAIL_LEGGINGS:
		case GOLDEN_LEGGINGS:
		case IRON_LEGGINGS:
		case DIAMOND_LEGGINGS:
		case NETHERITE_LEGGINGS:
			armorType = Armor.LEGGINGS;
			break;
		case LEATHER_BOOTS:
		case CHAINMAIL_BOOTS:
		case GOLDEN_BOOTS:
		case IRON_BOOTS:
		case DIAMOND_BOOTS:
		case NETHERITE_BOOTS:
			armorType = Armor.BOOTS;
			break;
		}

		// determine what it is made of
		switch (material) {
		case WOODEN_SWORD:
		case WOODEN_PICKAXE:
		case WOODEN_AXE:
		case WOODEN_SHOVEL:
		case WOODEN_HOE:
			toolMaterial = ToolMaterial.WOOD;
			break;
		case STONE_SWORD:
		case STONE_PICKAXE:
		case STONE_AXE:
		case STONE_SHOVEL:
		case STONE_HOE:
			toolMaterial = ToolMaterial.STONE;
			break;
		case GOLDEN_SWORD:
		case GOLDEN_PICKAXE:
		case GOLDEN_AXE:
		case GOLDEN_SHOVEL:
		case GOLDEN_HOE:
			toolMaterial = ToolMaterial.GOLD;
			break;
		case GOLDEN_HELMET:
		case GOLDEN_CHESTPLATE:
		case GOLDEN_LEGGINGS:
		case GOLDEN_BOOTS:
			armorMaterial = ArmorMaterial.GOLD;
			break;
		case IRON_SWORD:
		case IRON_PICKAXE:
		case IRON_AXE:
		case IRON_SHOVEL:
		case IRON_HOE:
			toolMaterial = ToolMaterial.IRON;
			break;
		case IRON_HELMET:
		case IRON_CHESTPLATE:
		case IRON_LEGGINGS:
		case IRON_BOOTS:
			armorMaterial = ArmorMaterial.IRON;
			break;
		case DIAMOND_SWORD:
		case DIAMOND_PICKAXE:
		case DIAMOND_AXE:
		case DIAMOND_SHOVEL:
		case DIAMOND_HOE:
			toolMaterial = ToolMaterial.DIAMOND;
			break;
		case DIAMOND_HELMET:
		case DIAMOND_CHESTPLATE:
		case DIAMOND_LEGGINGS:
		case DIAMOND_BOOTS:
			armorMaterial = ArmorMaterial.DIAMOND;
			break;
		case NETHERITE_SWORD:
		case NETHERITE_PICKAXE:
		case NETHERITE_AXE:
		case NETHERITE_SHOVEL:
		case NETHERITE_HOE:
			toolMaterial = ToolMaterial.NETHERITE;
			break;
		case NETHERITE_HELMET:
		case NETHERITE_CHESTPLATE:
		case NETHERITE_LEGGINGS:
		case NETHERITE_BOOTS:
			armorMaterial = ArmorMaterial.NETHERITE;
			break;
		case LEATHER_HELMET:
		case LEATHER_CHESTPLATE:
		case LEATHER_LEGGINGS:
		case LEATHER_BOOTS:
			armorMaterial = ArmorMaterial.LEATHER;
			break;
		case CHAINMAIL_HELMET:
		case CHAINMAIL_CHESTPLATE:
		case CHAINMAIL_LEGGINGS:
		case CHAINMAIL_BOOTS:
			armorMaterial = ArmorMaterial.CHAINMAIL;
			break;
		}
	}

	public boolean isBetter(VanillaCrossbowItem other) {
		if (other == null) {
			// if other item is null, ours is always better
			return true;
		}

		if (toolType != null) {
			if (toolType != other.toolType)
				return false;

			return toolMaterial.compareTo(other.toolMaterial) > 0;
		} else if (armorType != null) {
			if (armorType != other.armorType)
				return false;

			return armorMaterial.compareTo(other.armorMaterial) > 0;
		}
		return false;
	}

	public boolean isSameMaterial(VanillaCrossbowItem other) {
		return toolMaterial == other.toolMaterial && toolMaterial != null;
	}

	public boolean isSameTool(VanillaCrossbowItem other) {
		return toolType == other.toolType && toolType != null;
	}

	public VanillaCrossbowItem downgrade() {
		if (toolMaterial != null) {
			int index = Arrays.asList(ToolMaterial.values()).indexOf(toolMaterial) - 1;
			if (index < 0)
				index = 0;
			ToolMaterial material = ToolMaterial.values()[index];
			return get(material, toolType);
		} else if (armorMaterial != null) {
			int index = Arrays.asList(ArmorMaterial.values()).indexOf(armorMaterial) - 1;
			if (index < 0)
				index = 0;
			ArmorMaterial material = ArmorMaterial.values()[index];
			return get(material, armorType);
		}
		return null;
	}

	public static VanillaCrossbowItem get(ItemStack item) {
		if (item == null)
			return null;

		return get(item.getType());
	}

	public static VanillaCrossbowItem get(ArmorMaterial material, Armor armor) {
		switch (armor) {
		case HELMET:
			switch (material) {
			case LEATHER:
				return get(Material.LEATHER_HELMET);
			case CHAINMAIL:
				return get(Material.CHAINMAIL_HELMET);
			case GOLD:
				return get(Material.GOLDEN_HELMET);
			case IRON:
				return get(Material.IRON_HELMET);
			case DIAMOND:
				return get(Material.DIAMOND_HELMET);
			case NETHERITE:
				return get(Material.NETHERITE_HELMET);
			}
			break;
		case CHESTPLATE:
			switch (material) {
			case LEATHER:
				return get(Material.LEATHER_CHESTPLATE);
			case CHAINMAIL:
				return get(Material.CHAINMAIL_CHESTPLATE);
			case GOLD:
				return get(Material.GOLDEN_CHESTPLATE);
			case IRON:
				return get(Material.IRON_CHESTPLATE);
			case DIAMOND:
				return get(Material.DIAMOND_CHESTPLATE);
			case NETHERITE:
				return get(Material.NETHERITE_CHESTPLATE);
			}
			break;
		case LEGGINGS:
			switch (material) {
			case LEATHER:
				return get(Material.LEATHER_LEGGINGS);
			case CHAINMAIL:
				return get(Material.CHAINMAIL_LEGGINGS);
			case GOLD:
				return get(Material.GOLDEN_LEGGINGS);
			case IRON:
				return get(Material.IRON_LEGGINGS);
			case DIAMOND:
				return get(Material.DIAMOND_LEGGINGS);
			case NETHERITE:
				return get(Material.NETHERITE_LEGGINGS);
			}
			break;
		case BOOTS:
			switch (material) {
			case LEATHER:
				return get(Material.LEATHER_BOOTS);
			case CHAINMAIL:
				return get(Material.CHAINMAIL_BOOTS);
			case GOLD:
				return get(Material.GOLDEN_BOOTS);
			case IRON:
				return get(Material.IRON_BOOTS);
			case DIAMOND:
				return get(Material.DIAMOND_BOOTS);
			case NETHERITE:
				return get(Material.NETHERITE_BOOTS);
			}
			break;
		}
		return null;
	}

	public static VanillaCrossbowItem get(ToolMaterial material, Tool tool) {
		switch (tool) {
		case AXE:
			switch (material) {
			case DIAMOND:
				return get(Material.DIAMOND_AXE);
			case GOLD:
				return get(Material.GOLDEN_AXE);
			case IRON:
				return get(Material.IRON_AXE);
			case NETHERITE:
				return get(Material.NETHERITE_AXE);
			case STONE:
				return get(Material.STONE_AXE);
			case WOOD:
				return get(Material.WOODEN_AXE);
			}
			break;
		case HOE:
			switch (material) {
			case DIAMOND:
				return get(Material.DIAMOND_HOE);
			case GOLD:
				return get(Material.GOLDEN_HOE);
			case IRON:
				return get(Material.IRON_HOE);
			case NETHERITE:
				return get(Material.NETHERITE_HOE);
			case STONE:
				return get(Material.STONE_HOE);
			case WOOD:
				return get(Material.WOODEN_HOE);
			}
			break;
		case PICKAXE:
			switch (material) {
			case DIAMOND:
				return get(Material.DIAMOND_PICKAXE);
			case GOLD:
				return get(Material.GOLDEN_PICKAXE);
			case IRON:
				return get(Material.IRON_PICKAXE);
			case NETHERITE:
				return get(Material.NETHERITE_PICKAXE);
			case STONE:
				return get(Material.STONE_PICKAXE);
			case WOOD:
				return get(Material.WOODEN_PICKAXE);
			}
			break;
		case SHOVEL:
			switch (material) {
			case DIAMOND:
				return get(Material.DIAMOND_SHOVEL);
			case GOLD:
				return get(Material.GOLDEN_SHOVEL);
			case IRON:
				return get(Material.IRON_SHOVEL);
			case NETHERITE:
				return get(Material.NETHERITE_SHOVEL);
			case STONE:
				return get(Material.STONE_SHOVEL);
			case WOOD:
				return get(Material.WOODEN_SHOVEL);
			}
			break;
		case SWORD:
			switch (material) {
			case DIAMOND:
				return get(Material.DIAMOND_SWORD);
			case GOLD:
				return get(Material.GOLDEN_SWORD);
			case IRON:
				return get(Material.IRON_SWORD);
			case NETHERITE:
				return get(Material.NETHERITE_SWORD);
			case STONE:
				return get(Material.STONE_SWORD);
			case WOOD:
				return get(Material.WOODEN_SWORD);
			}
			break;
		}
		return null;
	}

	public static VanillaCrossbowItem get(Material material) {
		return new VanillaCrossbowItem(material, 1);
	}

}
