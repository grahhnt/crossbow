package com.grahhnt.Crossbow.Exceptions;

/**
 * Thrown when an action was about to happen, but the child event was canceled
 * 
 * Event canceler should announce why the action failed, either with external
 * method or by specifying {@link #EventCanceledEvent(String)} error message
 * 
 * @author grant
 *
 */
public class EventCanceledException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public EventCanceledException() {
		super();
	}

	public EventCanceledException(String errorMessage) {
		super(errorMessage);
	}

	public EventCanceledException(String errorMessage, Throwable err) {
		super(errorMessage, err);
	}

	public EventCanceledException(Throwable err) {
		super(err);
	}
}
