package com.grahhnt.Crossbow.Exceptions;

/**
 * Thrown when no map is selected, but one is required
 * 
 * @author grant
 *
 */
public class NoMapSelectedException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public NoMapSelectedException() {
		super();
	}

	public NoMapSelectedException(String errorMessage) {
		super(errorMessage);
	}

	public NoMapSelectedException(String errorMessage, Throwable err) {
		super(errorMessage, err);
	}

	public NoMapSelectedException(Throwable err) {
		super(err);
	}
}
