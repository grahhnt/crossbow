package com.grahhnt.Crossbow.Exceptions;

public class MapNotFoundException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public MapNotFoundException() {
		super();
	}

	public MapNotFoundException(String errorMessage) {
		super(errorMessage);
	}

	public MapNotFoundException(String errorMessage, Throwable err) {
		super(errorMessage, err);
	}

	public MapNotFoundException(Throwable err) {
		super(err);
	}
}
