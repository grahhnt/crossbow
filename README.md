# Crossbow
A minigame engine to prevent a ton of boilerplate.

## Minigames Using This
- [WoolWars](https://gitlab.com/grahhnt/woolwars)

## Features
- Player Management using a base GamePlayer class, but does allow for GamePlayer to be replaced by a different class upon minigame registration
- GameEventManager to allow for a custom event bus for game related events
- Scoreboard Management for personalized scoreboard for each player depending on game state
- Metadata management allowing for data such as game name, player minimum & maximum

## TODO List
[Stored in issues](https://gitlab.com/grahhnt/crossbow/-/issues)

## License
This project uses an MIT license, which allows you to make a private fork of this project but retaining the license and copyright listed in `LICENSE`

I am available for hire to make a custom variant of this library or other plugin development, DM me on Discord @ `grahhnt#0001`